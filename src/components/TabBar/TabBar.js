import React, { Component } from "react";
import { View, StyleSheet, TouchableWithoutFeedback, Text, Image } from "react-native";
import { addNavigationHelpers, NavigationActions } from "react-navigation";
import { connect } from "react-redux";
import _ from "lodash";
import { COLOR_DARKEN_RED } from "../../config/common";

const styles = StyleSheet.create({
  tabBar: {
    height: 56,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#FFFFFF",
    borderTopWidth: 1,
    borderTopColor: "#F0F0F0"
  },
  tab: {
    alignSelf: "stretch",
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  badge: {
    position: "absolute",
    top: 12,
    left:43,
    backgroundColor: COLOR_DARKEN_RED,
    height: 7,
    width: 7,
    borderRadius: 5,
  }
});

class TabBar extends Component {
  
  renderBadge = () => {
    let img;
    if (!this.props.notification_count 
        || this.props.notification_count === 0) {
      img = <Image
              source={require("../../assets/icon/tab-bottom/notif1.png")}
              style={[{ height: 24, width: 24 }]}
            />;
    }else{
      img = <Image
              source={require("../../assets/icon/tab-bottom/notif2.png")}
              style={[{ height: 24, width: 24 }]}
            />;
    }

    // return (<View style={styles.badge} />);

    return (img);
  }

  _renderIcon(scene, tintColor, center) {
    const { getLabel, renderIcon, navigation } = this.props;
    let label = null;
    // if (!center)
      label = (
        <Text style={{ fontSize: 12, color: tintColor }}>
          {getLabel({
            ...scene,
            tintColor
          })}
        </Text>
      ); 

    return (
      <View style={styles.tab}>
        {scene.route.key === "Notification" && this.renderBadge()}
        {renderIcon({
          ...scene,
          tintColor
        })}
        {label}
      </View>
    );
  }

  _onPress(scene, tintColor, center) {
    const { dispatch, index, getLabel, jumpToIndex } = this.props;
    const { routes } = this.props.screenProps;
    const label = getLabel({ ...scene, tintColor });
    const route = routes[scene.route.key];

    if (!route) {
      jumpToIndex(scene.index);
      return;
    }

    dispatch(
      NavigationActions.navigate({
        routeName: route.target,
        params: route.params || {}
      })
    );
  }

  render() {
    const {
      navigation,
      activeTintColor,
      inactiveTintColor,
      jumpToIndex,
      getLabel,
      dispatch
    } = this.props;
    const { routes } = navigation.state;
    const centerIndex = _.floor(routes.length / 2);

    return (
      <View style={styles.tabBar}>
        {routes &&
          routes.map((route, index) => {
            const focused = index === navigation.state.index;
            const tintColor = focused ? activeTintColor : inactiveTintColor;
            const scene = {
              route,
              index,
              focused
            };
            const center = index === centerIndex;

            return (
              <TouchableWithoutFeedback
                key={route.key}
                style={styles.tab}
                onPress={() => this._onPress(scene, tintColor, center)}
              >
                {this._renderIcon(scene, tintColor, center)}
              </TouchableWithoutFeedback>
            );
          })}
      </View>
    );
  }
}

const mapStateToProps = state => {
  const { notification } = state;
  const count = notification.list.filter((item) => {
    return (!item.read_at);
  });
  return {
    notification_count: count.length
  };
};

export default connect(mapStateToProps)(TabBar);
