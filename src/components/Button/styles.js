import EStyleSheet from "react-native-extended-stylesheet";

const styles = EStyleSheet.create({
  buttonContainer: {
    alignSelf: "stretch",
    borderRadius: 3,
    alignItems: "center",
    justifyContent: "center",
  },
  text: {
    color: "#FFFFFF",
    textAlign: "center",
    fontSize: 15,
    fontFamily: "Arial",
    fontWeight: "400"
  }
});

export default styles;
