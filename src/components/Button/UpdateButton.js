import React from "react";
import { Platform, TouchableOpacity } from "react-native";
import Icon from "react-native-vector-icons/Ionicons";

const UpdateButton = ({ onPress, theme }) => {
  const os = Platform.OS === "ios" ? "ios" : "md";
  const color = theme === "dark" ? "white" : "black";

  return (
    <TouchableOpacity
      activeOpacity={0.7}
      style={{
        paddingHorizontal: 16
      }}
      onPress={onPress}
    >
      <Icon name={`${os}-create`} size={24} color={color} />
    </TouchableOpacity>
  );
};

UpdateButton.defaultProps = {
  theme: "dark",
  onPress: () => {}
};

export default UpdateButton;
