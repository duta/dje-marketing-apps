import React, { PureComponent } from "react";
import { TouchableOpacity, Text, View } from "react-native";
import PropTypes from "prop-types";

import { COLOR_PRIMARY, COLOR_PRIMARY_GREY } from "../../config/common";
import styles from "./toggle_button.styles";

class ToggleButton extends PureComponent {
  styles = {};

  constructor(props) {
    super(props);
    this.styles = styles(props.activeColor, props.color, props.size);
  }

  static propTypes = {
    size: PropTypes.number,
    activeColor: PropTypes.string,
    color: PropTypes.string,
    isActive: PropTypes.bool.isRequired
  };

  static defaultProps = {
    size: 32,
    activeColor: COLOR_PRIMARY,
    color: COLOR_PRIMARY_GREY
  };

  renderIndicator() {
    let indicatorStyles = [this.styles.indicator];
    if (this.props.isActive) {
      indicatorStyles.push(this.styles.indicatorActive);
    }
    return <View style={indicatorStyles} />;
  }

  render() {
    const { isActive, onPress } = this.props;

    let wrapperSyles = [this.styles.wrapper];
    if (isActive) {
      wrapperSyles.push(this.styles.wrapperActive);
    }

    return (
      <TouchableOpacity
        activeOpacity={0.7}
        onPress={onPress}
        style={wrapperSyles}
      >
        {this.renderIndicator()}
      </TouchableOpacity>
    );
  }
}

export default ToggleButton;
