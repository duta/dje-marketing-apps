import React from "react";
import { StyleSheet, View } from "react-native";
import LinearGradient from "react-native-linear-gradient";
import _ from "lodash";

import Button from "./Button";

const GradientBorderButton = ({
  borderGradient,
  borderWidth,
  containerStyle,
  ...props
}) => {
  return (
    <LinearGradient
      colors={borderGradient}
      start={{ x: 0, y: 1 }}
      end={{ x: 1, y: 1 }}
      style={[
        { alignSelf: "stretch", padding: borderWidth, borderRadius: 3 },
        containerStyle
      ]}
    >
      <Button {...props} />
    </LinearGradient>
  );
};

GradientBorderButton.defaultProps = {
  borderWidth: 4
};

export default GradientBorderButton;
