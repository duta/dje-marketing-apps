import React from "react";
import PropTypes from "prop-types";
import { Text, TouchableOpacity, View } from "react-native";
import LinearGradient from "react-native-linear-gradient";

import styles from "./styles";
import { COLOR_PRIMARY_GREY, BUTTON_GRADIENT_GREY } from "../../config/common";

const Button = ({
  label,
  textColor,
  backgroundColor,
  onPress,
  style,
  gradient,
  gradientDirection,
  containerStyle,
  textStyle,
  disabled
}) => {
  let view = (
    <View
      style={[
        styles.buttonContainer,
        style,
        { backgroundColor: !disabled ? backgroundColor : COLOR_PRIMARY_GREY }
      ]}
    >
      <Text style={[styles.text, { color: textColor }]}>{label}</Text>
    </View>
  );

  if (gradient.length > 0) {
    let props = {};

    if (gradientDirection === "horizontal") {
      props.start = {
        x: 0,
        y: 1
      };
      props.end = {
        x: 1,
        y: 0
      };
    }

    view = (
      <LinearGradient
        style={[styles.buttonContainer, style ]}
        colors={!disabled ? gradient : BUTTON_GRADIENT_GREY}
        {...props}
      >
        <Text style={[styles.text, { color: textColor }, textStyle]}>
          {label}
        </Text>
      </LinearGradient>
    );
  }
  return (
    <TouchableOpacity
      activeOpacity={0.8}
      onPress={onPress}
      style={[styles.buttonContainer, containerStyle]}
      disabled={disabled}
    >
      {view}
    </TouchableOpacity>
  );
};

Button.propTypes = {
  label: PropTypes.string,
  textColor: PropTypes.string,
  backgroundColor: PropTypes.string,
  onPress: PropTypes.func,
  style: PropTypes.any,
  gradient: PropTypes.array,
  gradientDirection: PropTypes.oneOf(["vertical", "horizontal"]),
  containerStyle: PropTypes.any,
  textStyle: PropTypes.any,
  disabled: PropTypes.bool
};

Button.defaultProps = {
  label: "Button",
  textColor: "#FFFFFF",
  backgroundColor: "rgb(26, 189, 212)",
  onPress: () => {},
  style: {},
  gradient: [],
  gradientDirection: "vertical",
  containerStyle: {},
  textStyle: {},
  disabled: false
};

export default Button;
