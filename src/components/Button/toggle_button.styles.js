import { StyleSheet } from "react-native";

const styles = (activeColor, color, size) =>
  StyleSheet.create({
    wrapper: {
      height: size,
      width: size * 2,
      borderRadius: size / 2,
      borderColor: color,
      borderWidth: 1,
      justifyContent: "center"
    },
    wrapperActive: {
      borderColor: activeColor
    },
    indicator: {
      width: size,
      height: size,
      borderRadius: size / 2,
      backgroundColor: color,
      marginLeft: 0
    },
    indicatorActive: {
      marginLeft: size - 1,
      backgroundColor: activeColor
    }
  });

export default styles;
