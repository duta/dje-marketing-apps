import React, { Component } from "react";
import { Dimensions,View,Text } from "react-native";
import { COLOR_PRIMARY } from "../../config/common";
import {
    LineChart,
    BarChart,
    PieChart,
    ProgressChart,
    ContributionGraph,
    StackedBarChart
  } from "react-native-chart-kit";
import { nFormatter } from "../../utils/number";

const Line = props => {

  var marketingName = [];
  var priceCount = [];
  if (props.chart) {
    props.chart.forEach(function(item) {
      marketingName.push(item.name);
      // marketingName.push(item.name.split(" ",1));
    });
  
    props.chart.forEach(function(item) {
      priceCount.push(parseInt(item.price));
    });
  }else{
    marketingName = [""];
    priceCount = [0];
  }

  formater = (val) => {
    return nFormatter(val)
  }

  return (
    <View>
    {/* <Text>Daftar Kunjungan Marketing</Text> */}
    <LineChart
        data={{
        labels: marketingName,
        datasets: [
            {
            data: priceCount
            }
        ]
        }}
        width={Dimensions.get("window").width - 16}  // from react-native
        height={220}
        // yAxisLabel={"$"}
        // yAxisSuffix={"k"}
        formatYLabel={this.formater}
        // horizontalLabelRotation={-50}
        verticalLabelRotation={-50}
        chartConfig={{
          backgroundColor: "#e26a00",
          backgroundGradientFrom: "#fb8c00",
          backgroundGradientTo: "#ffa726",
          decimalPlaces: 0, // optional, defaults to 2dp 
          color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
          labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
          verticalLabelRotation:100,
          style: {
              borderRadius: 16
          },
          propsForDots: {
              r: "6",
              strokeWidth: "2",
              stroke: "#ffa726"
          }
        }}
        bezier
        style={{
        marginVertical: 8,
        borderRadius: 16
        }}
    />
    </View>
  );
};

export default Line;