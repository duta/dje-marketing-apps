import EStyleSheet from "react-native-extended-stylesheet";
import { Dimensions, Platform } from "react-native";
import { COLOR_VIOLET_GREY } from "../../config/common";

const styles = EStyleSheet.create({
  container: {
    flexDirection: "row"
  },
  item: {
    flex: 1,
    margin: 8,
    alignSelf: "stretch"
  },
  optionsWrapper: {
    backgroundColor: "white",
    borderColor: "#CCC",
    borderWidth: 1,
    borderBottomWidth: 0,
    borderTopWidth: 0,
    backgroundColor: "white",
    zIndex: 99,
    elevation: 4
  },
  option: {
    backgroundColor: "white",
    alignSelf: "stretch",
    justifyContent: "center",
    padding: 8,
    borderBottomColor: "#DDD",
    borderBottomWidth: 1,
    ...Platform.select({
      android: {
        zIndex: 99
      }
    })
  },
  options: {
    alignSelf: "stretch"
  },
  selectInner: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    zIndex: 98
  },
  itemCheck: {
    position: "absolute",
    right: 16
  },
  header: {
    alignSelf: "stretch",
    paddingHorizontal: 8,
    paddingVertical: 12,
    borderBottomColor: "#CCC",
    borderBottomWidth: 2,
    marginBottom: 8,
    backgroundColor: "#FDFDFD"
  },
  title: {
    fontFamily: "Arial",
    color: COLOR_VIOLET_GREY,
    fontSize: 18
  },
  select: {
    justifyContent: "center"
  },
  downArrow: {
    position: "absolute",
    right: 0
  },
  valueText: {
    color: "black"
  }
});

export default styles;
