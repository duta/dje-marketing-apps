import React, { Component } from "react";
import { ImageText } from "../ImageText";

class SelectItem extends Component {
  render() {
    let normalColor = "#CCC";
    let selectedColor = "rgb(76, 217, 100)";
    const { icon, label, selected, style } = this.props;

    let color = normalColor;
    if (selected) color = selectedColor;

    return (
      <ImageText
        icon={icon}
        label={label}
        iconColor={color}
        textColor={color}
        style={[style, { borderColor: color }]}
      />
    );
  }
}

export default SelectItem;
