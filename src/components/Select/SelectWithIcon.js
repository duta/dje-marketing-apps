import React, { Component } from "react";
import { Text, View } from "react-native";

import SelectItem from "./SelectItem";

import styles from "./styles";

class SelectWithIcon extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: -1
    };
  }

  render() {
    const { label, options, selectedItem } = this.props;
    const items = options.map((item, index) => {
      let selected = false;
      if (item.value === selectedItem) selected = true;

      return (
        <SelectItem
          style={styles.item}
          icon={item.icon}
          label={item.label}
          value={item.value}
          key={item.key}
          selected={selected}
        />
      );
    });

    return (
      <View>
        <Text style={styles.label}>{label}</Text>
        <View style={styles.container}>{items}</View>
      </View>
    );
  }
}

export default SelectWithIcon;
