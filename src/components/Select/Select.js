import { isFunction, map } from "lodash";
import React, { Component } from "react";
import {
  FlatList,
  ScrollView,
  Text,
  TouchableOpacity,
  View
} from "react-native";
import Modal from "react-native-modal";
import Icon from "react-native-vector-icons/MaterialIcons";
import { COLOR_PRIMARY } from "../../config/common";
import { toTitleCase } from "../../utils/string";
import styles from "./styles";

const Item = props => {
  return (
    <TouchableOpacity
      activeOpacity={0.8}
      onPress={props.onPress}
      style={props.style}
    >
      <Text>{props.label}</Text>
      {props.selected && (
        <View style={styles.itemCheck}>
          <Icon name="check-circle" color={COLOR_PRIMARY} size={16} />
        </View>
      )}
    </TouchableOpacity>
  );
};

class Select extends Component {
  state = {
    showOptions: false
  };

  onItemSelected = (item, index) => {
    if (this.props.onItemSelected && isFunction(this.props.onItemSelected)) {
      this.props.onItemSelected(item, index);
    }

    this.toggleOptions();
  };

  renderItem = ({ item, index }) => {
    return (
      <Item
        style={styles.option}
        onPress={() => this.onItemSelected(item, index)}
        label={item.label}
        selected={this.props.selectedIndex === index}
        key={`item-${item.label}-${index}`}
      />
    );
  };

  renderItems = () => {
    return (
      <FlatList
        data={this.props.items}
        renderItem={this.renderItem}
        removeClippedSubviews={false}
        keyExtractor={(item, index) => `select-item-${index}`}
      />
    );
  };

  renderOptions() {
    const { items } = this.props;
    // const options = map(items, this.renderItem);
    return (
      <Modal
        isVisible={this.state.showOptions}
        onBackButtonPress={this.toggleOptions}
        onBackdropPress={this.toggleOptions}
      >
        <View style={styles.optionsWrapper}>
          {this.renderTitle()}
          {this.renderItems()}
        </View>
      </Modal>
    );
  }

  toggleOptions = () => {
    this.setState({
      showOptions: !this.state.showOptions
    });
  };

  getLabel = () => {
    const { selectedIndex, items } = this.props;
    if (null === selectedIndex || undefined === selectedIndex) {
      return this.props.title || this.props.label || null;
    }

    return items[selectedIndex].label;
  };

  getSelectedItem = () => {
    const { selectedIndex, items } = this.props;
    if (null === selectedIndex || undefined === selectedIndex) return null;
    return items[selectedIndex].value;
  };

  renderTitle = () => {
    return (
      <View style={styles.header}>
        <Text style={styles.title}>{this.getTitle()}</Text>
      </View>
    );
  };

  getTitle = () => {
    if (this.props.title) return toTitleCase(this.props.title);
    return toTitleCase(this.props.label);
  };

  render() {
    const {
      label,
      style,
      inputStyle,
      labelStyle,
      items,
      ...props
    } = this.props;
    return (
      <View style={style}>
        {this.props.label && <Text style={labelStyle}>{this.props.label}</Text>}
        <TouchableOpacity
          activeOpacity={0.8}
          style={[styles.select, inputStyle]}
          onPress={this.toggleOptions}
        >
          <Text style={styles.valueText}>{this.getLabel()}</Text>
          <View style={styles.downArrow}>
            <Icon name="arrow-drop-down" size={16} color="black" />
          </View>
        </TouchableOpacity>
        {this.renderOptions()}
      </View>
    );
  }
}

export default Select;
