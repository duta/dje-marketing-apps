import React, { Component } from "react";
import { Image, StyleSheet } from "react-native";

class ProfilePicture extends Component {
  renderDefaultPict = () => {
    const { gender, ...props } = this.props;
    
    switch (gender) {
      case "L":
        return require("../../assets/icon/male.png");
      case "P":
        return require("../../assets/icon/female.png");
    }
  };

  getPictSource = () => {
    const { entity } = this.props;
    switch (entity) {
      case "patient":
        return this.renderDefaultPatientPict();
      case "doctor":
        return this.renderDefaultDoctorPict();
    }

    return null;
  }

  render() {
    const { source, entity, style, ...props } = this.props;
    var imageSource = source;

    if (!source || source === null) {
      imageSource = this.renderDefaultPict();
    }

    return (
      <Image 
        defaultSource={this.renderDefaultPict()}
        source={imageSource}
        style={[styles.resize, style]}
        {...props}
      />
    );
  }
}

const styles = StyleSheet.create({
  resize: {
    resizeMode: "contain"
  },
});

export default ProfilePicture;
