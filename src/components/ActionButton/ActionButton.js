import React from "react";
import { Platform, TouchableOpacity, View, Text } from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import PropTypes from "prop-types";

import styles from "./styles";

/**
 * Action Button Component
 * @param {*} props
 */
const ActionButton = props => {
  const iconPrefix = "md";
  const iconName = `${iconPrefix}-${props.icon}`;

  return (
    <TouchableOpacity
      activeOpacity={0.7}
      style={[styles.button, props.style]}
      onPress={props.onPress}
    >
      <Icon name={iconName} size={props.iconSize} color={props.iconColor} />
    </TouchableOpacity>
  );
};

ActionButton.propTypes = {
  icon: PropTypes.string.isRequired,
  style: PropTypes.any,
  iconSize: PropTypes.number,
  iconColor: PropTypes.string,
  onPress: PropTypes.func.isRequired
};

ActionButton.defaultProps = {
  iconSize: 24,
  iconColor: "#FFFFFF"
};

export default ActionButton;
