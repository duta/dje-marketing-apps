import React, { Component } from "react";
import { Dimensions, StyleSheet, ScrollView, StatusBar } from "react-native";
import { Header } from "react-navigation";
import LinearGradient from "react-native-linear-gradient";
import { COLOR_PRIMARY,COLOR_SECONDARY } from "../../config/common";

const width = Dimensions.get("window").width;
const height = Dimensions.get("window").height - StatusBar.currentHeight;

const Background = props => {
  var containerHeight = height;
  var color = COLOR_PRIMARY;
  if (props.withHeader) {
    containerHeight -= Header.HEIGHT;
  }
  if (props.color) {
    color = props.color;
  }

  return (
    <LinearGradient
      style={[styles.wrapper, { height: containerHeight }]}
      colors={["white", color]}
      locations={[0.75, 1]}
    >
      <ScrollView
        style={[styles.scroll, props.style]}
        contentContainerStyle={[styles.scrollContent, props.containerStyle]}
        keyboardShouldPersistTaps="always"
      >
        {props.children}
      </ScrollView>
    </LinearGradient>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    width
  },
  scroll: {
    flex: 1
  },
  scrollContent: {
    flexGrow: 1
  }
});

export default Background;
