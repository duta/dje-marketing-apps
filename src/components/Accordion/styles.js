import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  header: {
    alignSelf: "stretch",
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 16
  },
  headerContent: {
    flex: 1
  },
  arrow: {
    alignItems: "flex-end",
    justifyContent: "flex-end"
  },
  wrapper: {
    overflow: "hidden",
    zIndex: 1
  }
});

export default styles;
