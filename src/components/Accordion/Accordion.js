import PropTypes from "prop-types";
import React, { Component } from "react";
import { Text, TouchableOpacity, View } from "react-native";
import styles from "./styles";

class Accordion extends Component {
  state = {
    expanded: false,
    initial: true
  };

  static propTypes = {
    renderHeader: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.func,
      PropTypes.element
    ]),
    label: PropTypes.string,
    upArrow: PropTypes.any,
    downArrow: PropTypes.any,
    disabled: PropTypes.bool
  };

  static defaultProps = {
    disabled: false
  };

  constructor(props) {
    super(props);
  }

  _renderHeader() {
    if (this.props.renderHeader) {
      return this.props.renderHeader;
    }

    return <Text style={this.props.titleStyle}>{this.props.label}</Text>;
  }

  _renderIcon() {
    const upArrow = this.props.upArrow || <Text>^</Text>;
    const downArrow = this.props.downArrow || <Text>v</Text>;

    return this.state.expanded ? upArrow : downArrow;
  }

  _setMaxHeight(event) {
    this.setState({
      maxHeight: event.nativeEvent.layout.height
    });
  }

  _setMinHeight(event) {
    this.setState({
      minHeight: event.nativeEvent.layout.height
    });
  }

  toggle() {
    this.setState({
      expanded: !this.state.expanded
    });
  }

  get contentHeight() {
    let height = !this.state.expanded
      ? this.state.minHeight
      : this.state.maxHeight + this.state.minHeight;

    return height;
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.expanded !== this.state.expanded) {
      if (this.state.expanded && this.props.onExpand) {
        this.props.onExpand();
      } else if (!this.state.expanded && this.props.onHide) {
        this.props.onHide();
      }
    }
  }

  render() {
    return (
      <View
        style={[
          styles.wrapper,
          this.props.wrapperStyle,
          { height: this.contentHeight }
        ]}
      >
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={this.toggle.bind(this)}
          disabled={this.props.disabled}
        >
          <View
            style={[styles.header, this.props.headerStyle]}
            onLayout={this._setMinHeight.bind(this)}
          >
            <View style={styles.headerContent}>{this._renderHeader()}</View>
            <View style={[styles.arrow, this.props.indicatorStyle]}>
              {this._renderIcon()}
            </View>
          </View>
        </TouchableOpacity>
        <View onLayout={this._setMaxHeight.bind(this)}>
          {this.props.children}
        </View>
      </View>
    );
  }
}

export default Accordion;
