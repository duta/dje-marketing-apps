import EStyleSheet from "react-native-extended-stylesheet";

const styles = EStyleSheet.create({
  container: {
    borderWidth: 1,
    borderColor: "#CCCCCC",
    alignItems: "center",
    padding: 6,
    borderRadius: 3,
    justifyContent: "center"
  },
  text: {
    fontFamily: "Sukhumvit Set"
  }
});
export default styles;
