import React, { Component } from "react";
import { Image, Platform, Text, View } from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import PropTypes from "prop-types";

import styles from "./styles";

class ImageText extends Component {
  _renderImage = () => {
    const { icon, iconSize, iconColor, source, iconStyle } = this.props;
    const iconPrefix = Platform.ios ? "ios" : "md";
    let image = null;

    if (null !== source) {
      image = (
        <Image
          source={source}
          style={iconStyle}
          defaultSource={require("../../assets/icon/images/no-image.png")}
          borderRadius={this.props.imageRadius}
        />
      );
    } else {
      image = (
        <Icon
          name={`${iconPrefix}-${icon}`}
          size={iconSize}
          color={iconColor}
        />
      );
    }
    return image;
  };

  render() {
    const {
      icon,
      label,
      iconSize,
      iconColor,
      style,
      textColor,
      labelStyle
    } = this.props;

    return (
      <View style={[styles.container, style]}>
        {this._renderImage()}
        <Text style={[styles.text, labelStyle, { color: textColor }]}>
          {label}
        </Text>
      </View>
    );
  }
}

ImageText.propTypes = {
  icon: PropTypes.string,
  label: PropTypes.string,
  iconSize: PropTypes.number,
  iconColor: PropTypes.string,
  style: PropTypes.any,
  textColor: PropTypes.string,
  source: PropTypes.any,
  labelStyle: PropTypes.any
};

ImageText.defaultProps = {
  source: null,
  iconSize: 91
};

export default ImageText;
