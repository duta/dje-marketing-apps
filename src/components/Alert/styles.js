import EStyleSheet from "react-native-extended-stylesheet";

const styles = EStyleSheet.create({
  container: {
    alignSelf: "stretch",
    flexDirection: "row",
    alignItems: "center",
    alignContent: "center",
    paddingVertical: 4,
    paddingHorizontal: 8
  },
  text: {
    marginLeft: 8
  },

  container_default: {},
  text_default: {
    color: "#000000"
  },

  container_info: {
    borderColor: "#2288EF"
  },
  text_info: {
    color: "#2288EF"
  },

  container_warning: {
    borderColor: "orange"
  },
  text_warning: {
    color: "orange"
  },

  container_error: {},
  text_error: {
    color: "red"
  }
});

export default styles;
