import React, { Component } from "react";
import { Platform, Text, View } from "react-native";
import Icon from "react-native-vector-icons/Ionicons";

import styles from "./styles";

const platform = Platform.OS === "ios" ? "ios" : "md";

class Alert extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    let type = this.props.type || "default";
    let icon = "information";
    let color = "#2288EF";

    switch (type) {
      case "warning":
        icon = "alert";
        color = "rgb(255, 249, 149)";
        break;
      case "error":
        icon = "alert";
        color = "rgb(255, 59, 48)";
        break;
    }

    return (
      <View
        style={[
          this.props.style,
          styles.container,
          styles[`container_` + type]
        ]}
      >
        <Icon name={`${platform}-${icon}`} size={16} color={color} />
        <Text style={[styles.text, styles[`text_` + type]]}>
          {this.props.text}
        </Text>
      </View>
    );
  }
}

Alert.defaultProps = {
  style: {}
};

export default Alert;
