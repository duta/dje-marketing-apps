import { StyleSheet } from "react-native";
import { COLOR_GREY } from "../../config/common";

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: "white",
    borderRadius: 3,
    padding: 16
  },
  closeButton: {
    position: "absolute",
    top: -8,
    right: -8
  },
  headerWrapper: {
    paddingVertical: 6,
    marginBottom: 16
  },
  title: {
    color: COLOR_GREY,
    fontSize: 15
  }
});

export default styles;
