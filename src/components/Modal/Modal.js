import React from "react";
import Modal from "react-native-modal";
import PropTypes from "prop-types";
import styles from "./styles";
import { TouchableOpacity, View, Text } from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";
import { COLOR_GREY } from "../../config/common";

const ModalDialog = ({ isVisible, onClose, children, title }) => {
  return (
    <Modal
      isVisible={isVisible}
      onBackButtonPress={onClose}
      onBackdropPress={onClose}
    >
      <View style={styles.wrapper}>
        <View style={styles.headerWrapper}>
          <Text style={styles.title}>{title}</Text>
          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.closeButton}
            onPress={() => onClose()}
          >
            <Icon name="close" color={"#CCCCCC"} size={24} />
          </TouchableOpacity>
        </View>
        <View>{children}</View>
      </View>
    </Modal>
  );
};

ModalDialog.defaultProps = {
  isVisible: true,
  onClose: () => {}
};

ModalDialog.propTypes = {
  isVisible: PropTypes.bool,
  onClose: PropTypes.func,
  children: PropTypes.any
};

export default ModalDialog;
