import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    padding: 8,
    height: 56,
    justifyContent: "center",
    flexDirection: "row",
    alignItems: "center"
  },
  input: {
    alignSelf: "stretch",
    flex: 1,
    borderBottomColor: "transparent",
    backgroundColor: "white"
  },
  cancelText: {
    color: "white",
    paddingHorizontal: 8,
    fontWeight: "600"
  }
});

export default styles;
