import React from "react";
import PropTypes from "prop-types";
import { Text, View, TouchableOpacity } from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";
import { TextInputWithIcon } from "../TextInput";
import styles from "./styles";

class SearchBar extends React.Component {
  _onSubmit = () => {
    const { onSubmit } = this.props;
    if (undefined === onSubmit) return;

    onSubmit.bind(null, "");
  };

  render() {
    const {
      placeholder,
      onSubmit,
      backgroundColor,
      inputStyle,
      onCancel,
      onChangeText,
      text,
      actionSubmit
    } = this.props;

    const submit = <TouchableOpacity
                    activeOpacity={0.7}
                    onPress={onSubmit}
                    
                  >
                    <Icon size={24} name="check" color="white" />
                  </TouchableOpacity>;
    return (
      <View style={[styles.container, { backgroundColor: backgroundColor }]}>
        <TextInputWithIcon
          icon="search"
          style={[styles.input, inputStyle]}
          onSubmitEditing={this._onSubmit}
          onChangeText={onChangeText}
          value={text}
          autoFocus={true}
          placeholder={placeholder}
        />
        <TouchableOpacity activeOpacity={0.7} onPress={onCancel}>
          <Text style={styles.cancelText}>Cancel</Text>
        </TouchableOpacity>
        {actionSubmit && submit}
      </View>
    );
  }
}

SearchBar.propTypes = {
  placeholder: PropTypes.string,
  onSubmit: PropTypes.func,
  backgroundColor: PropTypes.string,
  inputStyle: PropTypes.any
};

SearchBar.defaultProps = {
  placeholder: "Search ..."
};

export default SearchBar;
