import React, { Component } from "react";
import { View, Text } from "react-native";
import PropTypes from "prop-types";
import { TextInputWithLabel } from "../TextInput";
import formStyles from "../../styles/form";
import styles from "./styles";
import { Button } from "../Button";
import { BUTTON_GRADIENT } from "../../config/common";

class UserForm extends Component {
  initialState = {
    fields: {
      email: null
    }
  };

  state = { ...this.initialState };

  componentDidMount() {
    this.setState({
      ...this.state,
      fields: {
        ...this.state.fields,
        email: this.props.email
      }
    });
  }

  updateTextField = field => value => {
    let newFields = { ...this.state.fields };
    newFields[field] = value;

    this.setState({
      ...this.state,
      fields: { ...newFields }
    });
  };

  static propTypes = {
    isEditing: PropTypes.bool
  };

  static defaultProps = {
    isEditing: false
  };

  submitButtonHandler = () => {
    const data = this.state.fields;
    if (this.props.onSubmit) this.props.onSubmit(data);
  };

  renderErrorMessage = field => {
    const { errors } = this.props;

    // No error
    if (typeof errors === "undefined" || typeof errors[field] === "undefined") {
      return null;
    } else {
      // Error found
      return <Text style={styles.errorMessage}> {errors[field][0]} </Text>;
    }
  };

  render() {
    const { isEditing } = this.props;
    var inputStyles = [formStyles.input];
    if (!isEditing) {
      inputStyles.push({
        backgroundColor: "#F2F2F2"
      });
    }

    return (
      <View style={styles.form}>
        <TextInputWithLabel
          label="Email"
          style={formStyles.inputWrapper}
          inputStyle={inputStyles}
          labelStyle={formStyles.inputLabel}
          editable={isEditing}
          value={this.state.fields.email}
          onChangeText={this.updateTextField("email")}
          autoCapitalize="none"
        />
        {this.renderErrorMessage("email")}
        {isEditing && (
          <TextInputWithLabel
            label={`Password${isEditing ? " Baru" : ""}`}
            style={formStyles.inputWrapper}
            inputStyle={inputStyles}
            labelStyle={formStyles.inputLabel}
            editable={isEditing}
            type="password"
            value={this.state.fields.password}
            onChangeText={this.updateTextField("password")}
          />
        )}

        {this.renderErrorMessage("password")}

        {isEditing && (
          <TextInputWithLabel
            label="Ulangi Password"
            style={formStyles.inputWrapper}
            inputStyle={inputStyles}
            labelStyle={formStyles.inputLabel}
            editable={isEditing}
            type="password"
            value={this.state.fields.password_confirmation}
            onChangeText={this.updateTextField("password_confirmation")}
          />
        )}
        {this.renderErrorMessage("password_confirmation")}

        {isEditing && (
          <Button
            gradient={BUTTON_GRADIENT}
            gradientDirection="horizontal"
            label="Simpan"
            style={[formStyles.button, { marginTop: 16 }]}
            onPress={this.submitButtonHandler}
          />
        )}
      </View>
    );
  }
}

export default UserForm;
