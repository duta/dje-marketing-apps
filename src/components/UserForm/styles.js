import { StyleSheet } from "react-native";
import { COLOR_SOFTEN_RED } from "../../config/common";

const styles = StyleSheet.create({
  form: {
    alignSelf: "stretch"
  },
  errorMessage: {
    color: COLOR_SOFTEN_RED,
    fontSize: 12
  }
});

export default styles;
