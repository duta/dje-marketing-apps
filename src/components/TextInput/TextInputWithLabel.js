import PropTypes from "prop-types";
import React from "react";
import { Text, View } from "react-native";
import TextInput from "./TextInput";
import styles from "./styles";

class TextInputWithLabel extends React.Component {
  render() {
    const {
      style,
      inputStyle,
      labelStyle,
      label,
      ...props
    } = this.props;
    return (
      <View style={[styles.inputContainer, style]}>
        <Text style={[styles.label, labelStyle]}>{label}</Text>
        <TextInput style={inputStyle} {...props} />
      </View>
    );
  }
}

TextInputWithLabel.propTypes = {
  label: PropTypes.string,
  inputType: PropTypes.string,
  style: PropTypes.any,
};

TextInputWithLabel.defaultProps = {
  label: null,
  inputType: "default",
  style: {},
  onFocus: () => {},
  labelStyle: {}
};

export default TextInputWithLabel;
