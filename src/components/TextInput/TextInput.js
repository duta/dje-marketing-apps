import React, { Component } from "react";
import {
  Text,
  TextInput as ReactInput,
  TouchableWithoutFeedback,
  TouchableOpacity,
  View,
  Platform
} from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";
import styles from "./styles";

class TextInput extends Component {
  static defaultProps = {
    iconColor: "#999"
  };

  state = {
    secureTextEntry: true
  };

  constructor(props) {
    super(props);
  }

  getSecureTextEntry = () => {
    const { type } = this.props;
    if (undefined === type || null === type || type !== "password")
      return false;

    return this.state.secureTextEntry;
  };

  toggleSecureTextEntry = () => {
    const { type } = this.props;
    if (undefined === type || null === type || type !== "password") return;

    this.setState({
      secureTextEntry: !this.state.secureTextEntry
    });
  };

  renderIcon = () => {
    const { type } = this.props;
    if (undefined === type || null === type || type !== "password") return null;

    const icon = this.state.secureTextEntry ? "visibility-off" : "visibility";
    return (
      <TouchableWithoutFeedback
        onPress={this.toggleSecureTextEntry}
        style={styles.addOn}
      >
        <Icon size={20} name={icon} color={this.props.iconColor} />
      </TouchableWithoutFeedback>
    );
  };

  renderTextInput() {
    const { style, onTouchStart, ...props } = this.props;
    if (Platform.OS === "android" && onTouchStart) {
      return (
        <TouchableOpacity 
          activeOpacity={0.8}
          onPress={onTouchStart}
          style={{alignSelf: "stretch", flex: 1}}
        >
          <ReactInput
            {...props}
            style={[styles.input, style, {alignSelf: "stretch"}]}
            underlineColorAndroid="transparent"
            secureTextEntry={this.getSecureTextEntry()}
          />
        </TouchableOpacity>
      );
    }

    return (
      <ReactInput
        {...props}
        style={[styles.input, style]}
        underlineColorAndroid="transparent"
        secureTextEntry={this.getSecureTextEntry()}
        onTouchStart={onTouchStart}
      />
    );
  }

  render() {
    var { style, secureTextEntry, ...props } = this.props;

    if (this.props.type === "password") {
      props.autoCapitalize = "none";
    }

    return (
      <View style={[styles.wrapper, this.props.wrapperStyle]}>
        {this.renderTextInput()}
        {this.renderIcon()}
      </View>
    );
  }
}

export default TextInput;
