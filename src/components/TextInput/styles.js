import EStyleSheet from "react-native-extended-stylesheet";

const styles = EStyleSheet.create({
  inputContainer: {
    alignSelf: "stretch"
  },
  input: {
    padding: 6,
    color: "black",
    flex: 1
  },
  label: {
    marginBottom: 4
  },
  inputIconContainer: {
    flexDirection: "row",
    borderBottomWidth: 1,
    borderBottomColor: "$borderInputNormal",
    alignItems: "center"
  },
  inputIcon: {
    flex: 1,
    fontSize: 14,
    height: 24,
    paddingVertical: 0,
    marginVertical: 4,
    borderWidth: 0,
    borderColor: "transparent"
  },
  iconContainer: {
    paddingVertical: 8,
    paddingHorizontal: 12,
    marginRight: 2,
    width: 42
  },
  wrapper: {
    flexDirection: "row",
    borderBottomColor: "#CCC",
    borderBottomWidth: 1,
    alignSelf: "stretch",
    alignItems: "center",
    overflow: "hidden"
  },
  addOn: {
    position: "absolute",
    top: 0,
    right: 8
  }
});

export default styles;
