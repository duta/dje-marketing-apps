import PropTypes from "prop-types";
import React from "react";
import { Platform, TextInput as ReactInput, Text, View } from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import styles from "./styles";
import {
  COLOR_INPUT_BORDER_NORMAL,
  COLOR_INPUT_BORDER_FOCUS
} from "../../config/common";

const ICON_PREFIX = Platform.OS === "ios" ? "ios" : "md";

const TextInputWithIcon = ({ icon, iconSize, inputType, style, ...props }) => (
  <View style={[styles.inputIconContainer, style]}>
    <View style={styles.iconContainer}>
      <Icon
        name={`${ICON_PREFIX}-${icon}`}
        size={iconSize}
        color={COLOR_INPUT_BORDER_NORMAL}
        style={styles.icon}
      />
    </View>
    <ReactInput
      style={styles.inputIcon}
      underlineColorAndroid="transparent"
      keyboardType={inputType}
      {...props}
    />
  </View>
);

TextInputWithIcon.propTypes = {
  icon: PropTypes.string,
  inputType: PropTypes.string,
  style: PropTypes.any,
  placeholder: PropTypes.string,
  iconSize: PropTypes.number,
  editable: PropTypes.bool,
  onSubmitEditing: PropTypes.func
};

TextInputWithIcon.defaultProps = {
  icon: null,
  inputType: "default",
  style: {},
  placeholder: null,
  iconSize: 18,
  editable: true,
  onSubmitEditing: () => {}
};

export default TextInputWithIcon;
