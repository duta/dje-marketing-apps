import EStyleSheet from "react-native-extended-stylesheet";
import { Dimensions } from "react-native";

const containerHeight = Dimensions.get("window").height;

const styles = EStyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF",
    alignItems: "center",
    justifyContent: "center",
    padding: 16,
    alignSelf: "stretch"
  }
});

export default styles;
