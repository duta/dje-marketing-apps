import React, { Component } from "react";
import PropTypes from "prop-types";
import { 
  Keyboard,
  KeyboardAvoidingView, 
  Platform,
  StatusBar, 
  ScrollView, 
  RefreshControl,
  View
} from "react-native";
import styles from "./styles";
import { COLOR_PRIMARY } from "../../config/common";

class Container extends Component {

  constructor(props) {
    super(props);

    this.handleRefresh = this.handleRefresh.bind(this);
  }

  handleRefresh() {
    if (this.props.onRefresh) {
      this.props.onRefresh();
    }
  }

  render() {
    const { children, style, scrollEnabled } = this.props;
    return (
      <ScrollView
        style={{ flex: 1, alignSelf: "stretch" }}
        contentContainerStyle={{ flexGrow: 1 }}
        scrollEnabled={scrollEnabled}
        keyboardShouldPersistTaps="always"
        refreshControl={
          <RefreshControl
            refreshing={this.props.refreshing}
            onRefresh={this.handleRefresh}
            tintColor={COLOR_PRIMARY}
          />
        }
      >
        <View style={[styles.container, style]}>
          <StatusBar translucent={false} barStyle="default" />
          {children}
        </View>
      </ScrollView>
    );
  }
}

Container.propTypes = {
  children: PropTypes.any
};

Container.defaultProps = {
  scrollEnabled: true
};

export default Container;
