import React from "react";
import ImageViewer from "react-native-image-zoom-viewer";
import { Modal } from "react-native";
import PropTypes from "prop-types";

const ImagePreview = props => {
  const images = props.imageSources.map(imageSource => ({
    url: imageSource
  }));

  return (
    <Modal
      visible={props.isVisible}
      transparent={true}
      onRequestClose={props.onClose}
    >
      <ImageViewer
        imageUrls={images}
        enableSwipeDown={true}
        onSwipeDown={props.onClose}
      />
    </Modal>
  );
};

ImagePreview.propTypes = {
  isVisible: PropTypes.bool,
  imageSources: PropTypes.array,
  onClose: PropTypes.func.isRequired
};

ImagePreview.defaultProps = {
  imageSources: []
}

export default ImagePreview;
