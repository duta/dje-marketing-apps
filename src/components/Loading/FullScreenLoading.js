import React from 'react'
import { View, Modal } from 'react-native'
import Loading from "./Loading";
import styles from "./styles";

const FullScreenLoading = props => {
    return (
        <Modal
            visible={props.isVisible}
            animationType={"fade"}
            transparent
        >
            <View style={styles.container}>
                <Loading {...props} />
            </View>
        </Modal>
    )
}

export default FullScreenLoading;
