import React from 'react';
import { View } from 'react-native';
import Loading from "./Loading";
import styles from "./styles";

const FullPageLoading = props => {
    if (!props.isVisible) return null;

    return (
        <View style={styles.fullPage}>
            <Loading {...props} />
        </View>
    );
}

export default FullPageLoading;

