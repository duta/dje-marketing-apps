import Loading from "./Loading";
import FullScreenLoading from "./FullScreenLoading";
import FullPageLoading from "./FullPageLoading";

export { 
    FullPageLoading, 
    FullScreenLoading, 
    Loading 
};
