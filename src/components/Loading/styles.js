import EStyleSheet from "react-native-extended-stylesheet";
import { Dimensions } from "react-native";
import { COLOR_PRIMARY } from "../../config/common";

const styles = EStyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(0, 0, 0, .6)",
    height: Dimensions.get("window").height,
    width: Dimensions.get("window").width,
    alignItems: "center",
    justifyContent: "center",
    position: "absolute",
    top: 0,
    left: 0,
  },
  loadingContainer: {
    alignSelf: "stretch",
    alignItems: "center",
    justifyContent: "center"
  },
  text: {
    marginTop: 16,
    textAlign: "center",
    fontSize: 16,
    color: COLOR_PRIMARY,
    fontFamily: "Quicksand",
    fontWeight: "bold"
  },
  image: {
    width: 60,
    height: 60,
    borderRadius: 30
  },
  fullPage: {
    flex: 1,
    alignSelf: "stretch",
    alignItems: "center",
    justifyContent: "center",
    padding: 16
  }
});

export default styles;
