import React, { Component } from "react";
import { Image, Text, View } from "react-native";
import Modal from "react-native-modal";
import PropTypes from "prop-types";
import styles from "./styles";
import { Spinner } from "native-base";
import { COLOR_PRIMARY } from "../../config/common";

class Loading extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { text, isVisible, style } = this.props;

    if (!isVisible) return null;

    return (
      <View style={styles.loadingContainer}>
        {/* <Image
          source={require("../../assets/images/loading-new.gif")}
          style={[styles.image, { overlayColor: "contain" }]}
          borderRadius={30}
        /> */}
        <Spinner color={COLOR_PRIMARY} />
        <Text style={styles.text}>{ text }</Text>
      </View>
    );
  }
}

Loading.propTypes = {
  text: PropTypes.string,
  isVisible: PropTypes.bool.isRequired,
  style: PropTypes.any
};

export default Loading;
