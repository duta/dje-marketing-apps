import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    height: 56,
    flexDirection: "row",
    alignItems: "center"
  },
  headerLeft: {
    width: 56
  },
  headerRight: {
    width: 56
  },
  title: {
    fontWeight: "600",
    fontSize: 17,
    textAlign: "center",
    flex: 1
  }
});

export default styles;
