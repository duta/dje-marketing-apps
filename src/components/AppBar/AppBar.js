import React, { Component } from "react";
import { Text, View } from "react-native";
import PropTypes from "prop-types";
import styles from "./styles";

/**
 * AppBar Component
 *
 * @prop backgroundColor
 * @prop title
 * @prop titleColor
 *
 */
class AppBar extends Component {
  static propTypes = {
    backgroundColor: PropTypes.string,
    title: PropTypes.string,
    titleColor: PropTypes.string
  };

  static defaultProps = {
    backgroundColor: "#CCC",
    titleColor: "#000"
  };

  constructor(props) {
    super(props);
  }

  render() {
    const {
      backgroundColor,
      title,
      titleColor,
      headerLeft,
      headerRight
    } = this.props;

    return (
      <View style={[styles.container, { backgroundColor: backgroundColor }]}>
        {headerLeft && <View style={styles.headerLeft}>{headerLeft}</View>}
        <Text style={[styles.title, { color: titleColor }]}>{title}</Text>
        {headerRight && <View style={styles.headerRight}>{headerRight}</View>}
      </View>
    );
  }
}

export default AppBar;
