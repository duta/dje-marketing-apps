import React, { Component } from "react";
import { StyleSheet, View } from "react-native";

const Separator = props => <View style={styles.separator} />;

const styles = StyleSheet.create({
  separator: {
    marginVertical: 16,
    borderTopWidth: 0.6,
    borderTopColor: "rgba(117, 123, 149, 0.5)",
    alignSelf: "stretch"
  }
});

export default Separator;
