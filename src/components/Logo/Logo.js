import React, { Component } from 'react';
import {StyleSheet,Text,View,Image} from 'react-native';
import { COLOR_PRIMARY,COLOR_SECONDARY } from "../../config/common";


export default class Logo extends Component<{}> {

    render(){
        return(
            <View style={styles.container}>
            <Image  style={{width:70, height: 70}}
            source={require('../../assets/logo/logo.jpg')}/>
            <Text style={styles.logoText}>Welcome to DJE app.</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container : {
        flex: 1,
        justifyContent:'flex-end',
        alignItems: 'center'
    },

    logoText : {
        marginVertical: 15,
        fontSize:18,
        color:COLOR_SECONDARY
    }

});
