import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  modal: {
    justifyContent: "flex-end",
    margin: 0
  },
  uploadOptions: {
    alignSelf: "flex-end",
    backgroundColor: "white",
    padding: 16,
    alignSelf: "stretch"
  }
});

export default styles;
