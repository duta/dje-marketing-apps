import React, { Component } from "react";
import PropTypes from "prop-types";
import { View, Text } from "react-native";
import ImagePicker from "react-native-image-crop-picker";
import Modal from "react-native-modal";
import _ from "lodash";
import { Button, GradientBorderButton } from "../../components/Button";

import styles from "./styles";
import formStyles from "../../styles/form";
import { BUTTON_GRADIENT, COLOR_PRIMARY } from "../../config/common";

class UploadImage extends Component {
  options = {
    cropping: this.props.cropping,
    multiple: this.props.multiple,
    width: 400,
    height: 400,
    mediaType: "photo",
    compressImageQuality: this.props.compressImageQuality
  };

  callback = response => {
    if (response.didCancel) {
      return;
    }

    if (response.error) {
      if (this.props.onError && _.isFunction(this.props.onError)) {
        this.props.onError(response.error);
      }
      return;
    }

    if (
      this.props.onImageSelected &&
      _.isFunction(this.props.onImageSelected)
    ) {
      this.props.onImageSelected(response);
    }
  };

  onError = (error) => {
    if (_.isFunction(this.props.onError)) {
      this.props.onError(error);
    }
  }

  onImageSelected = (image) => {
    if (_.isFunction(this.props.onImageSelected)) {
      this.props.onImageSelected(image);
    }
  }

  browseFromGallery = () => {
    ImagePicker.openPicker(this.options)
      .then(this.onImageSelected)
      .catch(this.onError);
  };

  openCamera = () => {
    ImagePicker.openCamera(this.options)
      .then(this.onImageSelected)
      .catch(this.onError);
  };

  render() {
    return (
      <Modal
        isVisible={this.props.isVisible}
        style={styles.modal}
        backdropTransitionOutTiming={700}
      >
        <View style={styles.uploadOptions}>
          <Button
            style={[formStyles.button, { marginBottom: 16 }]}
            gradient={BUTTON_GRADIENT}
            label="Kamera"
            onPress={this.openCamera}
          />

          <GradientBorderButton
            label="Upload dari Galeri"
            containerStyle={{
              height: 38,
              alignSelf: "stretch",
              marginBottom: 16
            }}
            style={formStyles.button}
            textColor={COLOR_PRIMARY}
            backgroundColor={"#FFFFFF"}
            borderGradient={BUTTON_GRADIENT}
            borderWidth={1}
            onPress={this.browseFromGallery}
          />

          <View style={styles.separator} />

          <GradientBorderButton
            label="Tutup"
            containerStyle={{
              height: 38,
              alignSelf: "stretch",
              marginBottom: 16
            }}
            style={formStyles.button}
            textColor={COLOR_PRIMARY}
            backgroundColor={"#FFFFFF"}
            borderGradient={BUTTON_GRADIENT}
            borderWidth={1}
            onPress={() => {
              this.props.onClose();
            }}
          />
        </View>
      </Modal>
    );
  }
}

UploadImage.propTypes = {
  isVisible: PropTypes.bool,
  title: PropTypes.string,
  onClose: PropTypes.func,
  onImageSelected: PropTypes.func,
  onError: PropTypes.func,
  cropping: PropTypes.bool,
  multiple: PropTypes.bool,
};

UploadImage.defaultProps = {
  isVisible: false,
  title: "Upload Image",
  cropping: true,
  multiple: false,
  compressImageQuality: 1
};

export default UploadImage;
