import React, { Component } from 'react';

class CustomComponent extends Component {
  

  constructor(props) {
    super(props);
    this.listeners = [];

    this.addNavigationListeners(props);
  }

  /**
   * Menambahkan listener untuk navigation event (willFocus dan didFocus)
   * 
   * @param {*} props 
   */
  addNavigationListeners(props) {
    const events = [
      {name: 'willFocus', handler: this.componentWillFocus},
      {name: 'didFocus', handler: this.componentDidFocus}
    ];

    if (props.navigation) {
      events.forEach(event => {
        if (event.handler) {
          this.listeners.push(
            props.navigation.addListener(event.name, event.handler.bind(this))
          );
        }
      });
    }
  }

  removeNavigationListeners() {
    if (this.listeners.length > 0) {
      this.listeners.forEach(listener => {
        listener.remove();
      });

      this.listeners = [];
    }
  }

  componentWillUnmount() {
    this.removeNavigationListeners();
  }

  componentDidFocus() {}
  componentWillFocus() {}
}

export default CustomComponent;
