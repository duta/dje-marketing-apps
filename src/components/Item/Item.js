import React from "react";
import { View, Text, StyleSheet } from "react-native";

const Item = props => (
  <View style={[styles.content, props.style]}>
    <Text style={styles.contentLabel}>{props.label}</Text>
    {props.value && <Text style={styles.contentValue}>{props.value}</Text>}
    {props.desc && <Text style={styles.contentDesc}>{props.desc}</Text>}
  </View>
);

const styles = StyleSheet.create({
  content: {
    marginBottom: 8
  },
  contentLabel: {
    color: "rgba(117, 123, 149, 0.7)",
    fontSize: 13,
    fontFamily: "Quicksand"
  },
  contentValue: {
    color: "rgba(117, 123, 149, 1)",
    fontSize: 14,
    fontWeight: "500",
    fontFamily: "Quicksand"
  },
  contentDesc: {
    color: "rgba(117, 123, 149, 1)",
    fontSize: 14,
    fontFamily: "Quicksand",
    fontWeight: "400"
  }
});

export default Item;
