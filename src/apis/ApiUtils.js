const REQUEST_OK = 200;
const UNAUTHORIZED_ERROR = 401;
const FORBIDDEN_ERROR = 403;
const NOT_FOUND_ERROR = 404;
const VALIDATION_ERROR = 422;
const INTERNAL_SERVER_ERROR = 500;

var ApiUtils = {
  _createErrorMessages: (code, messages) => {
    return {
      code: code,
      status: "error",
      message: messages,
      errors: [
        {
          message: messages
        }
      ]
    };
  },
  handleErrors: error => {
    // Network error
    if (typeof error.response === "undefined") {
      return error.message;
    }

    if (error.response.data) {
      if (error.response.data.errors) return error.response.data.errors;

      if (error.response.data.message) return error.response.data.message;
    }

    if (error.response.message) {
      return error.response.message;
    }

    return "Terjadi error yang tidak diketahui";
  },
  checkStatus: response => {
    if (response.status >= REQUEST_OK && response.status < 300) {
      return response;
    }
    return ApiUtils.handleErrors(response);
  }
};

export default ApiUtils;
