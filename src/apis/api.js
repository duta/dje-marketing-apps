import axios from "axios";
import { API_ROOT, API_PREFIX } from "../config/api";

const Api = {
  setAuthToken: token => {
    if (undefined === token || null === token) return;
    axios.defaults.headers.common["Authorization"] = `${token.token_type} ${
      token.access_token
    }`;
  },

  getUrl: (path, prefix = API_PREFIX) => {
    return `${API_ROOT}${prefix}/${path}`;
  },

  get: (path, params = {}, headers = {}, auth = false, prefix) => {
    return axios.get(Api.getUrl(path, prefix), {
      headers,
      params
    });
  },

  post: (path, body = {}, params = {}, headers = {}, auth = false, prefix) => {
    return axios.post(Api.getUrl(path, prefix), body, {
      headers,
      params
    });
  },

  put: (path, body = {}, params = {}, headers = {}, auth = false, prefix) => {
    return axios.put(Api.getUrl(path, prefix), body, {
      headers,
      params
    });
  },

  patch: (path, body = {}, params = {}, headers = {}, auth = false, prefix) => {
    return axios.patch(Api.getUrl(path, prefix), body, {
      headers,
      params
    });
  },

  delete: (path, params = {}, headers = {}, auth = false, prefix) => {
    return axios.delete(Api.getUrl(path, prefix), {
      headers,
      params
    });
  }
};

export default Api;
