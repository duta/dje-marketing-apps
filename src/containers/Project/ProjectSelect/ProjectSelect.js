import React, { Component } from "react";
import { connect } from "react-redux";
import _ from "lodash";
import { FlatList, View, Text, TouchableOpacity, RefreshControl,SafeAreaView } from "react-native";
import { NavigationActions, HeaderProps } from "react-navigation";
import Icon from "react-native-vector-icons/MaterialIcons";
import CustomComponent from "../../../components/CustomComponent";
import Container from "../../../components/Container";
import containerStyle from "../../../styles/container";
import styles from "./styles";
import { fetchProjects,resetProject } from "../project.action";
import { Loading, FullPageLoading } from "../../../components/Loading";
import ActionButton from "../../../components/ActionButton";
import {
  COLOR_PRIMARY,
  COLOR_SOFTEN_RED
} from "../../../config/common";
import SearchBar from "../../../components/SearchBar";

class ProjectSelect extends CustomComponent {
  
  constructor(props) {
    super(props);

    this.requestData = this.requestData.bind(this);
    this.handleLoadMore = this.handleLoadMore.bind(this);
    this.refreshData = this.refreshData.bind(this);

    this.state = {
      selected: null,
      loading: false,
      type:null,
      currentPage:1,
      searching: false,
      searchText: null,
    }
  }

  // static navigationOptions = ({ navigation }) => {
  //   const { action } = navigation.state.params;
  //   return {
  //     title: "Pilih Project",
  //     headerRight: (
  //       <TouchableOpacity
  //         activeOpacity={0.7}
  //         style={{ paddingHorizontal: 16 }}
  //         onPress={action}
  //       >
  //         <Icon size={24} name="check" color="white" />
  //       </TouchableOpacity>
  //     )
  //   };
  // };

  static navigationOptions = ({ navigation }) => ({
    header: navigation.state.params.searchBar || HeaderProps,
    headerRight: (
      <View style={{ paddingHorizontal: 16 }}>
        {navigation.state.params.right !== undefined
          ? navigation.state.params.right
          : null}
      </View>
    )
  });

  /**
   * Show search button
   *
   */
  _showSearchButton = () => {
    const { searching } = this.state;
    if (!searching) {
      let margin = 16;
      let add_button = 
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={this.handleButtonCheck}
        >
          <Icon size={24} name="check" color="white" />
        </TouchableOpacity>;
      this.props.navigation.setParams({
        right: (
          <View style={{ flexDirection: "row" }}>
            <ActionButton
              onPress={this._showSearchForm}
              icon="search"
              style={{ marginRight: margin }}
            />
            {add_button}
          </View>
        )
      });
    }
  };

  /**
   * Do search
   *
   */
  doSearch = text => {
    this.setState({ searchText: text });
    this.props.dispatch(resetProject());
    this.refreshData();
  };

  /**
   * Cancel search
   *
   */
  cancelSearch = () => {
    this.props.navigation.setParams({ searchBar: null });
    this.setState({ searching: false, searchText: null });
    this.props.dispatch(resetProject());
    this.refreshData();
  };

  /**
   * Show search form on header
   *
   */
  _showSearchForm = () => {
    this.setState({ searching: true });
    this.props.navigation.setParams({
      searchBar: (
        <SafeAreaView style={{backgroundColor: COLOR_PRIMARY}}>
          <SearchBar
            backgroundColor={COLOR_PRIMARY}
            placeholder="Ketikan kota/kabupaten"
            onCancel={this.cancelSearch}
            onChangeText={this.doSearch}
            text={this.state.searchText}
            inputStyle={{width:20}}
            onSubmit={this.handleButtonCheck}
            actionSubmit={true}
          />
        </SafeAreaView>
      )
    });
  };

  onItemSelected = project => {
    let selected;
    if (this.state.type == "single") {
      selected = project;
    }else if(this.state.type == "multy"){
      selected = [...this.state.selected];
      const itemIndex = this.state.selected.indexOf(project);
      if (itemIndex === -1) {
        selected.push(project);
      } else {
        selected.splice(itemIndex, 1);
      }
    }

    this.setState({
      selected: selected
    });
    
  };

  handleButtonCheck = () => {
    if (this.state.selected === null) return;

    const { dispatch } = this.props;
    dispatch(NavigationActions.back());

    const { callback } = this.props.navigation.state.params;

    if (callback !== undefined && _.isFunction(callback)) {
      callback(this.state.selected);
    }
  };

  componentWillFocus() {
    const { type } = this.props.navigation.state.params;
    let selected;
    if (type == "single") {
      selected = null;
    }else if(type == "multy"){
      selected = [];
    }
    this.setState({
      loading: true,
      type: type,
      selected : selected
    });
  }

  componentDidFocus() {
    this.setState({loading: false});
    this._showSearchButton();
    this.requestData();
  }

  refreshData(){
    this.setState({
      currentPage: 1,
    },() => {this.requestData()});
  }

  requestData() {
    const params = {
      page : this.state.currentPage,
      search : this.state.searchText,
    }
    this.props.dispatch(fetchProjects(params));
  }

  handleLoadMore() {
    if (this.props.lastPage !== this.state.currentPage) {
      this.setState({
        currentPage: parseInt(this.state.currentPage) + 1,
      },() => {this.requestData()});
    }
  };

  renderList = () => {
    const projects = Object.values(this.props.projects) || [];
    const listItem = ({item}) => {
      var selected = this.state.selected;
      var check = styles.uncheck;

      if (this.state.type == "single") {
        if (_.isEqual(item, this.state.selected)) check = styles.check;
      }else if(this.state.type == "multy"){
        if (_.includes(selected, item)) {
          check = styles.check;
        }
      }

      return (
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => {
            this.onItemSelected(item);
          }}
        >
          <View style={styles.listItem}>
            <Text style={styles.nama}>{item.name}</Text>
            <View style={check}>
              <Icon name="check" color="white" size={16} />
            </View>
          </View>
        </TouchableOpacity>
      );
    };

    return (
      <FlatList
        data={projects}
        renderItem={listItem}
        style={styles.list}
        removeClippedSubviews={false}
        extraData={this.state.selected}
        keyExtractor={(item, index) => `key-${index}`}
        onEndReached={this.handleLoadMore}
        onEndReachedThreshold={0.3}
        refreshControl={
          <RefreshControl
            refreshing={this.props.fetching}
            onRefresh={this.refreshData}
          />
        }
      />
    );
  };

  render() {
    return (
      <Container
        style={containerStyle.container}
        refreshing={this.props.fetching || this.state.loading}
        onRefresh={this.requestData}
      >
        {this.renderList()}

        {/* <FullPageLoading 
          text="Mengambil data project ..."
          isVisible={this.props.fetching || this.state.loading}
        /> */}
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    projects: state.project.list,
    fetching: state.project.isFetching,
    lastPage: state.project.lastPage
  };
};

export default connect(mapStateToProps)(ProjectSelect);
