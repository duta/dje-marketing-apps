import { all, call, put, takeLatest } from "redux-saga/effects";
import {
  PROJECTS_FETCHED,
  FETCH_PROJECTS_FAIL,
  FETCH_PROJECTS,
  ADD_PROJECT,
  ADD_PROJECT_FAIL,
  PROJECT_ADDED,
  PROJECT_UPDATED,
  UPDATE_PROJECT_FAIL,
  UPDATE_PROJECT,
  RESET_PROJECT
} from "./project.actionTypes";
import Api from "../../apis/api";
import { API_V3_PREFIX, API_VERSION } from "../../config/api";
import ApiUtils from "../../apis/ApiUtils";

function fetchProjects(params) {
  if (params.customerId) {
    return Api.get(`customer/${params.customerId}/project`, params);
  }
  return Api.get(`project`, params, {}, true);
  // return require('../../data/activity.json');
}

function* handleFetchingProjects(action) {
  try {
    const response = yield call(fetchProjects, action.params);
    const data = response.data;

    yield put({
      type: PROJECTS_FETCHED,
      data
    });
  } catch (error) {
    yield put({
      type: FETCH_PROJECTS_FAIL,
      errors: ApiUtils.handleErrors(error)
    });
  }
}

function addProject(data) {
  return Api.post("project", data);
}

function* handleAddingProject(action) {
  try {
    const response = yield call(addProject, action.data);
    const data = response.data.data;

    yield put({
      type: RESET_PROJECT,
    });

    yield put({
      type: FETCH_PROJECTS,
      params: {
        customerId:null
      }
    });
  } catch (error) {
    yield put({
      type: ADD_PROJECT_FAIL,
      errors: ApiUtils.handleErrors(error)
    });
  }
}

function updateProject(id, data) {
  return Api.patch(`project/${id}`, data);
}

function* handleUpdatingProject(action) {
  try {
    const response = yield call(updateProject, action.id, action.data);
    const data = response.data.data;

    yield put({
      type: RESET_PROJECT,
    });
    
    yield put({
      type: FETCH_PROJECTS,
      params: {
        customerId:null
      }
      // type: PROJECT_UPDATED,
      // data
    });
  } catch (error) {
    yield put({
      type: UPDATE_PROJECT_FAIL,
      errors: ApiUtils.handleErrors(error)
    });
  }
}

export default function* watchAll() {
  yield all([
    takeLatest(FETCH_PROJECTS, handleFetchingProjects),
    takeLatest(ADD_PROJECT, handleAddingProject),
    takeLatest(UPDATE_PROJECT, handleUpdatingProject),
  ]);
}
