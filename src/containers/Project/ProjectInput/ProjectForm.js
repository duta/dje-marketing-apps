import React, { Component } from "react";
import {
  Keyboard,
  KeyboardAvoidingView,
  ScrollView,
  Text,
  View,
} from "react-native";
import { TextInputWithLabel } from "../../../components/TextInput";
import { Select } from "../../../components/Select";
import formStyles from "../../../styles/form";
import styles from "./styles";
import _ from "lodash";
import DatePicker from "react-native-modal-datetime-picker";
import PropTypes from "prop-types";
import { formatDate,getToday } from "../../../utils/date";
import { clean } from "../../../utils/array";
import { pad } from "../../../utils/string";
import { formatMoney } from "../../../utils/number";
import { NavigationActions } from "react-navigation";
import { connect } from "react-redux";
import { BUTTON_GRADIENT, COLOR_GREY } from "../../../config/common";
import { Button } from "../../../components/Button";

let initialState = {
  fields: {
    name: null,
    regency_id: null,
    marketing_id:null,
    consultan_plan:null,
    consultan_super:null,
    contractor:null,
    contractor_sub:null,
    status: null,
    address: null,
    description:null,
    date: getToday(),
    due_date: getToday(),
    price:0
  }
  
};

class ProjectForm extends Component {
  state = {
    fields: initialState.fields,
    showDatePicker: false,
    showDueDatePicker: false,
    showImagePicker: false,
    pickAttachment: false,
    selectedRegency:null,
    selectedConsultanPlan: null,
    selectedConsultanSuper: null,
    selectedContractor: null,
    selectedSubContractor: null,
    selectedMarketing: null
  };

  static propTypes = {
    onImagePickerSelected: PropTypes.func,
    onImagePickerError: PropTypes.func
  };

  static defaultProps = {
    errors: []
  };

  initializeData = (fields) => {

    switch (fields.status) {
      case 0:
        fields["status_index"] = 0;
        break;
      case 1:
        fields["status_index"] = 1;
        break;
      case 2:
        fields["status_index"] = 2;
        break;
      case 3:
        fields["status_index"] = 3;
        break;  
      default:
        fields["status_index"] = null;
        break;
    }
    
    return fields;
  };

  componentDidMount() {
    const { project, marketing } = this.props;
    let regency;
    let marketing_data;
    let consultan_plan;
    let consultan_super;
    let contractor;
    let contractor_sub;
    if (project) {
      regency = project.regency ? project.regency : null;
      marketing_data = project.marketing ? project.marketing : marketing;
      consultan_plan = project.consultan_plan_data ? project.consultan_plan_data :null;
      consultan_super = project.consultan_super_data ? project.consultan_super_data :null;
      contractor = project.contractor_data ? project.contractor_data :null;
      contractor_sub = project.contractor_sub_data ? project.contractor_sub_data :null;
    }else{
      regency = null;
      marketing_data = marketing;
      consultan_plan = null;
      consultan_super = null;
      contractor = null;
      contractor_sub = null;
    }
        
    var fields = {
      ... this.state.fields,
      ... project,
      marketing_id: marketing_data.id,
      consultan_plan: consultan_plan ? consultan_plan.id : null,
      consultan_super: consultan_super ? consultan_super.id : null,
      contractor: contractor ? contractor.id : null,
      contractor_sub: contractor_sub ? contractor_sub.id : null,
    };

    fields = this.initializeData(fields);

    this.setState({
      ...this.state,
      selectedRegency : regency,
      selectedMarketing : marketing_data,
      selectedConsultanPlan : consultan_plan,
      selectedConsultanSuper : consultan_super,
      selectedContractor : contractor,
      selectedSubContractor : contractor_sub,
      fields
    });
  }

  resetFields = () => {
    const fields = {
      name: null,
      regency_id: null,
      marketing_id:null,
      consultan_plan:null,
      consultan_super:null,
      contractor:null,
      contractor_sub:null,
      status: null,
      address: null,
      description:null,
      date: getToday(),
      due_date: getToday(),
      price:0
    };

    initialState.fields = fields;
    this.setState({ fields });
  };

  componentWillUnmount() {
    this.resetFields();
  }

  onSubmit() {
    if (typeof this.props.onSubmit === "function") {
      const data = {
        ...this.state.fields,
      };
      this.props.onSubmit(clean(data));
    }
  }

  updateTextField = field => value => {
    let newFields = { ...this.state.fields };
    newFields[field] = value;
    if (field == 'price') {
      newFields[field] = value.replace(/\./g,"");
      if (!value || value == "NaN") {
        newFields[field] = parseInt(0);
      }
    }
    this.setState({
      ...this.state,
      fields: { ...newFields }
    });
  };

  updatePickerField = field => (value, index) => {
    let newFields = { ...this.state.fields };
    newFields[field] = value.value;
    newFields[field + "_index"] = index;

    this.setState({
      ...this.state,
      fields: { ...newFields }
    });
    
  };

  renderErrorMessage = field => {
    const { errors } = this.props;

    // No error
    if (typeof errors === "undefined" || typeof errors[field] === "undefined") {
      return null;
    } else {
      // Error found
      return <Text style={styles.errorMessage}> {errors[field][0]} </Text>;
    }
  };

  showDatePicker = () => {
    Keyboard.dismiss();
    this.setState({
      showDatePicker: true
    });
  };

  hideDatePicker = () => {
    this.setState({
      showDatePicker: false
    });
  };

  showDueDatePicker = () => {
    Keyboard.dismiss();
    this.setState({
      showDueDatePicker: true
    });
  };

  hideDueDatePicker = () => {
    this.setState({
      showDueDatePicker: false
    });
  };

  showImagePicker = () => {
    this.setState({
      showImagePicker: true
    });
  };

  hideImagePicker = () => {
    this.setState({
      showImagePicker: false
    });
  };

  onDatePickerConfirmed = date => {
    const d = pad(date.getMonth(), 2);
    const selectedDate = `${date.getFullYear()}-${pad(
      date.getMonth() + 1,
      2
    )}-${pad(date.getDate(), 2)}`;

    let newFields = { ...this.state.fields };
    newFields["date"] = selectedDate;

    this.setState({
      ...this.state,
      fields: { ...newFields }
    });
  };

  onDueDatePickerConfirmed = date => {
    const d = pad(date.getMonth(), 2);
    const selectedDate = `${date.getFullYear()}-${pad(
      date.getMonth() + 1,
      2
    )}-${pad(date.getDate(), 2)}`;

    let newFields = { ...this.state.fields };
    newFields["due_date"] = selectedDate;

    this.setState({
      ...this.state,
      fields: { ...newFields }
    });
  };

  selectRegeny = () => {
    const { dispatch } = this.props;

    dispatch(
      NavigationActions.navigate({
        routeName: "SelectRegency",
        params: {
          type:"single",
          callback: this.onRegencySelected
        }
      })
    );
  };

  onRegencySelected = regency => {
    this.setState({
      selectedRegency: regency,
      fields: {
        ...this.state.fields,
        regency_id: regency.id
      }
    });
  };

  selectConsultanPlan = () => {
    const { dispatch } = this.props;

    dispatch(
      NavigationActions.navigate({
        routeName: "CustomerSelect",
        params: {
          type:"single",
          callback: this.onConsultanPlanSelected
        }
      })
    );
  };

  onConsultanPlanSelected = customer => {
    this.setState({
      selectedConsultanPlan: customer,
      fields: {
        ...this.state.fields,
        consultan_plan: customer.id
      }
    });
  };

  selectConsultanSuper = () => {
    const { dispatch } = this.props;

    dispatch(
      NavigationActions.navigate({
        routeName: "CustomerSelect",
        params: {
          type:"single",
          callback: this.onConsultanSuperSelected
        }
      })
    );
  };

  onConsultanSuperSelected = customer => {
    this.setState({
      selectedConsultanSuper: customer,
      fields: {
        ...this.state.fields,
        consultan_super: customer.id
      }
    });
  };

  selectContractor = () => {
    const { dispatch } = this.props;

    dispatch(
      NavigationActions.navigate({
        routeName: "CustomerSelect",
        params: {
          type:"single",
          callback: this.onContractorSelected
        }
      })
    );
  };

  onContractorSelected = customer => {
    this.setState({
      selectedContractor: customer,
      fields: {
        ...this.state.fields,
        contractor: customer.id
      }
    });
  };

  selectSubContractor = () => {
    const { dispatch } = this.props;

    dispatch(
      NavigationActions.navigate({
        routeName: "CustomerSelect",
        params: {
          type:"single",
          callback: this.onSubContractorSelected
        }
      })
    );
  };

  onSubContractorSelected = customer => {
    this.setState({
      selectedSubContractor: customer,
      fields: {
        ...this.state.fields,
        contractor_sub: customer.id
      }
    });
  };

  selectMarketing = () => {
    const { dispatch } = this.props;

    dispatch(
      NavigationActions.navigate({
        routeName: "MarketingSelect",
        params: {
          type:"single",
          callback: this.onMarketingSelected
        }
      })
    );
  };

  onMarketingSelected = marketing => {
    this.setState({
      selectedMarketing: marketing,
      fields: {
        ...this.state.fields,
        marketing_id: marketing.id
      }
    });
  };

  getSubmitButtonLabel = () => {
    if (this.props.isLoading) {
      return "Menyimpan data ...";
    } else {
      return "Simpan";
    }
  };

  render() {
    const { fields } = this.state;
    const props = this.props;
    
    return (
      <KeyboardAvoidingView behavior="padding" style={{flex: 1, alignSelf: "stretch"}}>
        <ScrollView style={styles.form} keyboardShouldPersistTaps="always">
          <View style={styles.section}>
            <TextInputWithLabel
              label="Nama Project"
              style={formStyles.inputWrapper}
              inputStyle={formStyles.input}
              labelStyle={formStyles.inputLabel}
              onChangeText={this.updateTextField("name")}
              value={fields.name}
              autoCapitalize="words"
            />
            {this.renderErrorMessage("name")}
          </View>
          <View style={styles.section}>
            <TextInputWithLabel
              label="Tanggal"
              style={formStyles.inputWrapper}
              inputStyle={formStyles.input}
              labelStyle={formStyles.inputLabel}
              value={
                fields.date
                  ? formatDate(fields.date, "DD MMMM YYYY")
                  : null
              }
              editable={false}
              onTouchStart={this.showDatePicker}
            />
            {this.renderErrorMessage("date")}
          </View>
          <View style={styles.section}>
            <Select
              label="Tipe Project"
              title="Pilih Tipe Project"
              style={[formStyles.selectWrapper]}
              inputStyle={formStyles.select}
              labelStyle={formStyles.inputLabel}
              selectedIndex={fields.status_index}
              onItemSelected={this.updatePickerField("status")}
              items={[
                { label: "Perencanaan", value: 0 },
                { label: "Tender", value: 1 },
                { label: "PO", value: 2 },
                { label: "Selesai", value: 3 },
                { label: "Batal", value: 4 },
                { label: "Lose", value: 5 },
              ]}
            />
            {this.renderErrorMessage("status")}
          </View>
          <View style={styles.section}>
            <TextInputWithLabel
              label="Batas Akhir"
              style={formStyles.inputWrapper}
              inputStyle={formStyles.input}
              labelStyle={formStyles.inputLabel}
              value={
                fields.due_date
                  ? formatDate(fields.due_date, "DD MMMM YYYY")
                  : null
              }
              editable={false}
              onTouchStart={this.showDueDatePicker}
            />
            {this.renderErrorMessage("due_date")}
          </View>
          <View style={styles.section}>
            <TextInputWithLabel
              label="Kota/Kabupaten"
              placeholder={"Pilih kota/kabupaten"}
              style={formStyles.inputWrapper}
              inputStyle={[formStyles.input, { textAlignVertical: "center" }]}
              labelStyle={formStyles.inputLabel}
              editable={false}
              value={
                this.state.selectedRegency !== null
                ? this.state.selectedRegency.name
                : null}
              onTouchStart={this.selectRegeny}
            />
            {this.renderErrorMessage("regency_id")}
          </View>
          <View style={styles.section}>
            <TextInputWithLabel
              label="Alamat"
              style={formStyles.inputWrapper}
              inputStyle={[formStyles.input, { textAlignVertical: "top" }]}
              labelStyle={formStyles.inputLabel}
              multiline={true}
              onChangeText={this.updateTextField("address")}
              value={fields.address}
              autoCapitalize="words"
            />
            {this.renderErrorMessage("address")}
          </View>
          <View style={styles.section}>
            <TextInputWithLabel
              label="Keterangan"
              style={formStyles.inputWrapper}
              inputStyle={[formStyles.input, { textAlignVertical: "top" }]}
              labelStyle={formStyles.inputLabel}
              multiline={true}
              onChangeText={this.updateTextField("description")}
              value={fields.description}
              autoCapitalize="words"
            />
            {this.renderErrorMessage("description")}
          </View>
          <View style={styles.section}>
            <TextInputWithLabel
              label="Nilai"
              style={formStyles.inputWrapper}
              inputStyle={[formStyles.input, { textAlignVertical: "top" }]}
              labelStyle={formStyles.inputLabel}
              keyboardType="numeric"
              onChangeText={this.updateTextField("price")}
              value={formatMoney(fields.price)}
            />
            {this.renderErrorMessage("price")}
          </View>
          <View style={styles.section}>
            <TextInputWithLabel
              label="Marketing"
              placeholder={"Pilih marketing"}
              style={formStyles.inputWrapper}
              inputStyle={[formStyles.input, { textAlignVertical: "center" }]}
              labelStyle={formStyles.inputLabel}
              editable={false}
              value={
                this.state.selectedMarketing !== null
                ? this.state.selectedMarketing.name
                : null}
              onTouchStart={this.selectMarketing}
            />
            {this.renderErrorMessage("marketing_id")}
          </View>
          <View style={styles.section}>
            <TextInputWithLabel
              label="Konsultan Perencana"
              placeholder={"Pilih konsultan perencana"}
              style={formStyles.inputWrapper}
              inputStyle={[formStyles.input, { textAlignVertical: "center" }]}
              labelStyle={formStyles.inputLabel}
              editable={false}
              value={
                this.state.selectedConsultanPlan !== null
                ? this.state.selectedConsultanPlan.name
                : null}
              onTouchStart={this.selectConsultanPlan}
            />
            {this.renderErrorMessage("consultan_plan")}
          </View>
          <View style={styles.section}>
            <TextInputWithLabel
              label="Konsultan Pengawas"
              placeholder={"Pilih konsultan pengawas"}
              style={formStyles.inputWrapper}
              inputStyle={[formStyles.input, { textAlignVertical: "center" }]}
              labelStyle={formStyles.inputLabel}
              editable={false}
              value={
                this.state.selectedConsultanSuper !== null
                ? this.state.selectedConsultanSuper.name
                : null}
              onTouchStart={this.selectConsultanSuper}
            />
            {this.renderErrorMessage("consultan_super")}
          </View>
          <View style={styles.section}>
            <TextInputWithLabel
              label="Main Kontraktor"
              placeholder={"Pilih main kontraktor"}
              style={formStyles.inputWrapper}
              inputStyle={[formStyles.input, { textAlignVertical: "center" }]}
              labelStyle={formStyles.inputLabel}
              editable={false}
              value={
                this.state.selectedContractor !== null
                ? this.state.selectedContractor.name
                : null}
              onTouchStart={this.selectContractor}
            />
            {this.renderErrorMessage("contractor")}
          </View>
          <View style={styles.section}>
            <TextInputWithLabel
              label="Sub Kontraktor"
              placeholder={"Pilih sub kontraktor"}
              style={formStyles.inputWrapper}
              inputStyle={[formStyles.input, { textAlignVertical: "center" }]}
              labelStyle={formStyles.inputLabel}
              editable={false}
              value={
                this.state.selectedSubContractor !== null
                ? this.state.selectedSubContractor.name
                : null}
              onTouchStart={this.selectSubContractor}
            />
            {this.renderErrorMessage("contractor_sub")}
          </View>
          <Button
            label={this.getSubmitButtonLabel()}
            style={formStyles.button}
            gradient={BUTTON_GRADIENT}
            gradientDirection="horizontal"
            disabled={this.props.isDisabled || this.props.isLoading}
            onPress={() => this.onSubmit()}
            containerStyle={{ elevation: 3 }}
          />
          <DatePicker
            isVisible={this.state.showDatePicker}
            onConfirm={this.onDatePickerConfirmed}
            onCancel={this.hideDatePicker}
            onHideAfterConfirm={this.hideDatePicker}
            minimumDate={new Date()}
            datePickerModeAndroid="spinner"
            date={new Date(fields.date)}
          />
          <DatePicker
            isVisible={this.state.showDueDatePicker}
            onConfirm={this.onDueDatePickerConfirmed}
            onCancel={this.hideDueDatePicker}
            onHideAfterConfirm={this.hideDueDatePicker}
            minimumDate={new Date()}
            datePickerModeAndroid="spinner"
            date={new Date(fields.due_date)}
          />
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}

export default connect()(ProjectForm);
