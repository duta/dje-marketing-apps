import React, { Component } from "react";
import { Image, View, Text, StyleSheet } from "react-native";
import { connect } from "react-redux";
import Modal from "react-native-modal";
import SnackBar from "react-native-snackbar";
import { NavigationActions, StackActions } from "react-navigation";
import Container from "../../../components/Container";
import ProjectForm from "./ProjectForm";
import { addProject, updateProject, clearError } from "../project.action";
import { Loading, FullScreenLoading } from "../../../components/Loading";
import { COLOR_SOFTEN_RED, COLOR_VIOLET_GREY, BUTTON_GRADIENT } from "../../../config/common";


class ActivityInput extends Component {
  timeout = null;
  state = {
    success: false
  };

  static navigationOptions = ({ navigation }) => {
    return {
      headerRight: <View />
    };
  };

  onFormSubmit = data => {
    if (this.props.project === null) {
      this.props.dispatch(addProject(data));
    } else {
      this.props.dispatch(updateProject(this.props.project.id, data));
    }
  };

  componentWillMount() {
    this.props.dispatch(clearError());
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.isUpdating && !nextProps.isUpdating) {
      if (!nextProps.isError) {
        this.setState({ success: true });
      } else {
        setTimeout(() => {
          SnackBar.show({
            title: "Tidak dapat menyimpan data.",
            duration: SnackBar.LENGTH_LONG,
            backgroundColor: COLOR_SOFTEN_RED
          });
        }, 500);
      }
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (!prevState.success && this.state.success) {
      this.timeout = setTimeout(this.dismissSuccessMessage, 1000);
    }
  }

  dismissSuccessMessage = () => {
    //this.setState({ success: false });
    this.redirectToList();

    if (this.timeout) {
      clearTimeout(this.timeout);
    }
  };

  renderSuccessMessage = () => {
    var text = "Penambahan project telah berhasil";
    if (this.props.navigation.state.params.id) {
      text = "Data berhasil diperbarui";
    }

    return (
      <Modal
        isVisible={this.state.success}
        onBackButtonPress={this.dismissSuccessMessage}
        onBackdropPress={this.dismissSuccessMessage}
      >
        <View style={styles.messageWrapper}>
          <Image source={require("../../../assets/images/updated.png")} />
          <Text style={styles.message}>{text}</Text>
        </View>
      </Modal>
    );
  };

  redirectToList = () => {
    const { dispatch,routes } = this.props;
    const routeQty = routes.length;
    dispatch(NavigationActions.back());
    if (routeQty == 4) {
      dispatch(NavigationActions.back());
    }
    
  };

  render() {
    return (
      <Container style={{ padding: 0 }}>
        <ProjectForm
          errors={this.props.errors}
          onSubmit={this.onFormSubmit}
          project={this.props.project}
          isLoading={this.props.isUpdating}
          marketing={this.props.marketing}
        />
        
        <FullScreenLoading 
          isVisible={this.props.isUpdating}
          text="Memproses data ..."
        />
        {this.renderSuccessMessage()}
      </Container>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  const projectId = ownProps.navigation.state.params.id;
  const project = ownProps.navigation.state.params.data;
  return {
    project: projectId ? project : null,
    isUpdating: state.project.isUpdating,
    isError: state.project.isError,
    errors: state.project.errors || {},
    marketing: state.user.profile.data[0],
    routes: state.nav.routes
  };
};

const styles = StyleSheet.create({
  messageWrapper: {
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center",
    padding: 16,
    borderRadius: 3
  },
  message: {
    fontFamily: "Quicksand",
    color: COLOR_VIOLET_GREY,
    textAlign: "center",
    marginVertical: 16
  }
});

export default connect(mapStateToProps)(ActivityInput);
