import React, { Component } from "react";
import { Modal, FlatList, View, Text, HeaderProps, SafeAreaView, KeyboardAvoidingView, TouchableOpacity, RefreshControl,Keyboard,Platform } from "react-native";
import { NavigationActions } from "react-navigation";
import { connect } from "react-redux";
import containerStyles from "../../../styles/container";
import ActionButton from "../../../components/ActionButton";
import ListItem from "./ListItem";
import styles from "./styles";
import _ from "lodash";
import {
  BUTTON_GRADIENT,
  COLOR_PRIMARY,
  COLOR_SOFTEN_RED
} from "../../../config/common";
import {
  fetchProjects,
  resetProject
} from "../project.action";
import { FullPageLoading } from "../../../components/Loading";
import CustomComponent from "../../../components/CustomComponent";
import SearchBar from "../../../components/SearchBar";
import FilterForm from "./FilterForm";
import * as ActionProvince from "../../SelectProvince/select_province.action";
import DatePicker from "react-native-modal-datetime-picker";
import Icon from "react-native-vector-icons/MaterialIcons";
import SnackBar from "react-native-snackbar";
import inflateError from "../../../utils/inflateError";
import { pad } from "../../../utils/string";
import RNFetchBlob from 'rn-fetch-blob';
import * as Progress from 'react-native-progress';
import { getToday } from "../../../utils/date";
import FileViewer from 'react-native-file-viewer';

class ProjectList extends CustomComponent {
  state = {
    searching: false,
    searchText: null,
    showFilterForm: false,
    selectedTypes: [],
    selectedProvince: [],
    selectedProvinceData:[],
    selectedMarketing:null,
    showDatePickerFirst:false,
    showDatePickerEnd:false,
    firstDate:null,
    endDate:null,
    currentPage:1,
    loadingModal: false,
    downloadProgress:0
  };

  constructor(props) {
    super(props);

    this.requestData = this.requestData.bind(this);
    this.handleLoadMore = this.handleLoadMore.bind(this);
    this.refreshData = this.refreshData.bind(this);
    this.downloadExcel = this.downloadExcel.bind(this);
  }

  static navigationOptions = ({ navigation }) => ({
    header: navigation.state.params.searchBar || HeaderProps,
    headerRight: (
      <View style={{ paddingHorizontal: 16 }}>
        {navigation.state.params.right !== undefined
          ? navigation.state.params.right
          : null}
      </View>
    )
  });

  componentWillMount() {
    this.setState({loading: true});
  }

  componentDidFocus() {
    this.setState({loading: false});
    this._showSearchButton();

    if (!this.state.fetched) {
      this.refreshData();
      this.setState({fetched: true});
    }
  }

  /**
   * Show search button
   *
   */
  _showSearchButton = () => {
    const { searching } = this.state;
    if (!searching) {
      let add_button = <ActionButton onPress={this.onAddButtonPress} icon="add" />;
      let margin = 16;
      this.props.navigation.setParams({
        right: (
          <View style={{ flexDirection: "row" }}>
            <ActionButton
              onPress={this._showSearchForm}
              icon="search"
              style={{ marginRight: margin }}
            />
            {add_button}
          </View>
        )
      });
    }
  };

  /**
   * Do search
   *
   */
  doSearch = text => {
    this.setState({ searchText: text });
    this.refreshData();
  };

  /**
   * Cancel search
   *
   */
  cancelSearch = () => {
    this.props.navigation.setParams({ searchBar: null });
    this.setState({ searching: false, searchText: null });
    this.refreshData();
  };

  /**
   * Show search form on header
   *
   */
  _showSearchForm = () => {
    this.setState({ searching: true });
    this.props.navigation.setParams({
      searchBar: (
        <SafeAreaView style={{backgroundColor: COLOR_PRIMARY}}>
          <SearchBar
            backgroundColor={COLOR_PRIMARY}
            placeholder="Ketikan nama project"
            onCancel={this.cancelSearch}
            onChangeText={this.doSearch}
            text={this.state.searchText}
          />
        </SafeAreaView>
      )
    });
  };

  requestData() {
    const params = {
      page : this.state.currentPage,
      search : this.state.searchText,
      status : this.state.selectedTypes,
      province : this.state.selectedProvince,
      marketing : this.state.selectedMarketing ? this.state.selectedMarketing.id : null,
      first_date : this.state.firstDate,
      second_date : this.state.endDate,
    }

    this.props.dispatch(fetchProjects(params));
  }

  refreshData(){
    this.props.dispatch(resetProject());
    this.setState({
      currentPage: 1,
    },() => {this.requestData()});
  }

  componentWillUnmount() {
    super.componentWillUnmount();
    this.cancelSearch();
  }


  showProjectDetail = project => {
    this.props.dispatch(
      NavigationActions.navigate({ 
        routeName: "ProjectDetail", 
        params: { 
          data : project,
        } 
      })
    );
  };


  onEditButtonPress = project => {
    this.props.dispatch(
      NavigationActions.navigate({
        routeName: "ProjectInput",
        params: {
          title: "Edit Project",
          id: project.id,
          data: project
        }
      })
    );
  };

  onItemPress = project => {
    this.showProjectDetail(project);
  };

  componentWillReceiveProps(nextProps) {
    if (!this.props.isError && nextProps.isError) {
      setTimeout(() => {
        SnackBar.show({
          title: inflateError(nextProps.errors),
          duration: SnackBar.LENGTH_LONG,
          backgroundColor: COLOR_SOFTEN_RED
        });
      }, 500);
    }
  }

  handleLoadMore() {
    if (this.props.lastPage !== this.state.currentPage) {
      this.setState({
        currentPage: parseInt(this.state.currentPage) + 1,
      },() => {this.requestData()});
    }
  };

  renderList() {
    const projects = Object.values(this.props.projects) || [];
    const listItem = listItem => {
      const project = listItem.item;

      return (
        <ListItem
          title={project.name}
          address={project.address}
          date={project.date}
          status={project.status}
          marketing={project.marketing.name}
          onPress={() => this.onItemPress(project)}
          editAction={() => this.onEditButtonPress(project)}
          key={`project-list-${project.id}`}
          swipeDisable={this.state.selecting}
        />
      );
    };

    return (
      <FlatList
        data={projects}
        renderItem={listItem}
        style={styles.list}
        extraData={this.state}
        keyExtractor={item => `key-${item.id}`}
        removeClippedSubviews={false}
        onEndReached={this.handleLoadMore}
        onEndReachedThreshold={0.3}
        refreshControl={
          <RefreshControl
            refreshing={this.props.isFetching}
            onRefresh={this.refreshData}
          />
        }
      />
    );
  }

  onAddButtonPress = () => {
    this.props.dispatch(
      NavigationActions.navigate({
        routeName: "ProjectInput",
        params: {
          title: "Tambah Project"
        }
      })
    );
  };

  renderFilterButton = () => {
    return (
      <View style={styles.filterButtonWrapper}>
        <TouchableOpacity
          style={[styles.filterButton, styles.filterButtonBorder]}
          onPress={this.showFilterForm}
        >
          <Icon name="filter-list" size={24} color={COLOR_PRIMARY} />
          <Text style={styles.filterButtonText}>Filter</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.filterButton} onPress={this.downloadExcel}>
          <Icon name="cloud-download" size={24} color={COLOR_PRIMARY} />
          <Text style={styles.filterButtonText}>Excel</Text>
        </TouchableOpacity>
      </View>
    );
  };

  showFilterForm = () => {
    this.setState({ showFilterForm: true });
  };

  hideFilterForm = () => {
    this.setState({ showFilterForm: false });
  };
  
  onFilterTypeItemPress = type => {
    var selectedTypes = this.state.selectedTypes;
    const itemIndex = this.state.selectedTypes.indexOf(type.id);
    if (itemIndex === -1) {
      selectedTypes.push(type.id);
    } else {
      selectedTypes.splice(itemIndex, 1);
    }

    this.setState({
      selectedTypes
    });
  };

  selectProvince = () => {
    Keyboard.dismiss();
    this.props.dispatch(
      NavigationActions.navigate({
        routeName: "SelectProvince",
        params: {
          callback: this.onProvinceSelected,
          type: "multy"
        }
      })
    ); 
    this.hideFilterForm();
  };

  onProvinceSelected = province => {
    this.setState({ 
      selectedProvince: _.map(province , function(data) { return data.id; }),
      selectedProvinceData:province
    });
    this.showFilterForm();
  };

  selectMarketing = () => {
    Keyboard.dismiss();
    this.props.dispatch(
      NavigationActions.navigate({
        routeName: "MarketingSelect",
        params: {
          callback: this.onMarketingSelected,
          type: "single"
        }
      })
    ); 
    this.hideFilterForm();
  };

  onMarketingSelected = marketing => {
    this.setState({ 
      selectedMarketing:marketing
    });
    this.showFilterForm();
  };

  onFilterReset = () => {
    this.setState({
      selectedTypes: [],
      selectedProvince: [],
      selectedProvinceData:[],
      selectedMarketing:null,
      firstDate:null,
      endDate:null,
    });
  };

  onFilter = () => {
    this.refreshData();
    this.hideFilterForm();
  };

  showDatePickerFirst = () => {
    this.setState({
      showDatePickerFirst: true
    });
  };

  hideDatePickerFirst = () => {
    this.setState({
      showDatePickerFirst: false
    });
  };

  onDatePickerConfirmedFirst = date => {
    const selectedDate = `${date.getFullYear()}-${pad(
      date.getMonth() + 1,
      2
    )}-${pad(date.getDate(), 2)}`;

    this.setState({
      firstDate : selectedDate,
      endDate : selectedDate
    });
  };

  showDatePickerEnd = () => {
    this.setState({
      showDatePickerEnd: true
    });
  };

  hideDatePickerEnd = () => {
    this.setState({
      showDatePickerEnd: false
    });
  };

  onDatePickerConfirmedEnd = date => {
    const selectedDate = `${date.getFullYear()}-${pad(
      date.getMonth() + 1,
      2
    )}-${pad(date.getDate(), 2)}`;

    this.setState({
      endDate : selectedDate
    });
  };

  async downloadExcel() {
    let today = getToday();
    let dirs = RNFetchBlob.fs.dirs;
		let fileName = 'Project'+today+'.xlsx';
		let os = Platform.OS;

    const params = {
      status : this.state.selectedTypes,
      province : this.state.selectedProvince,
      marketing : this.state.selectedMarketing ? this.state.selectedMarketing.id : null,
      first_date : this.state.firstDate,
      second_date : this.state.endDate,
    }


		if(os == "android") {
			dirs = dirs.SDCardDir;
		}
		else {
			dirs = dirs.DocumentDir;
    }
    await this.setState({
      loadingModal: true
    }, () => {
      RNFetchBlob
      .config({
        path : dirs + '/DJE/' + fileName
      })
      .fetch('GET','https://deltajaya.com/backend/project-export?params='+JSON.stringify(params))
      .progress({ interval: 250 },(received,total)=>{
        this.setState({
          downloadProgress:(received/total)
        })
      })
      .then((res) => {
          this.setState({
              loadingModal: false
          }, () => {
            // console.log("sukses")
            // setTimeout(() => {
            //   SnackBar.show({
            //     title: 'File berhasil diunduh. \nLokasi: ' + res.path(),
            //     duration: SnackBar.LENGTH_LONG,
            //     backgroundColor: "rgba(0, 0, 0, .6)",
            //   });
            // }, 500);
            var localFile = res.path();
            FileViewer.open(localFile)
            .catch(error => {
              console.log(localFile);
              console.log(error);
              alert('Tidak ditemukan aplikasi untuk menayangkan dokumen'+error);
            });
          });
      });
    });
  }

  _modalLoading() {
    return (
        <Modal
            animationType="slide"
    transparent={true}
            visible={this.state.loadingModal}
            onRequestClose={() => {
                this.setState({loadingModal: false});
            }}>
                <View style={[styles.loading, {backgroundColor: "rgba(0, 0, 0, .6)"}]}>
                    <View style={{ backgroundColor:'transparent', height:86, width:86, justifyContent:'center', alignItems:'center', borderRadius:6 }} >
                        {/* <Image source={ require('../../../assets/loading.gif') }  style={{height:60, width:60 }} /> */}
                      <Progress.Bar showsText thickness={5} progress={this.state.downloadProgress} size={68} textStyle={{fontWeight: '700'}} />
                    </View>
                </View>
        </Modal>
    )
}

  render() {
    const typeList = this.props.types;

    return (
      <KeyboardAvoidingView style={containerStyles.container}>
        {/* <FullPageLoading
          isVisible={this.props.isFetching}
          text="Mengambil data ..."
        /> */}
        {this._modalLoading()}
        <View style={styles.listWrapper}>
          {this.renderList()}
        </View>
        {this.renderFilterButton()}
        <FilterForm
          types={typeList}
          isVisible={this.state.showFilterForm}
          onClose={this.hideFilterForm}
          onTypePress={this.onFilterTypeItemPress}
          selectedTypes={this.state.selectedTypes}
          onProvince={this.selectProvince}
          selectedProvince={this.state.selectedProvinceData}
          onMarketing={this.selectMarketing}
          selectedMarketing={this.state.selectedMarketing}
          onFilter={this.onFilter}
          onFilterReset={this.onFilterReset}
          showDatePickerFirst={this.showDatePickerFirst}
          showDatePickerEnd={this.showDatePickerEnd}
          valFirstDate={this.state.firstDate}
          valEndDate={this.state.endDate}
        />
        <DatePicker
          isVisible={this.state.showDatePickerFirst}
          onConfirm={this.onDatePickerConfirmedFirst}
          onCancel={this.hideDatePickerFirst}
          onHideAfterConfirm={this.hideDatePickerFirst}
          // minimumDate={new Date()}
          datePickerModeAndroid="spinner"
          date={this.state.firstDate ? new Date(this.state.firstDate) : new Date()}
        />
        <DatePicker
          isVisible={this.state.showDatePickerEnd}
          onConfirm={this.onDatePickerConfirmedEnd}
          onCancel={this.hideDatePickerEnd}
          onHideAfterConfirm={this.hideDatePickerEnd}
          // minimumDate={new Date()}
          datePickerModeAndroid="spinner"
          date={this.state.endDate ? new Date(this.state.endDate) : new Date()}
        />
      </KeyboardAvoidingView>
    );
  }
}

const mapStateToProps = state => {
  const data_type = [
    {
      id: 0,
      name: "Perencanaan",
    },
    {
      id: 1,
      name: "Tender",
    },
    {
      id: 2,
      name: "PO",
    },
    {
      id: 3,
      name: "Selesai",
    },
    {
      id: 4,
      name: "Batal",
    },
    {
      id: 5,
      name: "Lose",
    }
  ];
    
  return {
    projects: state.project.list,
    isFetching: state.project.isFetching,
    isError: state.project.isError,
    provinces: state.provinces.list,
    errors: state.project.errors,
    types:data_type,
    lastPage:state.project.lastPage
  };
};

export default connect(mapStateToProps)(ProjectList);
