import React from "react";
import { Text } from "react-native";
import PropTypes from "prop-types";

import styles from "./styles";
import {
  COLOR_PRIMARY,
  COLOR_SOFTEN_RED,
  COLOR_ORANGE,
  COLOR_VIOLET_GREY,
  COLOR_DARKEN_RED
} from "../../../config/common";

const QueueStatus = props => {
  const { status } = props;
  let statusStyles = [styles.status];
  let text = status;
  
  switch (status) {
    case 0:
      statusStyles.push({ color: COLOR_ORANGE,height:23 });
      text = "Perencanaan";
      break;
    case 1:
      statusStyles.push({ color: "blue",height:23 });
      text = "Tender";
      break;
    case 2:
      statusStyles.push({ color: COLOR_PRIMARY,height:23 });
      text = "PO";
      break;
    case 3:
      statusStyles.push({ color: COLOR_VIOLET_GREY,height:23 });
      text = "Selesai";
      break;
    case 4:
      statusStyles.push({ color: COLOR_SOFTEN_RED,height:23 });
      text = "Batal";
      break;
    case 5:
      statusStyles.push({ color: COLOR_DARKEN_RED,height:23 });
      text = "Lose";
      break;
  }

  return <Text style={statusStyles}>{text}</Text>;
};

QueueStatus.propTypes = {
  status: PropTypes.oneOf([0, 1, 2, 3, 4, 5]).isRequired
};

export default QueueStatus;
