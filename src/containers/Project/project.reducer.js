import * as actionTypes from "./project.actionTypes";
import { RESET_STATE } from "../../config/reducer.action";
import { isArray, mapKeys, sortBy, orderBy } from "lodash";

const iniialState = {
  isFetching: false,
  isError: false,
  isUpdating: false,
  errors: null,
  list: [],
  lastPage:1
};

const updateList = (list, data) => {
  var newList = list;
  newList[data.id] = data;

  return newList;
};

const updateFetchedList = (list,data) => {
  var newList = [...list,...data];

  return newList;
};

const reducer = (state = iniialState, action) => {
  const type = action.type;

  switch (type) {
    case actionTypes.FETCH_PROJECTS:
      return {
        ...state,
        isFetching: true,
        isError: false
      };
    case actionTypes.PROJECTS_FETCHED:
      return {
        ...state,
        isFetching: false,
        isError: false,
        isUpdating: false,
        list: updateFetchedList(state.list,action.data.data),
        errors: null,
        lastPage: action.data.last_page
      };
    case actionTypes.UPDATE_PROJECT:
    case actionTypes.ADD_PROJECT:
      return {
        ...state,
        isUpdating: true,
        isError: false
      };
    case actionTypes.PROJECT_UPDATED:
    case actionTypes.PROJECT_ADDED:
      return {
        ...state,
        isUpdating: false,
        isError: false,
        list: {
          ...state.list,
          ...updateList(state.list, action.data)
        }
      };
    case actionTypes.UPDATE_PROJECT_FAIL:
    case actionTypes.FETCH_PROJECTS_FAIL:
    case actionTypes.ADD_PROJECT_FAIL:
      return {
        ...state,
        isFetching: false,
        isUpdating: false,
        isError: true,
        errors: action.errors
      };
    case actionTypes.PROJECT_CLEAR_ERROR:
      return {
        ...state,
        isError: false,
        errors: null
      };
    case actionTypes.RESET_PROJECT:
    case RESET_STATE:
      return iniialState;
    default:
      return state;
  }
};

export default reducer;
