import * as actionTypes from "./project.actionTypes";

export function resetProject(){
  return {
    type:actionTypes.RESET_PROJECT
  }
}

export function fetchProjects(params) {
  return {
    type: actionTypes.FETCH_PROJECTS,
    params
  };
}

export function addProject(data) {
  return {
    type: actionTypes.ADD_PROJECT,
    data
  };
}

export function updateProject(id, data) {
  return {
    type: actionTypes.UPDATE_PROJECT,
    id,
    data
  };
}


export function clearError() {
  return {
    type: actionTypes.PROJECT_CLEAR_ERROR
  };
}
