import React, { Component } from "react";
import { Image, View, Text } from "react-native";
import LinearGradient from "react-native-linear-gradient";
import { connect } from "react-redux";
import { NavigationActions } from "react-navigation";
import Container from "../../../components/Container";
import containerStyles from "../../../styles/container";
import Item from "../../../components/Item";
import styles from "./styles";
import Separator from "../../../components/Separator";
import {
  HEADER_GRADIENT,
  COLOR_PRIMARY,
  COLOR_GREY
} from "../../../config/common";
import ActionButton from "../../../components/ActionButton";
import { formatDate } from "../../../utils/date";
import { projecStatus } from "../../../utils/string";
import ProfilePicture from "../../../components/ProfilePicture";
import { map } from "lodash";

class ProjectDetail extends Component {
  static navigationOptions = ({ navigation }) => {
    var action = () => {};
    if (navigation.state.params.action) {
      action = navigation.state.params.action;
    }

    return {
      headerStyle: {
        elevation: 0,
        backgroundColor: COLOR_PRIMARY
      },
      headerRight: (
        <View style={{ paddingHorizontal: 16 }}>
          <ActionButton icon="create" onPress={action} />
        </View>
      )
    };
  };

  componentDidMount() {
    this.props.navigation.setParams({
      action: this.navigateToEdit
    });
  }

  navigateToEdit = () => {
    this.props.dispatch(
      NavigationActions.navigate({
        routeName: "ProjectInput",
        params: {
          title: "Edit Project",
          id: this.props.project.id,
          data: this.props.project
        }
      })
    );
  };

  renderSchedules = () => {
    return (
      <View style={styles.schedules}>
        <Text style={[styles.label, { marginBottom: 16, fontSize: 15 }]}>
          Project Log{" "}
        </Text>

        {map(this.props.project.project_log, (log, key) => (
          <View style={styles.location} key={key}>
            <Text style={styles.label}>{log.type}</Text>
            <View style={styles.row}>
              <Text style={styles.contentLabel}>Tanggal</Text>
              <Text style={styles.contentValue}>{formatDate(log.date, "DD MMMM YYYY")}</Text>
            </View>
            <View style={styles.row}>
              <Text style={styles.contentLabel}>Status</Text>
              <Text style={styles.contentValue}>{projecStatus(log.status)}</Text>
            </View>
            <View style={styles.row}>
              <Text style={styles.contentLabel}>Marketing</Text>
              <Text style={styles.contentValue}>{log.marketing_name}</Text>
            </View>
            <View style={styles.row}>
              <Text style={styles.contentLabel}>Keterangan</Text>
              <Text style={styles.contentValue}>{log.description}</Text>
            </View>
          </View>
        ))}
      </View>
    );
  };


  render() {
    const project = this.props.project || {};
    return (
      <Container style={[containerStyles.container, styles.container]}>
        <View style={styles.content}>
          <View style={styles.row}>
            <Item
              label="Nama"
              value={project.name}
              style={{ flex: 1 }}
            />
            <Item
              label="Kota"
              value={project.regency ? project.regency.name : "-"}
              style={{ flex: 1 }}
            />
            
          </View>
          <View style={styles.row}>
          <Item
              label="Keterangan"
              value={project.description || "-"}
              style={{ flex: 1 }}
            />
            <Item 
              label="Alamat" 
              value={project.address || "-"} 
              style={{ flex: 1 }}/>
          </View>
          <Separator />
          {this.renderSchedules()}
        </View>
      </Container>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  
  const projectId = ownProps.navigation.state.params.data.id;
  const project = state.project.list.filter(function(data) {
    return data.id == projectId;
  });
  return {
    project: ownProps.navigation.state.params.data,
  };
};

export default connect(mapStateToProps)(ProjectDetail);
