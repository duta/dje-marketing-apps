import EStyleSheet from "react-native-extended-stylesheet";
import { COLOR_PRIMARY_GREY } from "../../../config/common";

const styles = EStyleSheet.create({
  container: {
    alignItems: "flex-start",
    justifyContent: "flex-start",
    flexGrow: 1,
    backgroundColor: "white"
  },
  content: {
    padding: 16,
    flex: 1,
    alignSelf: "stretch",
    backgroundColor: "white"
  },
  row: {
    flexDirection: "row",
    alignSelf: "stretch",
    alignItems: "center"
  },
  header: {
    alignSelf: "stretch",
    alignItems: "center",
    justifyContent: "center",
    paddingVertical: 16,
    borderTopColor: COLOR_PRIMARY_GREY,
    borderTopWidth: 1
  },
  profilePict: {
    height: 72,
    width: 72,
    borderRadius: 36,
    marginBottom: 8,
    resizeMode: "cover"
  },
  name: {
    color: "white",
    fontSize: 16,
    fontFamily: "Arial",
    fontWeight: "bold",
    textAlign: "center",
    alignSelf: "stretch"
  },
  schedules: {
    alignSelf: "stretch",
    marginTop: 16
  },
  label: {
    fontFamily: "Arial",
    color: "rgba(117, 123, 149, 1)",
    fontWeight: "bold"
  },
  location: {
    padding: 8,
    backgroundColor: "$bgColor",
    marginBottom: 8,
    alignSelf: "stretch",
    flex: 1
  },
  address: {
    marginBottom: 0
  },
  contentLabel: {
    color: "rgba(117, 123, 149, 0.7)",
    fontSize: 14,
    flex:2
  },
  contentValue: {
    fontSize: 14,
    fontFamily: "Quicksand",
    flex:4
  },
});

export default styles;
