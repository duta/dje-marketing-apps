import React, { Component } from "react";
import { 
  View, 
  Dimensions,
  Modal,
  Platform
} from "react-native";
import { NavigationActions } from "react-navigation";
import BackButton from "../../components/BackButton";
import * as Progress from 'react-native-progress';
import FileViewer from 'react-native-file-viewer';
import RNFetchBlob from 'rn-fetch-blob';
import EStyleSheet from "react-native-extended-stylesheet";
import { connect } from "react-redux";

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full width

class Help extends Component {
  webview = null;
  state = {
    loadingModal: false,
  }

  constructor(props) {
    super(props);

    this.downloadExcel = this.downloadExcel.bind(this);
  }

  static navigationOptions = ({ navigation }) => {
    var backAction = () => {};
    if (navigation.state.params && 
      navigation.state.params.backAction) {
        backAction = navigation.state.params.backAction;
      }

    return {
      headerRight: <View />,
      headerLeft: <BackButton onPress={backAction} />
    };
  };

  componentDidMount() {
    this.downloadExcel();
    // this.props.dispatch(NavigationActions.back());
  }

  async downloadExcel() {
    console.log("mulai");
    let dirs = RNFetchBlob.fs.dirs;
		let fileName = 'UserGuide-DJE.pdf';
		let os = Platform.OS;

		if(os == "android") {
			dirs = dirs.SDCardDir;
		}
		else {
			dirs = dirs.DocumentDir;
    }
    await this.setState({
      loadingModal: true
    }, () => {
      RNFetchBlob
      .config({
        path : dirs + '/DJE/' + fileName
      })
      .fetch('GET','https://www.deltajaya.com/backend/public/uploaded/UserGuide-DJE.pdf')
      .progress({ interval: 250 },(received,total)=>{
        this.setState({
          downloadProgress:(received/total)
        })
      })
      .then((res) => {
          this.setState({
              loadingModal: false
          }, () => {
            var localFile = res.path();
            FileViewer.open(localFile)
            .catch(error => {
              console.log(localFile);
              console.log(error);
              alert('Tidak ditemukan aplikasi untuk menayangkan dokumen'+error);
            });
            this.props.dispatch(NavigationActions.back());
          });
      });
    });
  }

  _modalLoading() {
    return (
        <Modal
            animationType="slide"
            transparent={true}
            visible={this.state.loadingModal}
            onRequestClose={() => {
                this.setState({loadingModal: false});
            }}>
                <View style={[styles.loading, {backgroundColor: "rgba(0, 0, 0, .6)"}]}>
                    <View style={{ backgroundColor:'transparent', height:86, width:86, justifyContent:'center', alignItems:'center', borderRadius:6 }} >
                      <Progress.Bar showsText thickness={5} progress={this.state.downloadProgress} size={68} textStyle={{fontWeight: '700'}} />
                    </View>
                </View>
        </Modal>
    )
  }

  render() {
    return (
      <View style={{height: height, width: width}}>
      {this._modalLoading()}
      </View>
    );
  }

}

const styles = EStyleSheet.create({
  loading: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center'
  },
});

export default connect()(Help);

