import React, { Component } from "react";
import { BackHandler, Image, FlatList, Text, View,KeyboardAvoidingView,RefreshControl } from "react-native";
import { connect } from "react-redux";
import { NavigationActions } from "react-navigation";
import BackButton from "../../components/BackButton";
import containerStyles from "../../styles/container";
import Container from "../../components/Container";
import NotificationItem from "./NotificationItem";
import { COLOR_VIOLET_GREY,BUTTON_GRADIENT,COLOR_PRIMARY } from "../../config/common";
import ActionButton from "../../components/ActionButton";
import { deleteNotification, fetchNotifications,readNotificationId } from "./notification.action";
import styles from "./styles";
import ModalDialog from "../../components/Modal";
import { GradientBorderButton, Button} from "../../components/Button";

class Notification extends Component {
  state = {
    selectedItems: [],
    selecting: false,
    currentPage:1,
    showDeleteDialog:false
  };

  static navigationOptions = ({ navigation }) => {
    var backAction = () => {};
    if (navigation.state.params.backAction) {
      backAction = navigation.state.params.backAction;
    }

    return {
      headerRight: navigation.state.params.headerRight ? (
        navigation.state.params.headerRight
      ) : (
        <View />
      ),
      headerLeft: <BackButton onPress={backAction} />
    };
  };

  constructor(props) {
    super(props);

    this.handleLoadMore = this.handleLoadMore.bind(this);
    this.requestData = this.requestData.bind(this);
    this.refreshData = this.refreshData.bind(this);
  }

  handleLoadMore() {
    if (this.props.lastPage !== this.state.currentPage) {
      this.setState({
        currentPage: parseInt(this.state.currentPage) + 1,
      },() => {this.requestData()});
    }
  };

  requestData() {
    const params = {
      page : this.state.currentPage,
    }
    this.props.dispatch(
      fetchNotifications(params)
    )
  }

  refreshData(){
    this.setState({
      currentPage: 1,
    },() => {this.requestData()});
  }

  initActionBar = () => {
    const headerRight = <View />;

    this.props.navigation.setParams({
      headerRight
    });
  };

  setMultipleDeleteButton = () => {
    const headerRight = (
      <View
        style={{
          flexDirection: "row",
          paddingHorizontal: 16,
          alignItems: "center"
        }}
      >
        <ActionButton icon="trash" onPress={this.confirmDelete} />
      </View>
    );

    this.props.navigation.setParams({
      headerRight
    });
  };

  componentDidMount() {
    this.props.navigation.setParams({
      deleteAction: this.confirmDelete,
      backAction: this.onBackPressed
    });
    BackHandler.addEventListener("hardwareBackPress", this.backButtonHandler);

    this.requestData();
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      "hardwareBackPress",
      this.backButtonHandler
    );
  }

  backButtonHandler = () => {
    if (this.state.selecting) {
      this.resetSelectedNotification();
      return true;
    }

    return false;
  };

  onBackPressed = () => {
    if (this.state.selecting) {
      this.resetSelectedNotification();
      return true;
    }

    this.props.dispatch(NavigationActions.back());
    return false;
  };

  selectNotification = id => {
    var selectedItems = this.state.selectedItems || [];

    if (selectedItems.indexOf(id) === -1) {
      selectedItems.push(id);
    } else {
      selectedItems.splice(selectedItems.indexOf(id), 1);
    }

    const selecting = selectedItems.length > 0;
    this.setState({ selectedItems, selecting });

    this.checkSelectedNotification();
    
  };

  checkSelectedNotification = () => {
    const isSelecting = this.state.selectedItems.length > 0;
    this.setState({ isSelecting });

    if (isSelecting) {
      this.setMultipleDeleteButton();
    } else {
      this.initActionBar();
    }
  };

  deleteNotification = () => {
    const { selectedItems } = this.state;
    if (selectedItems.length === 0) return;

    const selectedNotifications = this.props.notifications.filter(
      (val, index) => selectedItems.includes(index)
    );
    const ids = selectedNotifications.map(val => val.id);
    this.props.dispatch(deleteNotification(ids));
    this.resetSelectedNotification();
    this.hideDeleteDialog();
  };

  resetSelectedNotification = () => {
    this.setState({
      selectedItems: [],
      selecting: false
    });

    this.initActionBar();
  };

  onItemPress = id => {
    if (this.state.selecting) {
      this.selectNotification(id);
      return;
    }

    var notif = this.props.notifications[id];
    
    if (notif) {
      const { data,id } = notif;
      this.props.dispatch(readNotificationId(id));
      // console.log("data",notif)
      this.props.dispatch(
        NavigationActions.navigate({
          routeName: data.navigation,
          params: { 
            id: data.id,
            data: data.data
          }
        })
      );
    }

  };

  _renderList() {
    const item = ({ item, index }) => {

      return (
        <NotificationItem
          key={Math.random()}
          id={index}
          title={item.title}
          body={item.body}
          date={item.date}
          read={item.read_at}
          onPress={() => this.onItemPress(index)}
          onLongPress={() => this.selectNotification(index)}
          isChecked={this.state.selectedItems.includes(parseInt(index))}
        />
      );
    };

    return (
      <FlatList
        style={{ flexGrow: 1, alignSelf: "stretch" }}
        data={this.props.notifications}
        renderItem={item}
        removeClippedSubviews={false}
        keyExtractor={(item, index) => `notif-${index}`}
        removeClippedSubviews={false}
        onEndReached={this.handleLoadMore}
        onEndReachedThreshold={0.3}
        refreshControl={
          <RefreshControl
            refreshing={this.props.isFetching}
            onRefresh={this.refreshData}
          />
        }
      />
    );
  }

  renderDeleteDialog = () => {
    return (
      <ModalDialog
        isVisible={this.state.showDeleteDialog}
        title="Apakah Anda yakin akan menghapus data ?"
        onClose={this.hideDeleteDialog}
      >
        <View style={styles.deleteModalWrapper}>
          <GradientBorderButton
            borderGradient={BUTTON_GRADIENT}
            label="Batal"
            backgroundColor="white"
            textColor={COLOR_PRIMARY}
            style={{ alignSelf: "stretch", height: 34 }}
            containerStyle={[
              styles.deleteModalButton,
              { borderRadius: 3, marginRight: 8 }
            ]}
            borderWidth={2}
            onPress={this.hideDeleteDialog}
          />

          <Button
            gradient={BUTTON_GRADIENT}
            label="OK"
            style={styles.deleteModalButton}
            containerStyle={[styles.deleteModalButton, { marginLeft: 8 }]}
            onPress={this.deleteNotification}
            gradientDirection="horizontal"
          />
        </View>
      </ModalDialog>
    );
  };

  hideDeleteDialog = () => {
    this.setState({
      showDeleteDialog: false,
    });
  };

  confirmDelete = () => {
    this.setState({
      showDeleteDialog: true
    });
  };

  render() {
    
    return (
      <KeyboardAvoidingView style={containerStyles.container}>
        {/* <FullPageLoading
          isVisible={this.props.isFetching}
          text="Mengambil data ..."
        /> */}

        <View style={styles.listWrapper}>
          {this._renderList()}
        </View>
        {this.renderDeleteDialog()}
      </KeyboardAvoidingView>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    notifications: state.notification.list || [],
    isFetching: state.notification.isFetching,
    lastPage: state.notification.lastPage,
  };
};

export default connect(mapStateToProps)(Notification);
