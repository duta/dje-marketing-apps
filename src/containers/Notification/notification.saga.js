import { 
  takeEvery, 
  takeLatest,
  call,
  all, 
  put 
} from "redux-saga/effects";
import Api from "../../apis/api";
import { 
  NOTIFICATIONS_FETCHED, 
  NOTIFICATIONS_FETCH_FAILED, 
  FETCH_NOTIFICATIONS,
  NOTIFICATION_DELETE,
  NOTIFICATION_DELETED,
  NOTIFICATION_READ,
  NOTIFICATION_READ_ID
} from "./notification.action";
import ApiUtils from "../../apis/ApiUtils";

function fetchNotifications(params) {
  // const read = Api.get('notifications-read-all', {}, {}, true, API_V3_PREFIX);
  return Api.get('notifications',params);
}

function* handleFetchNotifications(action) {
  try {
    const response = yield call(fetchNotifications,action.params);
    const { data, ...meta } = response.data;

    yield put({
      type: NOTIFICATIONS_FETCHED,
      data,
      meta
    })
  } catch (errors) {
    yield put({
      type: NOTIFICATIONS_FETCH_FAILED,
      erorrs: ApiUtils.handleErrors(errors)
    });
  }
}

function deleteNotifications(ids) {
  return Api.post('notifications/delete', {
      selecteds: ids
    });
}

function* handleDeleteNotification(action) {
  try {
    const response = yield call(deleteNotifications, action.ids);
    yield put({
      type: FETCH_NOTIFICATIONS
    })
  } catch (errors) {
    yield put({
      type: NOTIFICATIONS_FETCH_FAILED,
      errors: ApiUtils.handleErrors(errors)
    })
  }
}

function handleReadtNotification(){
  return Api.get('notifications');
}

function readNotifications(ids) {
  return Api.post('notifications/mark-as-read',{ids});
}

function* handleReadIdtNotification(action) {
  try {
    const response = yield call(readNotifications,action.ids);
    console.log("berhasil",response)
    yield put({
      type: FETCH_NOTIFICATIONS,
    })
  } catch (errors) {
    yield put({
      type: NOTIFICATIONS_FETCH_FAILED,
      erorrs: ApiUtils.handleErrors(errors)
    });
  }
}

export default function* watchAll() {
  yield all([
    takeLatest(FETCH_NOTIFICATIONS, handleFetchNotifications),
    takeLatest(NOTIFICATION_DELETE, handleDeleteNotification),
    takeLatest(NOTIFICATION_READ, handleReadtNotification),
    takeLatest(NOTIFICATION_READ_ID, handleReadIdtNotification)
  ]);
}
