import Api from "../../apis/api";

export const NOTIFICATION_RECEIVED = "NOTIFICATION_RECEIVED";
export const NOTIFICATION_DELETE = "NOTIFICATION_DELETE";
export const NOTIFICATION_DELETED = "NOTIFICATION_DELETED";
export const NOTIFICATION_READ = "NOTIFICATION_READ";
export const NOTIFICATION_READ_ID = "NOTIFICATION_READ_ID";

export const FETCH_NOTIFICATIONS = "FETCH_NOTIFICATIONS";
export const NOTIFICATIONS_FETCHED = "NOTIFICATIONS_FETCHED";
export const NOTIFICATIONS_FETCH_FAILED = "NOTIFICATIONS_FETCH_FAILED";

export function receiveNotification(data) {
  return {
    type: NOTIFICATION_RECEIVED,
    data
  };
}

export function deleteNotification(ids) {
  return {
    type: NOTIFICATION_DELETE,
    ids
  };
}

export function readNotification() {
  return {
    type: NOTIFICATION_READ
  };
}

export function fetchNotifications(params) {
  return {
    type: FETCH_NOTIFICATIONS,
    params
  };
}

export function readNotificationId(ids) {
  return {
    type: NOTIFICATION_READ_ID,
    ids
  };
}
