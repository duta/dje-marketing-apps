import React, { Component } from "react";
import { NavigationActions, StackActions } from "react-navigation";
// import { NotificationsAndroid } from "react-native-notifications";
import { readNotification, fetchNotifications } from "./notification.action";
import { storeFcmToken } from "../Login/login.actions";
import { connect } from "react-redux";
import firebase from 'react-native-firebase';
import type { Notification, NotificationOpen  } from 'react-native-firebase';
 
class NotificationRecived extends Component {

  constructor(props) {
    super(props);

    this.handleReceivedNotification = this.handleReceivedNotification.bind(this);
  }

  componentDidMount() {
    const enable = firebase.messaging().hasPermission();

    firebase.notifications().onNotification((notification) => {
        this.props.dispatch(fetchNotifications());
        const localNotification = new firebase.notifications.Notification({
          sound: 'default',
          show_in_foreground: true,
        })
        .setNotificationId(notification.notificationId)
        .setTitle(notification.title)
        .setSubtitle(notification.subtitle)
        .setBody(notification.body)
        .setData(notification.data)
        .android.setChannelId('channelId')
        .android.setSmallIcon('ic_launcher')
        .android.setPriority(firebase.notifications.Android.Priority.High);

        firebase.notifications().displayNotification(localNotification)
        .catch(err => console.error(err));;
    });

    firebase.notifications().onNotificationOpened((notificationOpen: NotificationOpen) => {
      const data = notificationOpen.notification.data;
      this.handleReceivedNotification(data)
    });

  }

  handleReceivedNotification(notification) {
    this.props.dispatch(readNotification());
    this.props.dispatch(fetchNotifications());
    this.props.dispatch(
      NavigationActions.navigate({
        routeName: notification.navigation,
        params: { 
          id: parseInt(notification.id),
          data: JSON.parse(notification.data)
        }
      })
    );
  }

  componentWillUnmount() {
    // 
  }

  render() {
    return null;
  }
}

const mapStateToProps = props => ({
  nav: props.nav
});

export default connect(mapStateToProps)(NotificationRecived);
