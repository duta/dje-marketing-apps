import * as actions from "./notification.action";
// import Faker from "faker/locale/id_ID";
import { RESET_STATE } from "../../config/reducer.action";

const initialState = () => {
  return {
    list: [],
    meta: {},
    isFetching: false,
    lastPage:1
  };
};

const deleteNotification = (state, ids) => {
  const newList = state.list.filter(
    notification => !ids.includes(notification.id)
  );
  return {
    list: newList
  };
};

const addNewNotification = (state, notif) => {
  let newList = state.list;
  newList.push(notif);

  return {
    list: newList
  };
};

const updateFetchedList = (list,data) => {
  var newList = [...list,...data];

  return newList;
};

const reducer = (state = initialState(), action) => {
  const actType = action.type;

  switch (actType) {
    case actions.FETCH_NOTIFICATIONS:
      return {
        ...state,
        isFetching: true
      };
    case actions.NOTIFICATIONS_FETCHED:
      return {
        ...state,
        list: action.data.map(notif => ({
          id: notif.id,
          read_at: notif.read_at,
          date: notif.created_at,
          ...notif.data, 
        })),
        meta: action.meta,
        isFetching: false,
        lastPage: action.meta.last_page
      };
    case actions.NOTIFICATIONS_FETCH_FAILED:
      return {
        ...state,
        isFetching: false
      }
    case actions.NOTIFICATION_RECEIVED:
      return addNewNotification(state, action.data);
    // case actions.NOTIFICATION_DELETE:
    //   const ids = action.ids;
    //   return deleteNotification(state, ids);
    case RESET_STATE:
      return { list: [] };
    default:
      return state;
  }
};

export default reducer;
