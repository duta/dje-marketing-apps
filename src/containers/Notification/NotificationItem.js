import React from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import PropTypes from "prop-types";
import Icon from "react-native-vector-icons/MaterialIcons";
import { COLOR_PRIMARY } from "../../config/common";
import { formatDate } from "../../utils/date";

const NotificationItem = props => {
  const onPress = () => props.onPress(props.id);
  const onLongPress = () => props.onLongPress(props.id);

  return (
    <TouchableOpacity
      activeOpacity={0.8}
      onPress={onPress}
      onLongPress={onLongPress}
      delayLongPress={500}
      style={{ alignSelf: "stretch" }}
    >
      <View style={styles.wrapper}>
        {props.isChecked && (
          <View style={styles.check}>
            <Icon name="check-circle" color={COLOR_PRIMARY} size={24} />
          </View>
        )}
        {props.read && (
          <View style={styles.checkRead}>
            <Icon name="done-all" color={COLOR_PRIMARY} size={24} />
          </View>
        )}
        <Text style={styles.date}>
          { formatDate(props.date, "dddd, DD MMMM YYYY")+' Pk. '+formatDate(props.date, "HH:mm") }
        </Text>
        <Text style={styles.title}>{props.title}</Text>
        <Text style={styles.body}>{props.body}</Text>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: "white",
    marginBottom: 4,
    padding: 16,
    alignSelf: "stretch"
  },
  title: {
    fontWeight: "bold",
    fontFamily: "Arial",
    marginTop: 2,
    marginBottom: 4
  },
  body: {
    fontFamily: "Quicksand",
    fontSize: 12
  },
  check: {
    position: "absolute",
    top: 55,
    right: 8
  },
  checkRead: {
    position: "absolute",
    top: 8,
    right: 8
  },
  date: {
    fontFamily: "Quicksand",
    fontSize: 11
  }
});

NotificationItem.propTypes = {
  title: PropTypes.string,
  body: PropTypes.string,
  onPress: PropTypes.func,
  isChecked: PropTypes.bool
};

NotificationItem.defaultProps = {
  isChecked: false
};

export default NotificationItem;
