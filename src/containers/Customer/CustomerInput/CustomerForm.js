import React, { Component } from "react";
import {
  KeyboardAvoidingView,
  ScrollView,
  Text,
  View,
} from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import { TextInputWithLabel } from "../../../components/TextInput";
import { Select } from "../../../components/Select";
import formStyles from "../../../styles/form";
import styles from "./styles";
import _ from "lodash";
import { clean } from "../../../utils/array";
import {
  formatPhone,
  clearPhoneFormat,
  filterPhoneNumber
} from "../../../utils/string";
import { NavigationActions } from "react-navigation";
import { connect } from "react-redux";
import { BUTTON_GRADIENT, COLOR_GREY } from "../../../config/common";
import { Button } from "../../../components/Button";

let initialState = {
  fields: {
    name: null,
    pic: null,
    regency_id: null,
    type: null,
    phone: null,
    address: null,
    description:null,
  }
};

class CustomerForm extends Component {
  state = {
    fields: initialState.fields,
    selectedRegency: null,
  };

  static defaultProps = {
    errors: []
  };

  initializeData = (fields) => {

    switch (fields.type) {
      case 0:
        fields["type_index"] = 0;
        break;
      case 1:
        fields["type_index"] = 1;
        break;
      case 2:
        fields["type_index"] = 2;
        break;
      case 3:
        fields["type_index"] = 3;
        break;  
      default:
        fields["type_index"] = null;
        break;
    }
    
    return fields;
  };

  componentDidMount() {
    var fields = {
      ... this.state.fields,
      ... this.props.customer,
    };

    fields = this.initializeData(fields);

    this.setState({
      ...this.state,
      selectedRegency : this.props.regency,
      fields
    });
  }

  resetFields = () => {
    this.setState({ 
      fields: initialState.fields,
      selectedRegency: null
    });
  };

  componentWillUnmount() {
    this.resetFields();
  }

  onSubmit() {
    if (typeof this.props.onSubmit === "function") {
      const data = {
        ...this.state.fields,
        phone: clearPhoneFormat(this.state.fields.phone)
      };
      this.props.onSubmit(clean(data));
    }
  }

  updateTextField = field => value => {
    let newFields = { ...this.state.fields };
    newFields[field] = value;
    this.setState({
      ...this.state,
      fields: { ...newFields }
    });
  };

  updatePickerField = field => (value, index) => {
    let newFields = { ...this.state.fields };
    newFields[field] = value.value;
    newFields[field + "_index"] = index;

    this.setState({
      ...this.state,
      fields: { ...newFields }
    });
  };

  renderErrorMessage = field => {
    const { errors } = this.props;

    // No error
    if (typeof errors === "undefined" || typeof errors[field] === "undefined") {
      return null;
    } else {
      // Error found
      return <Text style={styles.errorMessage}> {errors[field][0]} </Text>;
    }
  };

  selectRegency = () => {
    const { dispatch } = this.props;

    dispatch(
      NavigationActions.navigate({
        routeName: "SelectRegency",
        params: {
          type:"single",
          callback: this.onRegencySelected
        }
      })
    );
  };

  onRegencySelected = regency => {
    const fields = {
      ...this.state.fields,
      regency_id:regency.id
    }
    this.setState({
      selectedRegency: regency,
      fields:fields
    });
  };

  getSubmitButtonLabel = () => {
    if (this.props.isLoading) {
      return "Menyimpan data ...";
    } else {
      return "Simpan";
    }
  };

  render() {
    const { fields } = this.state;
    const props = this.props;
    
    return (
      <KeyboardAvoidingView behavior="padding" style={{flex: 1, alignSelf: "stretch"}}>
        <ScrollView style={styles.form} keyboardShouldPersistTaps="always">
          <View style={styles.section}>
            <TextInputWithLabel
              label="Nama Perusahaan"
              style={formStyles.inputWrapper}
              inputStyle={formStyles.input}
              labelStyle={formStyles.inputLabel}
              onChangeText={this.updateTextField("name")}
              value={fields.name}
              autoCapitalize="words"
            />
            {this.renderErrorMessage("name")}
          </View>
          <View style={styles.section}>
            <TextInputWithLabel
              label="PIC"
              style={formStyles.inputWrapper}
              inputStyle={formStyles.input}
              labelStyle={formStyles.inputLabel}
              onChangeText={this.updateTextField("pic")}
              value={fields.pic}
              autoCapitalize="words"
            />
            {this.renderErrorMessage("pic")}
          </View>
          <View style={styles.section}>
            <Select
              label="Tipe Customer"
              title="Pilih Tipe Customer"
              style={[formStyles.selectWrapper]}
              inputStyle={formStyles.select}
              labelStyle={formStyles.inputLabel}
              selectedIndex={fields.type_index}
              onItemSelected={this.updatePickerField("type")}
              items={[
                { label: "Owner", value: 0 },
                { label: "Konsultan", value: 1 },
                { label: "Kontraktor", value: 2 },
                { label: "Sub Kontraktor", value: 3 },
              ]}
            />
            {this.renderErrorMessage("type")}
          </View>
          <View style={styles.section}>
            <TextInputWithLabel
              label="Nomor Telepon/Hp"
              style={formStyles.inputWrapper}
              inputStyle={formStyles.input}
              labelStyle={formStyles.inputLabel}
              keyboardType="numeric"
              onChangeText={val => this.updateTextField("phone")(filterPhoneNumber(val))}
              value={formatPhone(fields.phone)}
              maxLength={15}
            />
            {this.renderErrorMessage("phone")}
          </View>
          <View style={styles.section}>
            <TextInputWithLabel
              label="Kota/Kabupaten"
              placeholder={"Pilih kota/kabupaten"}
              style={formStyles.inputWrapper}
              inputStyle={[formStyles.input, { textAlignVertical: "center" }]}
              labelStyle={formStyles.inputLabel}
              editable={false}
              value={
                this.state.selectedRegency !== null
                ? this.state.selectedRegency.name
                : null}
              onTouchStart={this.selectRegency}
            />
            {this.renderErrorMessage("regency_id")}
          </View>
          <View style={styles.section}>
            <TextInputWithLabel
              label="Alamat"
              style={formStyles.inputWrapper}
              inputStyle={[formStyles.input, { textAlignVertical: "top" }]}
              labelStyle={formStyles.inputLabel}
              multiline={true}
              onChangeText={this.updateTextField("address")}
              value={fields.address}
              autoCapitalize="words"
            />
            {this.renderErrorMessage("address")}
          </View>
          <Button
            label={this.getSubmitButtonLabel()}
            style={formStyles.button}
            gradient={BUTTON_GRADIENT}
            gradientDirection="horizontal"
            disabled={this.props.isDisabled || this.props.isLoading}
            onPress={() => this.onSubmit()}
            containerStyle={{ elevation: 3 }}
          />
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}

export default connect()(CustomerForm);
