import EStyleSheet from "react-native-extended-stylesheet";

const styles = EStyleSheet.create({
  list: {
    flex: 1,
    alignSelf: "stretch"
  },
  listItem: {
    flex: 1,
    paddingVertical: 8,
    paddingHorizontal: 16,
    flexDirection: "row",
    backgroundColor: "white",
    alignItems: "center"
  },
  nama: {
    flex: 1
  },
  thumbnail: {
    width: 32,
    height: 32,
    borderWidth: 1,
    borderColor: "#CCCCCC",
    borderRadius: 16,
    resizeMode: "cover",
    marginRight: 8
  },
  check: {
    width: 24,
    height: 24,
    borderRadius: 12,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "$primaryColor"
  },
  uncheck: {
    width: 24,
    height: 24,
    borderRadius: 12,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#CCCCCC"
  }
});

export default styles;
