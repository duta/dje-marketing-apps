import React, { Component } from "react";
import { ScrollView, Text, View, Alert, TouchableOpacity } from "react-native";
import Modal from "react-native-modal";
import Icon from "react-native-vector-icons/MaterialIcons";
import styles from "../../../styles/filter_form";
import {
  COLOR_PRIMARY,
  COLOR_GREY,
  BUTTON_GRADIENT
} from "../../../config/common";
import { TextInput, TextInputWithLabel } from "../../../components/TextInput";
import { Button, GradientBorderButton } from "../../../components/Button";
// import CheckBox from "../../../components/CheckBox";
import formStyles from "../../../styles/form";

const FormSection = props => (
  <View style={styles.formSection}>
    <Text style={styles.subTitle}>{props.subTitle}</Text>
    <View style={styles.formContent}>{props.children}</View>
  </View>
);

const CustomerType = props => {
  let styls = [styles.customerType];
  if (props.status) styls.push(styles.cutomerTypeSelected);

  return (
    <TouchableOpacity onPress={props.onPress}>
      <Text style={styls}>{props.customer.name}</Text>
    </TouchableOpacity>
  );
};

class FilterForm extends Component {
  render() {
    const typeList = this.props.types;
    var provinceSelected = this.props.selectedProvince;
    var provinceName = [];
    provinceSelected.forEach(function(item) {
      provinceName.push(item.name);
    });
    return (
      <Modal
        isVisible={this.props.isVisible}
        style={styles.modal}
        onBackdropPress={this.props.onClose}
      >
        <View style={styles.wrapper}>
          <View style={styles.header}>
            <Text style={styles.title}>Filter Pencarian</Text>
            <Text style={styles.closeButton} onPress={this.props.onClose}>
              Tutup
            </Text>
          </View>

          <View style={styles.form}>
            <ScrollView keyboardShouldPersistTaps="always">
              <FormSection subTitle="Type Customer">
                <View style={styles.doctorCategories}>
                  {typeList.map(type => {
                    return (
                      <CustomerType
                        customer={type}
                        key={type.id}
                        onPress={() => this.props.onTypePress(type)}
                        status={
                          this.props.selectedTypes.indexOf(type.id) >
                          -1
                        }
                      />
                    );
                  })}
                </View>
              </FormSection>

              <FormSection subTitle="Provinsi">
                <TextInputWithLabel
                  style={formStyles.inputWrapper}
                  inputStyle={formStyles.input}
                  labelStyle={formStyles.inputLabel}
                  placeholder="Cari Provinsi"
                  onTouchStart={() => this.props.onProvince()}
                  editable={false}
                  value={
                    provinceName.toString() ? provinceName.toString() : null
                  }
                />
              </FormSection>
            </ScrollView>
          </View>

          <View style={styles.footer}>
            <GradientBorderButton
              label="Reset"
              borderWidth={1}
              containerStyle={[styles.button, { marginRight: 8 }]}
              borderGradient={BUTTON_GRADIENT}
              textColor={COLOR_PRIMARY}
              backgroundColor="white"
              style={{ height: 36 }}
              onPress={() => this.props.onFilterReset()}
            />
            <Button
              gradient={BUTTON_GRADIENT}
              gradientDirection="horizontal"
              label="Terapkan"
              style={styles.button}
              containerStyle={[styles.button, { marginLeft: 8 }]}
              onPress={() => this.props.onFilter()}
            />
          </View>
        </View>
      </Modal>
    );
  }
}

export default FilterForm;
