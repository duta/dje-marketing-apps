import React, { Component } from "react";
import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";
import Swipeout from "react-native-swipeout";
import ProfilePicture from "../../../components/ProfilePicture";
import {
  COLOR_PRIMARY,
  COLOR_VIOLET_GREY,
  COLOR_GREY,
  COLOR_DARKEN_RED,
  COLOR_ORANGE
} from "../../../config/common";
import { formatDate } from "../../../utils/date";

const OptionButton = props => (
  <TouchableOpacity
    activeOpacity={0.8}
    onPress={props.onPress}
    onLongPress={props.onLongPress}
    delayLongPress={4000}
    style={[styles.optionButton, { backgroundColor: props.backgroundColor }]}
  >
    <Icon name={props.icon} color="white" size={16} />
    <Text style={styles.buttonText}>{props.label}</Text>
  </TouchableOpacity>
);

class ListItem extends Component {
  renderOptionsButton = () => [
    {
      component: (
        <OptionButton
          icon="edit"
          backgroundColor={COLOR_ORANGE}
          label="Edit"
          onPress={this.props.editAction}
        />
      )
    }
  ];

  renderCheck = () => {
    if (!this.props.isChecked) return null;

    return (
      <View style={styles.check}>
        <Icon size={20} color={COLOR_PRIMARY} name="check-circle" />
      </View>
    );
  };

  render() {
    const {name, type, address} = this.props;
  
    switch (type) {
      case 0:
        type_name= "Owner"
        break;
      case 1:
        type_name= "Konsultan"
        break;
      case 2:
        type_name= "Kontraktor"
        break;
      case 3:
        type_name= "Sub Kontraktor"
        break;
    
      default:
        type_name= "Owner"
        break;
    }
    return (
      <Swipeout
        disabled={this.props.swipeDisable}
        right={this.renderOptionsButton()}
      >
        <TouchableOpacity
          activeOpacity={0.8}
          style={styles.wrapper}
          onPress={this.props.onPress}
          onLongPress={this.props.onLongPress}
        >
          <View style={styles.content}>
            <Text style={styles.name}>{name}</Text>
            <Text style={styles.gender}>
              {type_name}
            </Text>
            <Text style={styles.age}>
              {address}
            </Text>
          </View>

          {this.renderCheck()}
        </TouchableOpacity>
      </Swipeout>
    );
  }
}

const styles = StyleSheet.create({
  optionButton: {
    justifyContent: "center",
    alignItems: "center",
    flex: 1
  },
  buttonText: {
    color: "white",
    fontSize: 12,
    fontFamily: "Quicksand"
  },
  wrapper: {
    paddingHorizontal: 16,
    borderBottomColor: "rgba(117, 123, 149, 0.3)",
    borderBottomWidth: 1,
    flexDirection: "row",
    backgroundColor: "white",
    alignItems: "center"
  },
  content: {
    flex: 1,
    paddingVertical: 8
  },
  photo: {
    width: 40,
    height: 40,
    borderRadius: 20,
    marginRight: 16,
    marginVertical: 8,
    resizeMode: "cover"
  },
  name: {
    fontWeight: "bold",
    fontFamily: "Arial",
    fontSize: 13,
    color: COLOR_VIOLET_GREY
  },
  gender: {
    color: "rgba(117, 123, 149, 0.8)",
    fontFamily: "Quicksand",
    fontSize: 13
  },
  birthDate: {
    color: "rgba(117, 123, 149, 0.8)",
    fontFamily: "Quicksand",
    fontSize: 13
  },
  age: {
    color: "rgba(117, 123, 149, 0.6)"
  },
  check: {
    position: "absolute",
    right: 16
  }
});

export default ListItem;
