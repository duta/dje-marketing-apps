import EStyleSheet from "react-native-extended-stylesheet";
import { Dimensions } from "react-native";
import {
  COLOR_PRIMARY,
  COLOR_GREY,
  COLOR_VIOLET_GREY
} from "../../../config/common";

const styles = EStyleSheet.create({
  list: {
    flexGrow: 1,
    alignSelf: "stretch"
  },
  deleteModalWrapper: {
    flexDirection: "row",
    alignSelf: "stretch",
    paddingVertical: 16
  },
  deleteModalButton: {
    height: 38,
    flex: 1
  },
  listWrapper: {
    flex: 1,
    marginBottom: 48
  },
  filterButtonWrapper: {
    position: "absolute",
    width: Dimensions.get("window").width,
    bottom: 0,
    left: 0,
    flexDirection: "row",
    borderTopColor: "rgba(117, 123, 149, 0.2)",
    borderTopWidth: 1
  },
  filterButton: {
    flex: 1,
    height: 48,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white",
    flexDirection: "row"
  },
  filterButtonBorder: {
    borderRightColor: "rgba(117, 123, 149, 0.2)",
    borderRightWidth: 1
  },
  filterButtonText: {
    color: COLOR_PRIMARY,
    fontFamily: "Arial",
    fontWeight: "bold",
    marginLeft: 8
  },
});

export default styles;
