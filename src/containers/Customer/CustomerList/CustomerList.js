import React, { Component } from "react";
import CustomComponent from "../../../components/CustomComponent";
import { BackHandler, FlatList, View, Text, SafeAreaView, TouchableOpacity, KeyboardAvoidingView, Keyboard, RefreshControl } from "react-native";
import { NavigationActions, HeaderProps } from "react-navigation";
import { connect } from "react-redux";
import SnackBar from "react-native-snackbar";
import containerStyle from "../../../styles/container";
import ActionButton from "../../../components/ActionButton";
import ListItem from "./ListItem";
import styles from "./styles";
import {
  COLOR_PRIMARY,
  COLOR_SOFTEN_RED
} from "../../../config/common";
import {
  fetchCustomer,
  resetCustomer
} from "../customer.action";
import inflateError from "../../../utils/inflateError";
import SearchBar from "../../../components/SearchBar";
import Icon from "react-native-vector-icons/MaterialIcons";
import FilterForm from "./FilterForm";
import { Loading, FullScreenLoading } from "../../../components/Loading";
import _ from "lodash";

class CustomerList extends CustomComponent {
  state = {
    searching: false,
    searchText: null,
    loading: false,
    fetched: false,
    showFilterForm: false,
    selectedTypes: [],
    selectedProvince: [],
    selectedProvinceData:[],
    currentPage: 1
  };

  constructor(props) {
    super(props);

    this.requestData = this.requestData.bind(this);
    this.handleLoadMore = this.handleLoadMore.bind(this);
    this.refreshData = this.refreshData.bind(this);
  }

  static navigationOptions = ({ navigation }) => ({
    header: navigation.state.params.searchBar || HeaderProps,
    headerRight: (
      <View style={{ paddingHorizontal: 16 }}>
        {navigation.state.params.right !== undefined
          ? navigation.state.params.right
          : null}
      </View>
    )
  });

  componentWillUnmount() {
    super.componentWillUnmount();
    this.cancelSearch();
  }

  componentWillMount() {
    this.setState({loading: true});
  }

  componentDidFocus() {
    this.setState({loading: false});
    this._showSearchButton();

    if (!this.state.fetched) {
      this.requestData();
      this.setState({fetched: true});
    }
  }

  /**
   * Show search button
   *
   */
  _showSearchButton = () => {
    const { searching } = this.state;
    if (!searching) {
      let margin = 16;
      let add_button = <ActionButton onPress={this.onAddButtonPress} icon="add" />;
      this.props.navigation.setParams({
        right: (
          <View style={{ flexDirection: "row" }}>
            <ActionButton
              onPress={this._showSearchForm}
              icon="search"
              style={{ marginRight: margin }}
            />
            {add_button}
          </View>
        )
      });
    }
  };

  /**
   * Do search
   *
   */
  doSearch = text => {
    this.setState({ searchText: text });
    this.refreshData();
  };

  /**
   * Cancel search
   *
   */
  cancelSearch = () => {
    this.props.navigation.setParams({ searchBar: null });
    this.setState({ searching: false, searchText: null });
    this.refreshData();
  };

  /**
   * Show search form on header
   *
   */
  _showSearchForm = () => {
    this.setState({ searching: true });
    this.props.navigation.setParams({
      searchBar: (
        <SafeAreaView style={{backgroundColor: COLOR_PRIMARY}}>
          <SearchBar
            backgroundColor={COLOR_PRIMARY}
            placeholder="Ketikan nama customer"
            onCancel={this.cancelSearch}
            onChangeText={this.doSearch}
            text={this.state.searchText}
          />
        </SafeAreaView>
      )
    });
  };


  requestData() {
    const params = {
      page : this.state.currentPage,
      search : this.state.searchText,
      type : this.state.selectedTypes,
      province : this.state.selectedProvince
    }
    this.props.dispatch(fetchCustomer(params));
  }

  refreshData(){
    this.props.dispatch(resetCustomer());
    this.setState({
      currentPage: 1,
    },() => {this.requestData()});
  }

  showCustomerDetail = customer => {
    const { id } = customer;
    this.props.dispatch(
      NavigationActions.navigate({ routeName: "CustomerDetail", params: { id } })
    );
  };

  onEditButtonPress = customer => {
    this.props.dispatch(
      NavigationActions.navigate({
        routeName: "CustomerInput",
        params: {
          title: "Edit Customer",
          id: customer.id
        }
      })
    );
  };

  onItemPress = customer => {
      this.showCustomerDetail(customer);
  };


  componentWillReceiveProps(nextProps) {
    if (!this.props.isError && nextProps.isError) {
      setTimeout(() => {
        SnackBar.show({
          title: inflateError(nextProps.errors),
          duration: SnackBar.LENGTH_LONG,
          backgroundColor: COLOR_SOFTEN_RED
        });
      }, 500);
    }
  }

  handleLoadMore() {
    if (this.props.lastPage !== this.state.currentPage) {
      this.setState({
        currentPage: parseInt(this.state.currentPage) + 1,
      },() => {this.requestData()});
    }
  };
  
  renderList() {
    const customers = Object.values(this.props.customers) || [];
    const listItem = listItem => {
      const customer = listItem.item;

      return (
        <ListItem
          name={customer.name}
          type={customer.type}
          address={customer.address}
          onPress={() => this.onItemPress(customer)}
          editAction={() => this.onEditButtonPress(customer)}
          // deleteAction={() => this.onDeleteButtonPress(customer)}
          key={`customer-list-${customer.id}`}
          swipeDisable={this.state.selecting}
        />
      );
    };

    return (
      <FlatList
        data={customers}
        renderItem={listItem}
        style={styles.list}
        extraData={this.state}
        keyExtractor={item => `key-${item.id}`}
        removeClippedSubviews={false}
        onEndReached={this.handleLoadMore}
        onEndReachedThreshold={0.3}
        refreshControl={
          <RefreshControl
            refreshing={this.props.isFetching}
            onRefresh={this.refreshData}
          />
        }
      />
    );
  }

  onAddButtonPress = () => {
    this.props.dispatch(
      NavigationActions.navigate({
        routeName: "CustomerInput",
        params: {
          title: "Tambah Customer"
        }
      })
    );
  };

  renderFilterButton = () => {
    return (
      <View style={styles.filterButtonWrapper}>
        <TouchableOpacity
          style={[styles.filterButton, styles.filterButtonBorder]}
          onPress={this.showFilterForm}
        >
          <Icon name="filter-list" size={24} color={COLOR_PRIMARY} />
          <Text style={styles.filterButtonText}>Filter</Text>
        </TouchableOpacity>
      </View>
    );
  };

  showFilterForm = () => {
    this.setState({ showFilterForm: true });
  };

  hideFilterForm = () => {
    this.setState({ showFilterForm: false });
  };
  
  onFilterTypeItemPress = type => {
    var selectedTypes = this.state.selectedTypes;
    const itemIndex = this.state.selectedTypes.indexOf(type.id);
    if (itemIndex === -1) {
      selectedTypes.push(type.id);
    } else {
      selectedTypes.splice(itemIndex, 1);
    }

    this.setState({
      selectedTypes
    });
  };

  selectProvince = () => {
    Keyboard.dismiss();
    this.props.dispatch(
      NavigationActions.navigate({
        routeName: "SelectProvince",
        params: {
          callback: this.onProvinceSelected,
          type: "multy"
        }
      })
    ); 
    this.hideFilterForm();
  };

  onProvinceSelected = province => {
    this.setState({ 
      selectedProvince:_.map(province , function(data) { return data.id; }),
      selectedProvinceData: province
    });
    this.showFilterForm();
  };

  onFilterReset = () => {
    this.setState({
      selectedTypes: [],
      selectedProvince: [],
      selectedProvinceData:[]
    });
  };

  onFilter = () => {
    this.refreshData();
    this.hideFilterForm();
  };

  render() {
    const typeList = this.props.types;

    return (
      <KeyboardAvoidingView style={containerStyle.container}>
        <View style={styles.listWrapper}>
          {this.renderList()}
        </View>
        {this.renderFilterButton()}
        <FilterForm
          types={typeList}
          isVisible={this.state.showFilterForm}
          onClose={this.hideFilterForm}
          onTypePress={this.onFilterTypeItemPress}
          selectedTypes={this.state.selectedTypes}
          onProvince={this.selectProvince}
          selectedProvince={this.state.selectedProvinceData}
          onFilter={this.onFilter}
          onFilterReset={this.onFilterReset}
        />
      </KeyboardAvoidingView>
    );
  }
}

const mapStateToProps = state => {
  const data_type = [
    {
      id: 0,
      name: "Owner",
    },
    {
      id: 1,
      name: "Konsultan",
    },
    {
      id: 2,
      name: "Kontraktor",
    },
    {
      id: 3,
      name: "Sub Kontraktor",
    }];

  return {
    customers: state.customer.list,
    isFetching: state.customer.isFetching,
    isError: state.customer.isError,
    errors: state.customer.errors,
    types:data_type,
    lastPage:state.customer.lastPage
  };
};

export default connect(mapStateToProps)(CustomerList);
