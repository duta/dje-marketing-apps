import * as actionTypes from "./customer.actionTypes";

export function resetCustomer(){
  return {
    type:actionTypes.RESET_CUSTOMER
  }
}

export function fetchCustomer(params) {
  return {
    type: actionTypes.FETCH_CUSTOMERS,
    params
  };
}

export function addCustomer(data) {
  return {
    type: actionTypes.ADD_CUSTOMER,
    data
  };
}

export function updateCustomer(id, data) {
  return {
    type: actionTypes.UPDATE_CUSTOMER,
    id,
    data
  };
}

export function clearError() {
  return {
    type: actionTypes.CUSTOMER_CLEAR_ERROR
  };
}
