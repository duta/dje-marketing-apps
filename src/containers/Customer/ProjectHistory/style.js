import EStyleSheet from "react-native-extended-stylesheet";
import { COLOR_PRIMARY } from "../../../config/common";

const styles = EStyleSheet.create({
  header: {
    borderLeftWidth: 6,
    backgroundColor: "white",
    alignSelf: "stretch",
    height: 85
  },
  itemHeader: {
    alignSelf: "stretch",
    flex: 1
  },
  itemTitle: {
    fontFamily: "Arial",
    fontWeight: "bold",
    marginRight: 12
  },
  itemTitleDate: {
    fontFamily: "Arial",
    color: COLOR_PRIMARY,
    marginRight: 12,
    fontSize: 11
  },
  itemTitleContent: {
    fontFamily: "Arial",
    marginRight: 12,
    paddingTop: 7
  },
  wrapper: {
    alignSelf: "stretch",
    marginBottom: 1,
    width: "100%"
  },
  content: {
    padding: 8,
    borderLeftWidth: 6,
    backgroundColor: "white",
    alignSelf: "stretch"
  },
  contentItem: {
    fontFamily: "Arial",
    marginLeft: 9,
    paddingTop: 7
  },
  item: {
    backgroundColor: "white",
    alignSelf: "stretch",
    marginBottom: 8,
    padding: 8,
    flexDirection: "row",
    alignSelf: "stretch",
    alignItems: "flex-start"
  },
  itemNumber: {
    fontFamily: "Arial",
    color: "rgba(6, 190, 182, 1)",
    marginRight: 16,
    fontWeight: "bold"
  },
  itemDuration: {
    fontFamily: "Arial",
    color: "rgba(6, 190, 182, 1)",
    fontWeight: "bold"
  },
  itemDate: {
    fontFamily: "Arial",
    color: "rgba(6, 190, 182, 1)",
    fontWeight: "bold"
  },
  button: {
    alignSelf: "stretch",
    height: 35
  },
  buttonMargin: {
    marginBottom: 20
  },
  status: {
    fontWeight: "bold",
    fontSize: 14,
    borderRadius: 30,
    paddingVertical: 3,
    paddingHorizontal: 6,
    // color: "white",
    fontFamily: "Arial",
    flexDirection: "row",
    justifyContent: "space-between"
  },
});

export default styles;
