import React, { Component } from "react";
import { Text, TouchableOpacity, View, StyleSheet } from "react-native";
import Accordion from "../../../components/Accordion";
import Icon from "react-native-vector-icons/MaterialIcons";
import styles from "./style";
import ProjectButton from "./ProjectButton";
import { formatDate } from "../../../utils/date";
import { COLOR_PRIMARY,COLOR_ORANGE,COLOR_VIOLET_GREY,COLOR_SOFTEN_RED } from "../../../config/common";

class ListItem extends Component {

  state = {
    expanded: false
  }

  render() {
    const { title, desc, index, date, status, marketing } = this.props;
    var backstyle = styles2.back2;
    if (index % 2 === 0) {
      backstyle = styles2.back1;
    } else {
      backstyle = styles2.back2;
    }
    var desccut = "";
    if(desc){
      desccut = desc.substring(0,30);
    }

    let statusStyles = [styles.status];
    switch (status) {
      case 0:
        statusStyles.push({ color: COLOR_ORANGE,height:23 });
        text = "Perencanaan";
        break;
      case 1:
        statusStyles.push({ color: "blue",height:23 });
        text = "Tender";
        break;
      case 2:
        statusStyles.push({ color: COLOR_PRIMARY,height:23 });
        text = "PO";
        break;
      case 3:
        statusStyles.push({ color: COLOR_VIOLET_GREY,height:23 });
        text = "Selesai";
        break;
      case 4:
        statusStyles.push({ color: COLOR_SOFTEN_RED,height:23 });
        text = "Batal";
        break;
    }
    
    return (
      <Accordion
        wrapperStyle={styles.wrapper}
        headerStyle={[styles.header, backstyle]}
        renderHeader={
          <View style={[styles.itemHeader, {alignContent: "center"}]}>
            <Text style={styles.itemTitle}>{`${title} (${marketing})`}</Text>
            <Text style={styles.itemTitleDate}>
              {`${formatDate(date, "DD MMMM YYYY")} - `} 
              <Text style={statusStyles}>{text}</Text>
            </Text>
            {!this.state.expanded && (
              <Text style={styles.itemTitleContent}>{`${desccut}...`}</Text>
            )}
          </View>
        }
        upArrow={<ProjectButton style={styles.buttonMargin} isActive={false} name="Tutup" />}
        downArrow={<ProjectButton style={styles.buttonMargin} isActive={true} name="Detail" />}
        onExpand={() => this.setState({expanded: true})}
        onHide={() => this.setState({expanded: false})}
      >
        <View style={[styles.content, backstyle]}>
          <Text style={styles.contentItem}>{desc}</Text>
        </View>
      </Accordion>
    );
  }
}

const styles2 = StyleSheet.create({
  back1: {
    borderLeftColor: "#cacaca"
  },
  back2: {
    borderLeftColor: COLOR_PRIMARY
  }
});

export default ListItem;
