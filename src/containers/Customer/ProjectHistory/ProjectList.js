import React, { Component } from "react";
import { FlatList, Image, View, Text, RefreshControl } from "react-native";
import { connect } from "react-redux";
import ListItem from "./ListItem";
import { COLOR_VIOLET_GREY } from "../../../config/common";
import { values } from "lodash";
import { fetchProjects,resetProject} from "../../../containers/Project/project.action";
class ProjectList extends Component {
  state = {
    currentPage:1
  }

  constructor(props) {
    super(props);

    this.requestData = this.requestData.bind(this);
    this.handleLoadMore = this.handleLoadMore.bind(this);
    this.refreshData = this.refreshData.bind(this);
  }

  componentDidMount() {
    this.props.dispatch(resetProject());
    this.refreshData();
  }

  handleLoadMore() {
    if (this.props.lastPage !== this.state.currentPage) {
      this.setState({
        currentPage: parseInt(this.state.currentPage) + 1,
      },() => {this.requestData()});
    }
  };

  requestData() {
    const params = {
      page : this.state.currentPage,
      customerId : this.props.customerId,
    }

    this.props.dispatch(fetchProjects(params));
  }

  refreshData(){
    this.setState({
      currentPage: 1,
    },() => {this.requestData()});
  }

  _renderList() {
    const projects = Object.values(this.props.projects) || [];
    const listItem = listItem => {
      const project = listItem.item;
      const index = listItem.index;
      return (
        <ListItem
          title={project.name}
          desc={project.description}
          date={project.date}
          price={project.price}
          status={project.status}
          marketing={project.marketing.name}
          key={`project-list-${project.id}`}
          index={index}
        />
      );
    };

    return (
      <FlatList
        data={projects}
        renderItem={listItem}
        style={{
          alignSelf: "stretch"
        }}
        removeClippedSubviews={false}
        keyExtractor={item => `key-${item.id}`}
        onEndReached={this.handleLoadMore}
        onEndReachedThreshold={0.3}
        refreshControl={
          <RefreshControl
            refreshing={this.props.isFetching}
            onRefresh={this.refreshData}
          />
        }
      />
    );
  }

  render() {
    return (
      <View style={{ flex: 1, alignItems: "flex-start" }}>
          {this._renderList()}
      </View>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    projects: state.project.list || {},
    lastPage:state.project.lastPage,
    isFetching: state.project.isFetching,
  };
};

export default connect(mapStateToProps)(ProjectList);
