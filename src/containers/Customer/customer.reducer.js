import * as actionTypes from "./customer.actionTypes";
import { RESET_STATE } from "../../config/reducer.action";
import { isArray, mapKeys } from "lodash";

const iniialState = {
  isFetching: false,
  isError: false,
  isUpdating: false,
  errors: null,
  list: [],
  lastPage:1
};

const updateList = (list, data) => {
  var newList = list;
  newList[data.id] = data;

  return newList;
};

const reducer = (state = iniialState, action) => {
  const type = action.type;

  switch (type) {
    case actionTypes.FETCH_CUSTOMERS:
      return {
        ...state,
        isFetching: true,
        isError: false
      };
    case actionTypes.CUSTOMER_FETCHED:
      return {
        ...state,
        isFetching: false,
        isError: false,
        list: {
          ...state.list,
          ...mapKeys(action.data.data, "id")
        },
        errors: null,
        lastPage: action.data.last_page
      };
    case actionTypes.UPDATE_CUSTOMER:
    case actionTypes.ADD_CUSTOMER:
      return {
        ...state,
        isUpdating: true,
        isError: false
      };
    case actionTypes.CUSTOMER_UPDATED:
    case actionTypes.CUSTOMER_ADDED:
      return {
        ...state,
        isUpdating: false,
        isError: false,
        list: {
          ...state.list,
          ...updateList(state.list, action.data)
        }
      };
    case actionTypes.UPDATE_CUSTOMER_FAIL:
    case actionTypes.FETCH_CUSTOMER_FAIL:
    case actionTypes.ADD_CUSTOMER_FAIL:
      return {
        ...state,
        isFetching: false,
        isUpdating: false,
        isError: true,
        errors: action.errors
      };
    case actionTypes.CUSTOMER_CLEAR_ERROR:
      return {
        ...state,
        isError: false,
        errors: null
      };
    case RESET_STATE:
    case actionTypes.RESET_CUSTOMER:
      return iniialState;
    default:
      return state;
  }
};

export default reducer;
