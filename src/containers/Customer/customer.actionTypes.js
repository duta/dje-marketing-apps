export const FETCH_CUSTOMERS = "FETCH_CUSTOMERS";
export const CUSTOMER_FETCHED = "CUSTOMER_FETCHED";
export const FETCH_FAMILIES_FAIL = "FETCH_FAMILIES_FAILED";

export const ADD_CUSTOMER = "ADD_CUSTOMER";
export const CUSTOMER_ADDED = "CUSTOMER_ADDED";
export const ADD_CUSTOMER_FAIL = "ADD_CUSTOMER_FAIL";

export const UPDATE_CUSTOMER = "UPDATE_CUSTOMER";
export const CUSTOMER_UPDATED = "FAMILY_UPDATED";
export const UPDATE_CUSTOMER_FAIL = "UPDATE_CUSTOMER_FAIL";

export const CUSTOMER_CLEAR_ERROR = "CUSTOMER_CLEAR_ERROR";
export const RESET_CUSTOMER = "RESET_CUSTOMER";
