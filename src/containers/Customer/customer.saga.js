import { all, call, put, takeLatest } from "redux-saga/effects";
import {
  FETCH_CUSTOMERS,
  FETCH_CUSTOMER_FAIL,
  CUSTOMER_FETCHED,
  ADD_CUSTOMER,
  ADD_CUSTOMER_FAIL,
  CUSTOMER_ADDED,
  UPDATE_CUSTOMER,
  CUSTOMER_UPDATED,
  UPDATE_CUSTOMER_FAIL,
} from "./customer.actionTypes";
import Api from "../../apis/api";
import { API_V3_PREFIX } from "../../config/api";
import ApiUtils from "../../apis/ApiUtils";

function fetchCustomers(params) {
  return Api.get(`customer`,params, {}, true);
  // return require('../../data/customer.json');
}

function* handleFetchingCustomers(action) {
  try {
    const response = yield call(fetchCustomers, action.params);
    const data = response.data;
    yield put({
      type: CUSTOMER_FETCHED,
      data
    });
  } catch (error) {
    yield put({
      type: FETCH_CUSTOMER_FAIL,
      errors: ApiUtils.handleErrors(error)
    });
  }
}

function addCustomer(data) {
  return Api.post("customer", data);
}

function* handleAddingCustomer(action) {
  try {
    const response = yield call(addCustomer, action.data);
    const data = response.data.data;

    yield put({
      type: CUSTOMER_ADDED,
      data
    });
  } catch (error) {
    yield put({
      type: ADD_CUSTOMER_FAIL,
      errors: ApiUtils.handleErrors(error)
    });
  }
}

function updateCustomer(id, data) {
  return Api.patch(`customer/${id}`, data);
}

function* handleUpdatingCustomer(action) {
  try {
    const response = yield call(updateCustomer, action.id, action.data);
    const data = response.data.data;

    yield put({
      type: CUSTOMER_UPDATED,
      data
    });
  } catch (error) {
    yield put({
      type: UPDATE_CUSTOMER_FAIL,
      errors: ApiUtils.handleErrors(error)
    });
  }
}

export default function* watchAll() {
  yield all([
    takeLatest(FETCH_CUSTOMERS, handleFetchingCustomers),
    takeLatest(ADD_CUSTOMER, handleAddingCustomer),
    takeLatest(UPDATE_CUSTOMER, handleUpdatingCustomer),
  ]);
}
