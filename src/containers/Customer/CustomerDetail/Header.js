import React from "react";
import { Image, Text, View } from "react-native";
import LinearGradient from "react-native-linear-gradient";
import AppBar from "../../../components/AppBar";
import { HEADER_GRADIENT, COLOR_PRIMARY } from "../../../config/common";
import styles from "./styles";
import BackButton from "../../../components/BackButton";
import {UpdateButton} from "../../../components/Button";
import ProfilePicture from "../../../components/ProfilePicture";
import Item from "../../../components/Item";

const Header = ({
  name,
  city,
  onBackPressed,
  phone,
  type_name,
  address
}) => {

  return (
    <View>
      <AppBar
        title="Detail Customer"
        backgroundColor={COLOR_PRIMARY}
        titleColor="white"
        headerLeft={<BackButton onPress={onBackPressed} />}
        // headerRight={<UpdateButton onPress={onUpdatePressed}/>}
        headerRight={<Text></Text>}
      />
      <View style={styles.contentHeader}>
        <View style={styles.row}>
          <Item
            label="Nama"
            value={name}
            style={{ flex: 1 }}
          />
          <Item
            label="No. Telepon"
            value={phone || "-"}
            style={{ flex: 1 }}
          />
        </View>
        <View style={styles.row}>
          <Item
            label="Kota/Kabupaten"
            value={city || "-"}
            style={{ flex: 1 }}
          />
          <Item
            label="Tipe"
            value={type_name}
            style={{ flex: 1 }}
          />
        </View>
        <Item label="Alamat" value={address || "-"} style={{ flex: 1 }}/>
      </View>
      <View style={styles.contentSeparator}>
        <View style={styles.separator} />
      </View>
      
    </View>
  );
};

export default Header;
