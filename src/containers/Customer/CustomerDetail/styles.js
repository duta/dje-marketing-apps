import EStyleSheet from "react-native-extended-stylesheet";
import { COLOR_PRIMARY_GREY } from "../../../config/common";

const styles = EStyleSheet.create({
  container: {
    alignItems: "flex-start",
    justifyContent: "flex-start",
    flexGrow: 1,
    backgroundColor: "white"
  },
  content: {
    padding: 16,
    flex: 1,
    alignSelf: "stretch",
    backgroundColor: "white"
  },
  contentHeader: {
    padding: 16,
    // flex: 1,
    alignSelf: "stretch",
    backgroundColor: "white",
    paddingBottom:70
  },
  row: {
    flexDirection: "row",
    alignSelf: "stretch",
    alignItems: "center"
  },
  header: {
    alignSelf: "stretch",
    alignItems: "center",
    justifyContent: "center",
    paddingVertical: 16,
    borderTopColor: COLOR_PRIMARY_GREY,
    borderTopWidth: 1
  },
  profilePict: {
    height: 72,
    width: 72,
    borderRadius: 36,
    marginBottom: 8,
    resizeMode: "cover"
  },
  name: {
    color: "white",
    fontSize: 16,
    fontFamily: "Arial",
    fontWeight: "bold",
    textAlign: "center",
    alignSelf: "stretch"
  },
  tabItemLabel: {
    textAlign: "center",
    fontSize: 12,
    marginHorizontal: 0,
    marginVertical: 4,
    fontFamily: "Arial",
    fontWeight: "500"
  },
  tabBar: {
    flexDirection: "row"
  },
  tabItem: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "white",
    borderColor: "#CCC",
    paddingVertical: 8
  },
  separator: {
    // marginVertical: 16,
    borderTopWidth: 0.6,
    borderTopColor: "rgba(117, 123, 149, 0.5)",
    alignSelf: "stretch",
    backgroundColor: "white"
  },
  contentSeparator: {
    alignSelf: "stretch",
    backgroundColor: "white"
  },
});

export default styles;
