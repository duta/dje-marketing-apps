import React, { Component } from "react";
import { Image, View, Text,TouchableOpacity,Animated } from "react-native";
import { connect } from "react-redux";
import { NavigationActions, HeaderProps } from "react-navigation";
import styles from "./styles";
import {
  HEADER_GRADIENT,
  COLOR_PRIMARY,
  COLOR_GREY
} from "../../../config/common";
import Header from "./Header";
import { TabViewAnimated, TabView } from "react-native-tab-view";
import ProjectHistory from "../ProjectHistory";
import ActivityHistory from "../ActivityHistory";

class CustomerDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tabs: {
        index: 0,
        routes: [
          { key: "project", title: "Project Log" },
          { key: "activity", title: "Aktivitas Log" }
        ]
      },
      keyboardShow: false,
      loading: false
    };
  }

  static navigationOptions = ({ navigation }) => {
    const header = navigation.state.params.header || HeaderProps;

    return {header};
  };
  componentDidMount() {
    this.props.navigation.setParams({
      action: this.navigateToEdit
    });
    this.setHeader();
  }

  setHeader(){
    const { navigation } = this.props;
    const customer = this.props.customer || {};
    let regency_name = null;
    if (customer.regency) {
      regency_name = customer.regency.name;
    }

    switch (customer.type) {
      case 0:
        type_name= "Owner"
        break;
      case 1:
        type_name= "Konsultan"
        break;
      case 2:
        type_name= "Kontraktor"
        break;
      case 3:
        type_name= "Sub Kontraktor"
        break;
    
      default:
        type_name= "Owner"
        break;
    }

    navigation.setParams({header: (
      <Header
        name={customer.name}
        address={customer.address}
        city={regency_name}
        phone={customer.phone}
        type_name={type_name}
        onBackPressed={() => navigation.dispatch(NavigationActions.back())}
        onUpdatePressed={() => this.navigateToEdit()}
      />
    )
    });
  }

  navigateToEdit = () => {
    this.props.dispatch(
      NavigationActions.navigate({
        routeName: "CustomerInput",
        params: {
          title: "Edit Customer",
          id: this.props.customer.id
        }
      })
    );
  };

  renderTab(){
    return (
      <TabViewAnimated
        renderHeader={this.renderTabHeader}
        navigationState={this.state.tabs}
        renderScene={this.renderScene}
        onIndexChange={this.handleTabIndexChange}
      />
    );
  };

  renderTabHeader = props => {
    const inputRange = props.navigationState.routes.map((x, i) => i);
    return (
      <View style={styles.tabBar}>
        {props.navigationState.routes.map((route, i) => {
          const color = props.position.interpolate({
            inputRange,
            outputRange: inputRange.map(
              inputIndex => (props.navigationState.index === i ? COLOR_PRIMARY : "#999")
            )
          });
          
          const borderBottomColor =
            props.navigationState.index === i ? COLOR_PRIMARY : "#CCC";
          const borderBottomWidth = props.navigationState.index === i ? 3 : 1;
          
          return (
            <TouchableOpacity
              activeOpacity={0.8}
              style={[
                styles.tabItem,
                { borderBottomColor, borderBottomWidth: borderBottomWidth }
              ]}
              onPress={() => this.handleTabIndexChange(i)}
              key={i}
            >
              <Animated.Text style={[styles.tabItemLabel, { color }]}>
                {route.title}
              </Animated.Text>
            </TouchableOpacity>
          );
        })}
      </View>
    );
  };

  renderScene = ({ route }) => {
    var id = this.props.customer.id;
    switch (route.key | this.state.tabs.index) {
      case "project" | 0:
        return (<ProjectHistory customerId={id} />);
      case "activity" | 1:
        return (<ActivityHistory customerId={id} />);
    }
  };

  handleTabIndexChange = index => {
    this.setState({
      tabs: {
        ...this.state.tabs,
        index
      }
    });
  }
  
  render() {
    return this.renderTab()
  } 
}

const mapStateToProps = (state, ownProps) => {
  const customerId = ownProps.navigation.state.params.id;
  return {
    customer: state.customer.list[customerId]
  };
};

export default connect(mapStateToProps)(CustomerDetail);
