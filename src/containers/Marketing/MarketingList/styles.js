import EStyleSheet from "react-native-extended-stylesheet";

const styles = EStyleSheet.create({
  list: {
    flexGrow: 1,
    alignSelf: "stretch"
  },
  deleteModalWrapper: {
    flexDirection: "row",
    alignSelf: "stretch",
    paddingVertical: 16
  },
  deleteModalButton: {
    height: 38,
    flex: 1
  },
  listWrapper: {
    flex: 1,
    marginBottom: 48
  },
});

export default styles;
