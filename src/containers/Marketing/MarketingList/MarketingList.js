import React, { Component } from "react";
import { BackHandler, FlatList, View, Text, SafeAreaView, KeyboardAvoidingView, RefreshControl } from "react-native";
import { NavigationActions, HeaderProps } from "react-navigation";
import { connect } from "react-redux";
import containerStyles from "../../../styles/container";
import ActionButton from "../../../components/ActionButton";
import ListItem from "./ListItem";
import styles from "./styles";
import { size } from "lodash";
import {
  BUTTON_GRADIENT,
  COLOR_PRIMARY,
  COLOR_SOFTEN_RED
} from "../../../config/common";
import {
  fetchMarketings,
  resetMarketing
} from "../marketing.action";
import { FullPageLoading } from "../../../components/Loading";
import CustomComponent from "../../../components/CustomComponent";
import SearchBar from "../../../components/SearchBar";

class MarketingList extends CustomComponent {
  state = {
    searching: false,
    searchText: null,
    currentPage:1,
    loading: false,
    fetched: false,
  };

  constructor(props) {
    super(props);

    this.handleLoadMore = this.handleLoadMore.bind(this);
    this.requestData = this.requestData.bind(this);
    this.refreshData = this.refreshData.bind(this);
  }

  static navigationOptions = ({ navigation }) => ({
    header: navigation.state.params.searchBar || HeaderProps,
    headerRight: (
      <View style={{ paddingHorizontal: 16 }}>
        {navigation.state.params.right !== undefined
          ? navigation.state.params.right
          : null}
      </View>
    )
  });

  requestData() {
    const params = {
      page : this.state.currentPage,
      search : this.state.searchText
    }
    this.props.dispatch(fetchMarketings(params));
  }

  refreshData(){
    this.props.dispatch(resetMarketing());
    this.setState({
      currentPage: 1,
    },() => {this.requestData()});
  }

  componentWillUnmount() {
    super.componentWillUnmount();
    this.cancelSearch();
  }

  componentWillMount() {
    this.setState({loading: true});
  }

  componentDidFocus() {
    this.setState({loading: false});
    this._showSearchButton();

    if (!this.state.fetched) {
      this.requestData();
      this.setState({fetched: true});
    }
  }

  /**
   * Show search button
   *
   */
  _showSearchButton = () => {
    const { searching } = this.state;
    if (!searching) {
      let add_button = <ActionButton onPress={this.onAddButtonPress} icon="add" />;
      let margin = 16;
      this.props.navigation.setParams({
        right: (
          <View style={{ flexDirection: "row" }}>
            <ActionButton
              onPress={this._showSearchForm}
              icon="search"
              style={{ marginRight: margin }}
            />
            {add_button}
          </View>
        )
      });
    }
  };

  /**
   * Do search
   *
   */
  doSearch = text => {
    this.setState({ searchText: text });
    this.refreshData();
  };

  /**
   * Cancel search
   *
   */
  cancelSearch = () => {
    this.props.navigation.setParams({ searchBar: null });
    this.setState({ searching: false, searchText: null });
    this.refreshData();
  };

  /**
   * Show search form on header
   *
   */
  _showSearchForm = () => {
    this.setState({ searching: true });
    this.props.navigation.setParams({
      searchBar: (
        <SafeAreaView style={{backgroundColor: COLOR_PRIMARY}}>
          <SearchBar
            backgroundColor={COLOR_PRIMARY}
            placeholder="Ketikan nama marketing"
            onCancel={this.cancelSearch}
            onChangeText={this.doSearch}
            text={this.state.searchText}
          />
        </SafeAreaView>
      )
    });
  };

  showMarketingDetail = marketing => {
    const { id } = marketing;
    this.props.dispatch(
      NavigationActions.navigate({ routeName: "MarketingDetail", params: { id } })
    );
  };

  onEditButtonPress = marketing => {
    this.props.dispatch(
      NavigationActions.navigate({
        routeName: "MarketingInput",
        params: {
          title: "Edit Marketing",
          id: marketing.id
        }
      })
    );
  };


  onItemPress = marketing => {
    this.showMarketingDetail(marketing);
  };

  handleLoadMore() {
    if (this.props.lastPage !== this.state.currentPage) {
      this.setState({
        currentPage: parseInt(this.state.currentPage) + 1,
      },() => {this.requestData()});
    }
  };

  renderList() {
    const marketings = Object.values(this.props.marketings) || [];
    const listItem = listItem => {
      const marketing = listItem.item;

      return (
        <ListItem
          name={marketing.name}
          address={marketing.address}
          gender={marketing.gender}
          photo={marketing.url_photo}
          onPress={() => this.onItemPress(marketing)}
          editAction={() => this.onEditButtonPress(marketing)}
          deleteAction={() => this.onDeleteButtonPress(marketing)}
          key={`marketing-list-${marketing.id}`}
          swipeDisable={this.state.selecting}
        />
      );
    };

    return (
      <FlatList
        data={marketings}
        renderItem={listItem}
        style={styles.list}
        extraData={this.state.currentPage}
        keyExtractor={item => `key-${item.id}`}
        removeClippedSubviews={false}
        onEndReached={this.handleLoadMore}
        onEndReachedThreshold={0.3}
        refreshControl={
          <RefreshControl
            refreshing={this.props.isFetching}
            onRefresh={this.refreshData}
          />
        }
      />
    );
  }

  onAddButtonPress = () => {
    this.props.dispatch(
      NavigationActions.navigate({
        routeName: "MarketingInput",
        params: {
          title: "Tambah Marketing"
        }
      })
    );
  };

  render() {
    return (
      <KeyboardAvoidingView style={containerStyles.container}>
        <View style={styles.listWrapper}>
          {this.renderList()}
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const mapStateToProps = state => {
  return {
    marketings: state.marketing.list,
    isFetching: state.marketing.isFetching,
    isError: state.marketing.isError,
    errors: state.marketing.errors,
    lastPage: state.marketing.lastPage
  };
};

export default connect(mapStateToProps)(MarketingList);
