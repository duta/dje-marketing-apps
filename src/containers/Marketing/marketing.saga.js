import { all, call, put, takeLatest } from "redux-saga/effects";
import {
  MARKETINGS_FETCHED,
  FETCH_MARKETING_FAIL,
  FETCH_MARKETINGS,
  ADD_MARKETING,
  MARKETING_ADDED,
  ADD_MARKETING_FAIL,
  UPDATE_MARKETING,
  MARKETING_UPDATED,
  UPDATE_MARKETING_FAIL,
  FETCH_CHART,
  CHART_FETCHED,
  FETCH_CHART_FAIL
} from "./marketing.actionTypes";
import Api from "../../apis/api";
import ApiUtils from "../../apis/ApiUtils";
import { FETCH_PROFILE } from "../Profile/profile.action";

function fetchMarketings(params) {
  return Api.get(`marketing`, params);
  // return require('../../data/marketing.json');
}

function* handleFetchingMarketings(action) {
  try {
    const response = yield call(fetchMarketings, action.params);
    const data = response.data;

    yield put({
      type: MARKETINGS_FETCHED,
      data
    });
  } catch (error) {
    yield put({
      type: FETCH_MARKETING_FAIL,
      errors: ApiUtils.handleErrors(error)
    });
  }
}

function addMarketing(data) {

  let header = {
    headers: {
      'Content-Type': 'multipart/form-data; charset=utf-8; boundary="another cool boundary";'
    }
  };

  let formData = new FormData();
  
  Object.keys(data).forEach(key => {
    formData.append(key, data[key]);
  });

  return Api.post("marketing", formData, {}, header);
}

function* handleAddingMarketing(action) {
  try {
    const response = yield call(addMarketing, action.data);
    const data = response.data.data;

    yield put({
      
      type: MARKETING_ADDED,
      data
    });
  } catch (error) {
    yield put({
      type: ADD_MARKETING_FAIL,
      errors: ApiUtils.handleErrors(error)
    });
  }
}

function updateMarketing(id, data) {

  let header = {
    headers: {
      'Content-Type': 'multipart/form-data; charset=utf-8; boundary="another cool boundary";'
    }
  };

  let formData = new FormData();
  
  Object.keys(data).forEach(key => {
    if (key != "regency" && key != "last_activity" && key != "user") {
      formData.append(key, data[key]);
    }
    
  });

  return Api.post(`marketing/${id}/update`, formData, {}, header);
}

function* handleUpdatingMarketing(action) {
  try {
    const response = yield call(updateMarketing, action.id, action.data);
    const data = response.data;

    yield put({
      type: FETCH_PROFILE,
    });
    yield put({
      type: MARKETING_UPDATED,
      data
    });
  } catch (error) {
    yield put({
      type: UPDATE_MARKETING_FAIL,
      errors: ApiUtils.handleErrors(error)
    });
  }
}

function fetchChart() {
  return Api.get(`marketing-chart`);
}

function* handleFetchingChart() {
  try {
    const response = yield call(fetchChart);
    const data = response.data;
    yield put({
      type: CHART_FETCHED,
      data
    });
  } catch (error) {
    yield put({
      type: FETCH_CHART_FAIL,
      errors: ApiUtils.handleErrors(error)
    });
  }
}

export default function* watchAll() {
  yield all([
    takeLatest(FETCH_MARKETINGS, handleFetchingMarketings),
    takeLatest(ADD_MARKETING, handleAddingMarketing),
    takeLatest(UPDATE_MARKETING, handleUpdatingMarketing),
    takeLatest(FETCH_CHART, handleFetchingChart),
  ]);
}
