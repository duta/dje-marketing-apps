import React, { Component } from "react";
import { Text, TouchableOpacity, View, StyleSheet } from "react-native";
import Accordion from "../../../components/Accordion";
import Icon from "react-native-vector-icons/MaterialIcons";
import styles from "./style";
import PlanButton from "./PlanButton";
import { formatDate } from "../../../utils/date";
import { COLOR_PRIMARY,COLOR_ORANGE,COLOR_VIOLET_GREY,COLOR_SOFTEN_RED } from "../../../config/common";

class ListItem extends Component {

  state = {
    expanded: false
  }

  render() {
    const { title, desc, index, second_date, first_date } = this.props;
    var backstyle = styles2.back2;
    if (index % 2 === 0) {
      backstyle = styles2.back1;
    } else {
      backstyle = styles2.back2;
    }
    var desccut = "";
    if(desc){
      desccut = desc.substring(0,30);
    }
    return (
      <Accordion
        wrapperStyle={styles.wrapper}
        headerStyle={[styles.header, backstyle]}
        renderHeader={
          <View style={[styles.itemHeader, {alignContent: "center"}]}>
            <Text style={styles.itemTitle}>{title}</Text>
            <Text style={styles.itemTitleDate}>
              {`${formatDate(first_date, "DD MMMM YYYY")} - ${formatDate(second_date, "DD MMMM YYYY")}`} 
            </Text>
            {!this.state.expanded && (
              <Text style={styles.itemTitleContent}>{`${desccut}...`}</Text>
            )}
          </View>
        }
        upArrow={<PlanButton style={styles.buttonMargin} isActive={false} name="Tutup" />}
        downArrow={<PlanButton style={styles.buttonMargin} isActive={true} name="Detail" />}
        onExpand={() => this.setState({expanded: true})}
        onHide={() => this.setState({expanded: false})}
      >
        <View style={[styles.content, backstyle]}>
          <Text style={styles.contentItem}>{desc}</Text>
        </View>
      </Accordion>
    );
  }
}

const styles2 = StyleSheet.create({
  back1: {
    borderLeftColor: "#cacaca"
  },
  back2: {
    borderLeftColor: COLOR_PRIMARY
  }
});

export default ListItem;
