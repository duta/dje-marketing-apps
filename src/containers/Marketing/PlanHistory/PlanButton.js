import PropTypes from "prop-types";
import React, { Component } from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";
import { COLOR_PRIMARY } from "../../../config/common";

class PlanButton extends Component {
  static propTypes = {
    onPress: PropTypes.func,
    activeColor: PropTypes.string,
    color: PropTypes.string,
    isActive: PropTypes.bool
  };

  static defaultProps = {
    isActive: false,
    color: "white",
    activeColor: COLOR_PRIMARY
  };

  getColor() {
    return this.props.isActive ? this.props.activeColor : this.props.color;
  }

  getBackgroundColor() {
    return !this.props.isActive ? this.props.activeColor : this.props.color;
  }

  render() {
    return (
      <View
        activeOpacity={0.8}
        style={[
          styles.wrapper,
          {
            borderColor: this.getColor(),
            backgroundColor: this.getBackgroundColor()
          },
          this.props.style
        ]}
        onPress={this.props.onPress}
      >
        <Text
          style={[
            styles.text,
            {
              color: this.getColor()
            }
          ]}
        >
          {this.props.name}
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
    flexDirection: "row",
    padding: 1,
    borderWidth: 1,
    alignContent: "center",
    width: 60,
    borderRadius: 5,
    justifyContent: "center",
  },
  text: {
    fontSize: 11,
    fontFamily: "Shikumvit",
    alignSelf: "stretch",
    justifyContent: "center"
  }
});

export default PlanButton;
