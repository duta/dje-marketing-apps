import React, { Component } from "react";
import { FlatList, Image, View, Text, RefreshControl } from "react-native";
import { connect } from "react-redux";
import ListItem from "./ListItem";
import { COLOR_VIOLET_GREY } from "../../../config/common";
import { values } from "lodash";
import { fetchPlans,resetPlan} from "../../Plan/plan.action";
class PlanList extends Component {
  state = {
    currentPage:1
  }

  constructor(props) {
    super(props);

    this.requestData = this.requestData.bind(this);
    this.handleLoadMore = this.handleLoadMore.bind(this);
    this.refreshData = this.refreshData.bind(this);
  }

  componentDidMount() {
    this.props.dispatch(resetPlan());
    this.refreshData();
  }

  handleLoadMore() {
    if (this.props.lastPage !== this.state.currentPage) {
      this.setState({
        currentPage: parseInt(this.state.currentPage) + 1,
      },() => {this.requestData()});
    }
  };

  requestData() {
    const params = {
      page : this.state.currentPage,
      marketing : this.props.marketingId,
    }

    this.props.dispatch(fetchPlans(params));
  }

  refreshData(){
    this.setState({
      currentPage: 1,
    },() => {this.requestData()});
  }

  _renderList() {
    const plans = Object.values(this.props.plans) || [];
    const listItem = listItem => {
      const plan = listItem.item;
      const index = listItem.index;
      return (
        <ListItem
          title={plan.title}
          desc={plan.description}
          first_date={plan.first_date}
          second_date={plan.second_date}
          key={`plan-list-${plan.id}`}
          index={index}
        />
      );
    };

    return (
      <FlatList
        data={plans}
        renderItem={listItem}
        style={{
          alignSelf: "stretch"
        }}
        removeClippedSubviews={false}
        keyExtractor={item => `key-${item.id}`}
        onEndReached={this.handleLoadMore}
        onEndReachedThreshold={0.3}
        refreshControl={
          <RefreshControl
            refreshing={this.props.isFetching}
            onRefresh={this.refreshData}
          />
        }
      />
    );
  }

  render() {
    return (
      <View style={{ flex: 1, alignItems: "flex-start" }}>
          {this._renderList()}
      </View>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    plans: state.plan.list || {},
    lastPage:state.plan.lastPage,
    isFetching: state.plan.isFetching,
  };
};

export default connect(mapStateToProps)(PlanList);
