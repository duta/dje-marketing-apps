import * as actionTypes from "./marketing.actionTypes";
import { RESET_STATE } from "../../config/reducer.action";
import { isArray, mapKeys, orderBy } from "lodash";

const iniialState = {
  isFetching: false,
  isError: false,
  isUpdating: false,
  errors: null,
  list: [],
  lastPage: 1
};

const updateList = (list, data) => {
  var newList = list;
  newList[data.id] = data;

  return newList;
};

const reducer = (state = iniialState, action) => {
  const type = action.type;

  switch (type) {
    case actionTypes.FETCH_MARKETINGS:
    case actionTypes.FETCH_CHART:
      return {
        ...state,
        isFetching: true,
        isError: false
      };
    case actionTypes.MARKETINGS_FETCHED:
      return {
        ...state,
        isFetching: false,
        isError: false,
        list: mapKeys(action.data.data, "id"),
        errors: null,
        lastPage: action.data.last_page
      };
    case actionTypes.CHART_FETCHED:
      return {
        ...state,
        isFetching: false,
        isError: false,
        chart: action.data.data,
        errors: null,
      };
    case actionTypes.UPDATE_MARKETING:
    case actionTypes.ADD_MARKETING:
      return {
        ...state,
        isUpdating: true,
        isError: false
      };
    case actionTypes.MARKETING_ADDED:
    case actionTypes.MARKETING_UPDATED:
      return {
        ...state,
        isUpdating: false,
        isError: false,
        list: {
          ...state.list,
          ...updateList(state.list, action.data)
        }
      };
    case actionTypes.UPDATE_MARKETING_FAIL:
    case actionTypes.FETCH_MARKETING_FAIL:
    case actionTypes.ADD_MARKETING_FAIL:
    case actionTypes.FETCH_CHART_FAIL:
      return {
        ...state,
        isFetching: false,
        isUpdating: false,
        isError: true,
        errors: action.errors
      };
    case actionTypes.MARKETING_CLEAR_ERROR:
      return {
        ...state,
        isError: false,
        errors: null
      };
    case RESET_STATE:
    case actionTypes.RESET_MARKETING:
      return iniialState;
    default:
      return state;
  }
};

export default reducer;
