import React, { Component } from "react";
import { Image, View, Text, StyleSheet } from "react-native";
import { connect } from "react-redux";
import Modal from "react-native-modal";
import SnackBar from "react-native-snackbar";
import { NavigationActions, StackActions } from "react-navigation";
import Container from "../../../components/Container";
import MarketingForm from "./MarketingForm";
import { addMarketing, updateMarketing, clearError } from "../marketing.action";
import { Loading, FullScreenLoading } from "../../../components/Loading";
import { COLOR_SOFTEN_RED, COLOR_VIOLET_GREY } from "../../../config/common";

class MarketingInput extends Component {
  timeout = null;
  state = {
    success: false
  };

  static navigationOptions = ({ navigation }) => {
    return {
      headerRight: <View />
    };
  };

  onFormSubmit = data => {
    if (this.props.marketing === null) {
      this.props.dispatch(addMarketing(data));
    } else {
      this.props.dispatch(updateMarketing(this.props.marketing.id, data));
    }
  };

  componentWillMount() {
    this.props.dispatch(clearError());
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.isUpdating && !nextProps.isUpdating) {
      if (!nextProps.isError) {
        this.setState({ success: true });
      } else {
        setTimeout(() => {
          SnackBar.show({
            title: "Tidak dapat menyimpan data.",
            duration: SnackBar.LENGTH_LONG,
            backgroundColor: COLOR_SOFTEN_RED
          });
        }, 500);
      }
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (!prevState.success && this.state.success) {
      this.timeout = setTimeout(this.dismissSuccessMessage, 1000);
    }
  }

  dismissSuccessMessage = () => {
    //this.setState({ success: false });
    this.redirectToList();

    if (this.timeout) {
      clearTimeout(this.timeout);
    }
  };

  renderSuccessMessage = () => {
    var text = "Penambahan marketing telah berhasil";
    if (this.props.navigation.state.params.id) {
      text = "Data berhasil diperbarui";
    }

    return (
      <Modal
        isVisible={this.state.success}
        onBackButtonPress={this.dismissSuccessMessage}
        onBackdropPress={this.dismissSuccessMessage}
      >
        <View style={styles.messageWrapper}>
          <Image source={require("../../../assets/images/updated.png")} />
          <Text style={styles.message}>{text}</Text>
        </View>
      </Modal>
    );
  };

  redirectToList = () => {
    const { dispatch } = this.props;
    dispatch(NavigationActions.back());
  };

  render() {
    return (
      <Container style={{ padding: 0 }}>
        <MarketingForm
          errors={this.props.errors}
          onSubmit={this.onFormSubmit}
          marketing={this.props.marketing}
          isLoading={this.props.isUpdating}
          regency={this.props.regency}
        />
        <FullScreenLoading 
          isVisible={this.props.isUpdating}
          text="Memproses data ..."
        />
        {this.renderSuccessMessage()}
      </Container>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  const marketingId = ownProps.navigation.state.params.id;
  const marketing = marketingId ? ownProps.navigation.state.params.data : null;
  const regencyId = marketingId && marketing.regency_id ? marketing.regency_id : null;
  return {
    marketing: marketingId ? marketing : null,
    isUpdating: state.marketing.isUpdating,
    isError: state.marketing.isError,
    regency: regencyId ? state.regencies.list[regencyId] : null,
    errors: state.marketing.errors || {}
  };
};

const styles = StyleSheet.create({
  messageWrapper: {
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center",
    padding: 16,
    borderRadius: 3
  },
  message: {
    fontFamily: "Quicksand",
    color: COLOR_VIOLET_GREY,
    textAlign: "center",
    marginVertical: 16
  }
});

export default connect(mapStateToProps)(MarketingInput);
