import React, { Component } from "react";
import {
  Image,
  Keyboard,
  KeyboardAvoidingView,
  ScrollView,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
  Platform,
  Picker
} from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import { TextInputWithLabel } from "../../../components/TextInput";
import { Select } from "../../../components/Select";
import formStyles from "../../../styles/form";
import styles from "./styles";
import { BUTTON_GRADIENT, COLOR_GREY } from "../../../config/common";
import _ from "lodash";
import UploadImage from "../../../components/UploadImage";
import Snackbar from "react-native-snackbar";
import PropTypes from "prop-types";
import { clean } from "../../../utils/array";
import {
  pad,
  formatPhone,
  clearPhoneFormat,
  toTitleCase,
  filterPhoneNumber
} from "../../../utils/string";
import { NavigationActions } from "react-navigation";
import { connect } from "react-redux";
import { Button } from "../../../components/Button";

let initialState = {
  fields: {
    name: null,
    regency_id: null,
    phone: null,
    address: null,
    gender:null,
    email:null,
    photo:null
  }
};

class MarketingForm extends Component {
  state = {
    fields: initialState.fields,
    showImagePicker: false,
    selectedRegency:null
  };

  static propTypes = {
    onImagePickerSelected: PropTypes.func,
    onImagePickerError: PropTypes.func
  };

  static defaultProps = {
    errors: []
  };

  initializeData = (fields) => {

    if (fields.gender === "Laki-laki" || fields.gender === "L") {
      fields["gender_index"] = 0;
    } else if (fields.gender === "Perempuan" || fields.gender === "P") {
      fields["gender_index"] = 1;
    } else {
      fields["gender_index"] = null;
    }
    
    return fields;
  };

  componentDidMount() {

    var fields = {
      ... this.state.fields,
      ... this.props.marketing,
    };

    fields = this.initializeData(fields);

    this.setState({
      ...this.state,
      selectedRegency : this.props.regency,
      fields
    });
  }

  resetFields = () => {
    const fields = {
      name: null,
      regency_id: null,
      phone: null,
      address: null,
      gender:null,
      email:null,
      photo:null
    };

    initialState.fields = fields;
    this.setState({ fields });
  };

  componentWillUnmount() {
    this.resetFields();
  }

  onSubmit() {
    if (typeof this.props.onSubmit === "function") {
      const data = {
        ...this.state.fields,
        no_telp: clearPhoneFormat(this.state.fields.no_telp)
      };
      this.props.onSubmit(clean(data));
    }
  }

  updateTextField = field => value => {
    let newFields = { ...this.state.fields };
    newFields[field] = value;
    this.setState({
      ...this.state,
      fields: { ...newFields }
    });
  };

  updatePickerField = field => (value, index) => {
    let newFields = { ...this.state.fields };
    newFields[field] = value.value;
    newFields[field + "_index"] = index;

    this.setState({
      ...this.state,
      fields: { ...newFields }
    });
  };

  renderErrorMessage = field => {
    const { errors } = this.props;

    // No error
    if (typeof errors === "undefined" || typeof errors[field] === "undefined") {
      return null;
    } else {
      // Error found
      return <Text style={styles.errorMessage}> {errors[field][0]} </Text>;
    }
  };

  showImagePicker = () => {
    this.setState({
      showImagePicker: true
    });
  };

  hideImagePicker = () => {
    this.setState({
      showImagePicker: false
    });
  };

  onImagePickerSelected = response => {
    // Update state
    this.setState({
      showImagePicker: false,
      fields: {
        ...this.state.fields,
        photo: {
          uri: response.path,
          name: "profile-pict.jpeg",
          type: response.mime
        }
      }
    });

    // Jalankan callback dari parent
    if (typeof this.props.onImagePickerSelected === "function") {
      this.props.onImagePickerSelected(response);
    }
  };

  onImagePickerError = response => {
    // Tutup image picker
    this.setState({ showImagePicker: false });

    // Tampilkan error
    Snackbar.show({
      title: "ImagePicker error: " + response.error,
      duration: Snackbar.LENGTH_SHORT
    });

    // Jalankan callback tambahan dari parent
    if (typeof this.props.onImagePickerError === "function") {
      this.props.onImagePickerError(response);
    }
  };

  getSubmitButtonLabel = () => {
    if (this.props.isLoading) {
      return "Menyimpan data ...";
    } else {
      return "Simpan";
    }
  };

  selectRegeny = () => {
    const { dispatch } = this.props;

    dispatch(
      NavigationActions.navigate({
        routeName: "SelectRegency",
        params: {
          type:"single",
          callback: this.onRegencySelected
        }
      })
    );
  };

  onRegencySelected = regency => {
    this.setState({
      selectedRegency: regency,
      fields: {
        ...this.state.fields,
        regency_id: regency.id
      }
    });
  };

  render() {
    const { fields } = this.state;
    const props = this.props;
    let imageSource = null;
    if(fields.photo !== undefined){
      if(props.marketing != null){
        if(fields.photo === props.marketing.photo){
          imageSource = { uri: props.marketing.url_photo };
        }else{
          imageSource = fields.photo;
        }
        
      }else{
        imageSource = fields.photo;
      }
    }
    
    return (
      <KeyboardAvoidingView behavior="padding" style={{flex: 1, alignSelf: "stretch"}}>
        <ScrollView style={styles.form} keyboardShouldPersistTaps="always">
          <View
            style={[
              styles.section,
              { alignItems: "center", justifyContent: "center", marginBottom: 24 }
            ]}
          >
            <View style={styles.profilePictWrapper}>
              <TouchableWithoutFeedback
                onPress={this.showImagePicker}
              >
                {fields.photo === undefined || fields.photo === null ? (
                  <Icon
                    name={(Platform.OS === "ios" ? "ios" : "md") + "-camera"}
                    color={COLOR_GREY}
                    size={32}
                  />
                ) : (
                  <Image style={styles.profilePict} source={imageSource} />
                )}
              </TouchableWithoutFeedback>
            </View>
            <Text style={{ color: "#CCC", fontSize: 13 }}>
              Tap untuk mengganti foto
            </Text>
          </View>
          <View style={styles.section}>
            <TextInputWithLabel
              label="Nama"
              style={formStyles.inputWrapper}
              inputStyle={formStyles.input}
              labelStyle={formStyles.inputLabel}
              onChangeText={this.updateTextField("name")}
              value={fields.name}
              autoCapitalize="words"
            />
            {this.renderErrorMessage("name")}
          </View>
          {!props.marketing && <View style={styles.section}>
            <TextInputWithLabel
              label="Email"
              style={formStyles.inputWrapper}
              inputStyle={formStyles.input}
              labelStyle={formStyles.inputLabel}
              onChangeText={this.updateTextField("email")}
              value={fields.email}
              autoCapitalize="none"
              keyboardType="email-address"
            />
            {this.renderErrorMessage("email")}
          </View>}
          <View style={styles.section}>
            <Select
              label="Jenis Kelamin"
              title="Pilih Jenis Kelamin"
              style={[formStyles.selectWrapper]}
              inputStyle={formStyles.select}
              labelStyle={formStyles.inputLabel}
              selectedIndex={fields.gender_index}
              onItemSelected={this.updatePickerField("gender")}
              items={[
                { label: "Laki-laki", value: "L" },
                { label: "Perempuan", value: "P" }
              ]}
            />
            {this.renderErrorMessage("gender")}
          </View>
          <View style={styles.section}>
            <TextInputWithLabel
              label="Nomor Telepon/Hp"
              style={formStyles.inputWrapper}
              inputStyle={formStyles.input}
              labelStyle={formStyles.inputLabel}
              keyboardType="numeric"
              onChangeText={val => this.updateTextField("phone")(filterPhoneNumber(val))}
              value={formatPhone(fields.phone)}
              maxLength={15}
            />
            {this.renderErrorMessage("phone")}
          </View>
          <View style={styles.section}>
            <TextInputWithLabel
              label="Kota"
              placeholder={"Pilih kota"}
              style={formStyles.inputWrapper}
              inputStyle={[formStyles.input, { textAlignVertical: "center" }]}
              labelStyle={formStyles.inputLabel}
              editable={false}
              value={
                this.state.selectedRegency !== null
                ? this.state.selectedRegency.name
                : null}
              onTouchStart={this.selectRegeny}
            />
            {this.renderErrorMessage("regency_id")}
          </View>
          <View style={styles.section}>
            <TextInputWithLabel
              label="Alamat"
              style={formStyles.inputWrapper}
              inputStyle={[formStyles.input, { textAlignVertical: "top" }]}
              labelStyle={formStyles.inputLabel}
              multiline={true}
              onChangeText={this.updateTextField("address")}
              value={fields.address}
              autoCapitalize="words"
            />
            {this.renderErrorMessage("address")}
          </View>
          <Button
            label={this.getSubmitButtonLabel()}
            style={formStyles.button}
            gradient={BUTTON_GRADIENT}
            gradientDirection="horizontal"
            disabled={this.props.isDisabled || this.props.isLoading}
            onPress={() => this.onSubmit()}
            containerStyle={{ elevation: 3 }}
          />
          <UploadImage
            isVisible={this.state.showImagePicker}
            onClose={this.hideImagePicker}
            onImageSelected={this.onImagePickerSelected}
            onError={this.onImagePickerError}
          /> 
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}

export default connect()(MarketingForm);
