import React, { Component } from "react";
import { Image, View, Text, Animated, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import { NavigationActions, HeaderProps } from "react-navigation";
import styles from "./styles";
import {
  HEADER_GRADIENT,
  COLOR_PRIMARY,
  COLOR_GREY
} from "../../../config/common";
import { TabViewAnimated, TabView } from "react-native-tab-view";
import CustomComponent from "../../../components/CustomComponent";
import Header from "./Header";
import ProjectHistory from "../ProjectHistory";
import PlanHistory from "../PlanHistory";
import ActivityHistory from "../ActivityHistory";


class MarketingDetail extends CustomComponent {
  
  static navigationOptions = ({ navigation }) => {
    const header = navigation.state.params.header || HeaderProps;

    return {header};
  };

  constructor(props) {
    super(props);
    this.state = {
      tabs: {
        index: 1,
        routes: [
          { key: "project", title: "Project" },
          { key: "plan", title: "Rencana" },
          { key: "activity", title: "Aktivitas" }
        ]
      },
      keyboardShow: false,
      loading: false
    };
  }

  componentDidMount() {
    this.props.navigation.setParams({
      action: this.navigateToEdit
    });
    this.setHeader();
  }

  navigateToEdit = () => {
    this.props.dispatch(
      NavigationActions.navigate({
        routeName: "MarketingInput",
        params: {
          title: "Edit Marketing",
          id: this.props.marketing.id
        }
      })
    );
  };

  setHeader(){
    const { navigation } = this.props;
    const marketing = this.props.marketing || {};
    let imageSource = null;
    let regency_name = null;
    if (null !== marketing.photo){
      imageSource = { uri: marketing.url_photo };
    }

    if (marketing.regency) {
      regency_name = marketing.regency.name;
    }

    navigation.setParams({header: (
      <Header
        name={marketing.name}
        address={marketing.address}
        city={regency_name}
        gender={marketing.gender}
        photo={imageSource}
        phone={marketing.phone}
        onBackPressed={() => navigation.dispatch(NavigationActions.back())}
        onUpdatePressed={() => this.navigateToEdit()}
      />
    )
    });
  }

  renderTab(){
    return (
      <TabViewAnimated
        renderHeader={this.renderTabHeader}
        navigationState={this.state.tabs}
        renderScene={this.renderScene}
        onIndexChange={this.handleTabIndexChange}
      />
    );
  };

  renderTabHeader = props => {
    const inputRange = props.navigationState.routes.map((x, i) => i);
    return (
      <View style={styles.tabBar}>
        {props.navigationState.routes.map((route, i) => {
          const color = props.position.interpolate({
            inputRange,
            outputRange: inputRange.map(
              inputIndex => (props.navigationState.index === i ? COLOR_PRIMARY : "#999")
            )
          });
          
          const borderBottomColor =
            props.navigationState.index === i ? COLOR_PRIMARY : "#CCC";
          const borderBottomWidth = props.navigationState.index === i ? 3 : 1;
          
          return (
            <TouchableOpacity
              activeOpacity={0.8}
              style={[
                styles.tabItem,
                { borderBottomColor, borderBottomWidth: borderBottomWidth }
              ]}
              onPress={() => this.handleTabIndexChange(i)}
              key={i}
            >
              <Animated.Text style={[styles.tabItemLabel, { color }]}>
                {route.title}
              </Animated.Text>
            </TouchableOpacity>
          );
        })}
      </View>
    );
  };

  renderScene = ({ route }) => {
    var id = this.props.marketing.id;
    switch (route.key | this.state.tabs.index) {
      case "project" | 0:
        return (<ProjectHistory marketingId={id} />);
      case "plan" | 1:
        return (<PlanHistory marketingId={id} />);
      case "activity" | 2:
        return (<ActivityHistory marketingId={id} />);
    }
  };

  handleTabIndexChange = index => {
    this.setState({
      tabs: {
        ...this.state.tabs,
        index
      }
    });
  }

  render() {
    return this.renderTab()
  }
}

const mapStateToProps = (state, ownProps) => {
  const marketingId = ownProps.navigation.state.params.id;
  return {
    marketing: state.marketing.list[marketingId]
  };
};

export default connect(mapStateToProps)(MarketingDetail);
