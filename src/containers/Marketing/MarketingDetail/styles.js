import EStyleSheet from "react-native-extended-stylesheet";
import { Dimensions } from "react-native";
import { COLOR_PRIMARY, COLOR_ORANGE } from "../../../config/common";

const styles = EStyleSheet.create({
  scrollView: {
    flex: 1
  },
  container: {
    alignContent: "flex-start",
    justifyContent: "flex-start"
  },
  header: {
    width: Dimensions.get("window").width,
    padding: 16,
    flexDirection: "row",
    borderTopWidth: 0.3,
    borderTopColor: COLOR_ORANGE
  },
  imageThumbnail: {
    height: 110,
    width: 110,
    resizeMode: "cover",
    marginRight: 24,
    borderRadius: 55,
  },
  headerDetail: {
    paddingTop: 4,
    flex: 1
  },
  headerText: {
    color: "white"
  },
  headerDoctorName: {
    fontSize: 16,
    fontWeight: "600",
    fontFamily: "Arial"
  },
  headerAddrWrap: {
    flexDirection: "row"
  },
  headerDoctorDesc: {
    fontSize: 12,
    flex: 1,
    flexWrap: "wrap",
    fontFamily: "Quicksand"
  },
  headerSpWrap: {
    marginVertical: 12,
    flex: 1
  },
  headerDoctorSp: {
    fontFamily: "Quicksand"
  },
  tabBar: {
    flexDirection: "row"
  },
  tabItem: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "white",
    borderColor: "#CCC",
    paddingVertical: 8
  },
  tabItemLabel: {
    textAlign: "center",
    fontSize: 12,
    marginHorizontal: 0,
    marginVertical: 4,
    fontFamily: "Arial",
    fontWeight: "500"
  },
  row: {
    flexDirection: "row",
    alignItems: "center"
  },
  distance: {
    fontSize: 12,
    color: "white",
    fontFamily: "Quicksand"
  }
});

export default styles;
