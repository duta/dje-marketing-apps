import * as actionTypes from "./marketing.actionTypes";

export function resetMarketing(){
  return {
    type:actionTypes.RESET_MARKETING
  }
}

export function fetchMarketings(params) {
  return {
    type: actionTypes.FETCH_MARKETINGS,
    params
  };
}

export function addMarketing(data) {
  return {
    type: actionTypes.ADD_MARKETING,
    data
  };
}

export function updateMarketing(id, data) {
  return {
    type: actionTypes.UPDATE_MARKETING,
    id,
    data
  };
}

export function clearError() {
  return {
    type: actionTypes.MARKETING_CLEAR_ERROR
  };
}

export function fetchChart(){
  return {
    type:actionTypes.FETCH_CHART
  }
}
