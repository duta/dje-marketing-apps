import MarketingList from "./MarketingList";
import MarketingDetail from "./MarketingDetail";
import MarketingInput from "./MarketingInput";
import reducer from "./marketing.reducer";
import saga from "./marketing.saga";

export { MarketingList, MarketingDetail, MarketingInput, reducer, saga };
