import React, { Component } from "react";
import { FlatList, Image, View, Text, RefreshControl } from "react-native";
import { connect } from "react-redux";
import ListItem from "./ListItem";
import { COLOR_VIOLET_GREY } from "../../../config/common";
import { fetchActivities,resetActivity} from "../../Activity/activity.action";
class ActivityList extends Component {
  state = {
    currentPage:1
  }

  constructor(props) {
    super(props);

    this.requestData = this.requestData.bind(this);
    this.handleLoadMore = this.handleLoadMore.bind(this);
    this.refreshData = this.refreshData.bind(this);
  }

  componentDidMount() {
    this.props.dispatch(resetActivity());
    this.refreshData();
  }

  handleLoadMore() {
    if (this.props.lastPage !== this.state.currentPage) {
      this.setState({
        currentPage: parseInt(this.state.currentPage) + 1,
      },() => {this.requestData()});
    }
  };

  requestData() {
    const params = {
      page : this.state.currentPage,
      marketing : this.props.marketingId,
    }

    this.props.dispatch(fetchActivities(params));
  }

  refreshData(){
    this.setState({
      currentPage: 1,
    },() => {this.requestData()});
  }

  _renderList() {
    const activities = Object.values(this.props.activities) || [];
    const listItem = listItem => {
      const activity = listItem.item;
      const index = listItem.index;
      return (
        <ListItem
          title={activity.title}
          desc={activity.description}
          date={activity.first_date}
          status={activity.type}
          key={`activity-list-${activity.id}`}
          index={index}
        />
      );
    };

    return (
      <FlatList
        data={activities}
        renderItem={listItem}
        style={{
          alignSelf: "stretch"
        }}
        removeClippedSubviews={false}
        keyExtractor={item => `key-${item.id}`}
        onEndReached={this.handleLoadMore}
        onEndReachedThreshold={0.3}
        refreshControl={
          <RefreshControl
            refreshing={this.props.isFetching}
            onRefresh={this.refreshData}
          />
        }
      />
    );
  }

  render() {
    return (
      <View style={{ flex: 1, alignItems: "flex-start" }}>
          {this._renderList()}
      </View>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    activities: state.activity.list || {},
    lastPage:state.activity.lastPage,
    isFetching: state.activity.isFetching,
  };
};

export default connect(mapStateToProps)(ActivityList);
