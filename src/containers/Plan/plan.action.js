import * as actionTypes from "./plan.actionTypes";

export function resetPlan(){
  return {
    type:actionTypes.RESET_PLAN
  }
}

export function fetchPlans(params) {
  return {
    type: actionTypes.FETCH_PLANS,
    params
  };
}

export function addPlan(data) {
  return {
    type: actionTypes.ADD_PLAN,
    data
  };
}

export function updatePlan(id, data) {
  return {
    type: actionTypes.UPDATE_PLAN,
    id,
    data
  };
}

export function deletePlan(id) {
  return {
    type: actionTypes.DELETE_PLAN,
    id
  };
}

export function clearError() {
  return {
    type: actionTypes.PLAN_CLEAR_ERROR
  };
}
