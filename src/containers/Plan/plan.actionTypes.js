export const FETCH_PLANS = "FETCH_PLANS";
export const PLANS_FETCHED = "PLANS_FETCHED";
export const FETCH_PLANS_FAIL = "FETCH_PLANS_FAIL";

export const ADD_PLAN = "ADD_PLAN";
export const PLAN_ADDED = "PLAN_ADDED";
export const ADD_PLAN_FAIL = "ADD_PLAN_FAIL";

export const UPDATE_PLAN = "UPDATE_PLAN";
export const PLAN_UPDATED = "PLAN_UPDATED";
export const UPDATE_PLAN_FAIL = "UPDATE_PLAN_FAIL";

export const DELETE_PLAN = "DELETE_PLAN";
export const PLAN_DELETED = "PLAN_DELETED";
export const DELETE_PLAN_FAIL = "DELETE_PLAN_FAIL";

export const RESET_PLAN = "RESET_PLAN";
export const PLAN_CLEAR_ERROR = "PLAN_CLEAR_ERROR";
