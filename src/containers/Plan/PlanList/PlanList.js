import React, { Component } from "react";
import { BackHandler, FlatList, View, Text, SafeAreaView, KeyboardAvoidingView,TouchableOpacity, RefreshControl } from "react-native";
import { NavigationActions,HeaderProps } from "react-navigation";
import { connect } from "react-redux";
import containerStyle from "../../../styles/container";
import ActionButton from "../../../components/ActionButton";
import ListItem from "./ListItem";
import styles from "./styles";
import {
  BUTTON_GRADIENT,
  COLOR_PRIMARY,
  COLOR_SOFTEN_RED
} from "../../../config/common";
import { isEq } from "../../../utils/array";
import {
  fetchPlans,
  resetPlan,
  deletePlan
} from "../plan.action";
import { FullPageLoading } from "../../../components/Loading";
import CustomComponent from "../../../components/CustomComponent";
import SearchBar from "../../../components/SearchBar";
import Icon from "react-native-vector-icons/MaterialIcons";
import FilterForm from "./FilterForm";
import * as ActionProvince from "../../SelectProvince/select_province.action";
import DatePicker from "react-native-modal-datetime-picker";
import { pad } from "../../../utils/string";
import _ from "lodash";
import SnackBar from "react-native-snackbar";
import inflateError from "../../../utils/inflateError";
import ModalDialog from "../../../components/Modal";
import { Button, GradientBorderButton } from "../../../components/Button";

class PlanList extends CustomComponent {
  state = {
    searching: false,
    searchText: null,
    loading: false,
    fetched: false,
    showFilterForm: false,
    selectedProvince: [],
    selectedMarketing:null,
    showDatePickerFirst:false,
    showDatePickerEnd:false,
    firstDate:null,
    endDate:null,
    currentPage:1,
    showDeleteDialog:false,
    deletePlan:null
  };

  constructor(props) {
    super(props);

    this.requestData = this.requestData.bind(this);
    this.handleLoadMore = this.handleLoadMore.bind(this);
    this.refreshData = this.refreshData.bind(this);
  }

  static navigationOptions = ({ navigation }) => ({
    header: navigation.state.params.searchBar || HeaderProps,
    headerRight: (
      <View style={{ paddingHorizontal: 16 }}>
        {navigation.state.params.right !== undefined
          ? navigation.state.params.right
          : null}
      </View>
    )
  });

  
  componentWillUnmount() {
    super.componentWillUnmount();
    this.cancelSearch();
  }

  componentWillMount() {
    this.setState({loading: true});
  }

  componentDidFocus() {
    this.setState({loading: false});
    this._showSearchButton();

    if (!this.state.fetched) {
      this.refreshData();
      this.setState({fetched: true});
    }
  }

  /**
   * Show search button
   *
   */
  _showSearchButton = () => {
    const { searching } = this.state;
    if (!searching) {
      let add_button = <ActionButton onPress={this.onAddButtonPress} icon="add" />;
      let margin = 16;
      this.props.navigation.setParams({
        right: (
          <View style={{ flexDirection: "row" }}>
            <ActionButton
              onPress={this._showSearchForm}
              icon="search"
              style={{ marginRight: margin }}
            />
            {add_button}
          </View>
        )
      });
    }
  };

  /**
   * Do search
   *
   */
  doSearch = text => {
    this.setState({ searchText: text });
    this.refreshData();
  };

  /**
   * Cancel search
   *
   */
  cancelSearch = () => {
    this.props.navigation.setParams({ searchBar: null });
    this.setState({ searching: false, searchText: null });
    this.refreshData();
  };

  /**
   * Show search form on header
   *
   */
  _showSearchForm = () => {
    this.setState({ searching: true });
    this.props.navigation.setParams({
      searchBar: (
        <SafeAreaView style={{backgroundColor: COLOR_PRIMARY}}>
          <SearchBar
            backgroundColor={COLOR_PRIMARY}
            placeholder="Ketikan judul plan"
            onCancel={this.cancelSearch}
            onChangeText={this.doSearch}
            text={this.state.searchText}
          />
        </SafeAreaView>
      )
    });
  };

  requestData() {
    const provinces = this.state.selectedProvince.map(function(item){
      return item.id;
    });

    const params = {
      page : this.state.currentPage,
      search : this.state.searchText,
      province : provinces,
      marketing : this.state.selectedMarketing ? this.state.selectedMarketing.id : null,
      first_date : this.state.firstDate,
      second_date : this.state.endDate,
    }
    this.props.dispatch(fetchPlans(params));
  }

  refreshData(){
    this.props.dispatch(resetPlan());
    this.setState({
      currentPage: 1,
    },() => {this.requestData()});
  }

  showPlanDetail = plan => {
    this.props.dispatch(
      NavigationActions.navigate({ 
        routeName: "PlanDetail", 
        params: { 
          data:plan
        }
      })
    );
  };

  onEditButtonPress = plan => {
    this.props.dispatch(
      NavigationActions.navigate({
        routeName: "PlanInput",
        params: {
          title: "Edit Plan",
          id: plan.id,
          data: plan
        }
      })
    );
  };

  deletePlan = () => {
    const plan = this.state.deletePlan;
    this.props.dispatch(deletePlan(plan.id));
    this.hideDeleteDialog();
  };

  onItemPress = plan => {
    this.showPlanDetail(plan);
  };

  onActivityButtonPress = (plan) => {
    this.props.dispatch(
      NavigationActions.navigate({
        routeName: "ActivityInput",
        params: {
          title: "Tambah Activity",
          plan:plan
        }
      })
    );
  };

  componentWillReceiveProps(nextProps) {
    if (!this.props.isError && nextProps.isError) {
      setTimeout(() => {
        SnackBar.show({
          title: inflateError(nextProps.errors),
          duration: SnackBar.LENGTH_LONG,
          backgroundColor: COLOR_SOFTEN_RED
        });
      }, 500);
    }
  }

  handleLoadMore() {
    if (this.props.lastPage !== this.state.currentPage) {
      this.setState({
        currentPage: parseInt(this.state.currentPage) + 1,
      },() => {this.requestData()});
    }
  };

  onDeleteButtonPress = plan => {
    this.setState({ deletePlan: plan });
    this.showDeleteDialog();
  };

  renderDeleteDialog = () => {
    return (
      <ModalDialog
        isVisible={this.state.showDeleteDialog}
        title="Apakah Anda yakin akan menghapus plan ?"
        onClose={this.hideDeleteDialog}
      >
        <View style={styles.deleteModalWrapper}>
          <GradientBorderButton
            borderGradient={BUTTON_GRADIENT}
            label="Batal"
            backgroundColor="white"
            textColor={COLOR_PRIMARY}
            style={{ alignSelf: "stretch", height: 34 }}
            containerStyle={[
              styles.deleteModalButton,
              { borderRadius: 3, marginRight: 8 }
            ]}
            borderWidth={2}
            onPress={this.hideDeleteDialog}
          />

          <Button
            gradient={BUTTON_GRADIENT}
            label="OK"
            style={styles.deleteModalButton}
            containerStyle={[styles.deleteModalButton, { marginLeft: 8 }]}
            onPress={this.deletePlan}
            gradientDirection="horizontal"
          />
        </View>
      </ModalDialog>
    );
  };

  showDeleteDialog = () => {
    this.setState({ showDeleteDialog: true });
  };

  hideDeleteDialog = () => {
    this.setState({ showDeleteDialog: false });
  };

  renderList() {
    const plans = Object.values(this.props.plans) || [];
    const listItem = listItem => {
      const plan = listItem.item;

      return (
        <ListItem
          title={plan.title}
          description={plan.description}
          first_date={plan.first_date}
          second_date={plan.second_date}
          status={plan.status}
          marketing={plan.marketing.name}
          onPress={() => this.onItemPress(plan)}
          editAction={() => this.onEditButtonPress(plan)}
          deleteAction={() => this.onDeleteButtonPress(plan)}
          aktivityAction={() => this.onActivityButtonPress(plan)}
          key={`plan-list-${plan.id}`}
          swipeDisable={this.state.selecting}
        />
      );
    };

    return (
      <FlatList
        data={plans}
        renderItem={listItem}
        style={styles.list}
        extraData={this.state.currentPage}
        keyExtractor={item => `key-${item.id}`}
        removeClippedSubviews={false}
        onEndReached={this.handleLoadMore}
        onEndReachedThreshold={0.3}
        refreshControl={
          <RefreshControl
            refreshing={this.props.isFetching}
            onRefresh={this.refreshData}
          />
        }
      />
    );
  }

  onAddButtonPress = () => {
    this.props.dispatch(
      NavigationActions.navigate({
        routeName: "PlanInput",
        params: {
          title: "Tambah Plan"
        }
      })
    );
  };

  renderFilterButton = () => {
    return (
      <View style={styles.filterButtonWrapper}>
        <TouchableOpacity
          style={[styles.filterButton, styles.filterButtonBorder]}
          onPress={this.showFilterForm}
        >
          <Icon name="filter-list" size={24} color={COLOR_PRIMARY} />
          <Text style={styles.filterButtonText}>Filter</Text>
        </TouchableOpacity>
        {/* <TouchableOpacity style={styles.filterButton} onPress={this.showMap}>
          <Icon name="map" size={24} color={COLOR_PRIMARY} />
          <Text style={styles.filterButtonText}>Sekitar Saya</Text>
        </TouchableOpacity> */}
      </View>
    );
  };

  showFilterForm = () => {
    this.setState({ showFilterForm: true });
  };

  hideFilterForm = () => {
    this.setState({ showFilterForm: false });
  };
  
  selectProvince = () => {
    this.props.dispatch(
      NavigationActions.navigate({
        routeName: "SelectProvince",
        params: {
          callback: this.onProvinceSelected,
          type: "multy"
        }
      })
    ); 
    this.hideFilterForm();
  };

  onProvinceSelected = province => {
    this.setState({ selectedProvince: province });
    this.showFilterForm();
  };

  selectMarketing = () => {
    this.props.dispatch(
      NavigationActions.navigate({
        routeName: "MarketingSelect",
        params: {
          callback: this.onMarketingSelected,
          type: "single"
        }
      })
    ); 
    this.hideFilterForm();
  };

  onMarketingSelected = marketing => {
    this.setState({ 
      selectedMarketing:marketing
    });
    this.showFilterForm();
  };

  onFilterReset = () => {
    this.setState({
      selectedProvince: [],
      firstDate:null,
      endDate:null,
      selectedMarketing: null,
    });
  };

  onFilter = () => {
    this.refreshData();
    this.hideFilterForm();
  };

  showDatePickerFirst = () => {
    this.setState({
      showDatePickerFirst: true
    });
  };

  hideDatePickerFirst = () => {
    this.setState({
      showDatePickerFirst: false
    });
  };

  onDatePickerConfirmedFirst = date => {
    const selectedDate = `${date.getFullYear()}-${pad(
      date.getMonth() + 1,
      2
    )}-${pad(date.getDate(), 2)}`;

    this.setState({
      firstDate : selectedDate,
      endDate : selectedDate
    });
  };

  showDatePickerEnd = () => {
    this.setState({
      showDatePickerEnd: true
    });
  };

  hideDatePickerEnd = () => {
    this.setState({
      showDatePickerEnd: false
    });
  };

  onDatePickerConfirmedEnd = date => {
    const selectedDate = `${date.getFullYear()}-${pad(
      date.getMonth() + 1,
      2
    )}-${pad(date.getDate(), 2)}`;

    this.setState({
      endDate : selectedDate
    });
  };

  render() {
    return (
      <KeyboardAvoidingView style={containerStyle.container}>
        
        {/* <FullPageLoading
          isVisible={this.props.isFetching}
          text="Mengambil data ..."
        /> */}

        <View style={styles.listWrapper}>
          {this.renderList()}
        </View>
        {this.renderFilterButton()}
        <FilterForm
          isVisible={this.state.showFilterForm}
          onClose={this.hideFilterForm}
          onProvince={this.selectProvince}
          selectedProvince={this.state.selectedProvince}
          onMarketing={this.selectMarketing}
          selectedMarketing={this.state.selectedMarketing}
          showDatePickerFirst= {this.showDatePickerFirst}
          valFirstDate={this.state.firstDate}
          showDatePickerEnd={this.showDatePickerEnd}
          valEndDate={this.state.endDate}
          onFilter={this.onFilter}
          onFilterReset={this.onFilterReset}
          roleUser={this.props.roleUser}
        />
        <DatePicker
          isVisible={this.state.showDatePickerFirst}
          onConfirm={this.onDatePickerConfirmedFirst}
          onCancel={this.hideDatePickerFirst}
          onHideAfterConfirm={this.hideDatePickerFirst}
          // minimumDate={new Date()}
          datePickerModeAndroid="spinner"
          date={this.state.firstDate ? new Date(this.state.firstDate) : new Date()}
        />
        <DatePicker
          isVisible={this.state.showDatePickerEnd}
          onConfirm={this.onDatePickerConfirmedEnd}
          onCancel={this.hideDatePickerEnd}
          onHideAfterConfirm={this.hideDatePickerEnd}
          // minimumDate={new Date()}
          datePickerModeAndroid="spinner"
          date={this.state.endDate ? new Date(this.state.endDate) : new Date()}
        />
        {this.renderDeleteDialog()}
      </KeyboardAvoidingView>
    );
  }
}

const mapStateToProps = state => {
  return {
    plans: state.plan.list,
    isFetching: state.plan.isFetching,
    isError: state.plan.isError,
    errors: state.plan.errors,
    lastPage:state.plan.lastPage,
    roleUser:state.user.profile.data[0].user.user_role
  };
};

export default connect(mapStateToProps)(PlanList);
