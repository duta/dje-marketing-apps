import React, { Component } from "react";
import { ScrollView, Text, View, Alert, TouchableOpacity } from "react-native";
import Modal from "react-native-modal";
import styles from "../../../styles/filter_form";
import {
  COLOR_PRIMARY,
  COLOR_GREY,
  BUTTON_GRADIENT
} from "../../../config/common";
import { TextInput, TextInputWithLabel } from "../../../components/TextInput";
import { Button, GradientBorderButton } from "../../../components/Button";
import formStyles from "../../../styles/form";
import { formatDate } from "../../../utils/date";


const FormSection = props => (
  <View style={styles.formSection}>
    <Text style={styles.subTitle}>{props.subTitle}</Text>
    <View style={styles.formContent}>{props.children}</View>
  </View>
);

class FilterForm extends Component {
  render() {
    var provinceSelected = this.props.selectedProvince;
    var marketing = this.props.selectedMarketing;
    var firstDate = this.props.valFirstDate;
    var endDate = this.props.valEndDate;
    var provinceName = [];
    provinceSelected.forEach(function(item) {
      provinceName.push(item.name);
    });
    return (
      <Modal
        isVisible={this.props.isVisible}
        style={styles.modal}
        onBackdropPress={this.props.onClose}
      >
        <View style={styles.wrapper}>
          <View style={styles.header}>
            <Text style={styles.title}>Filter Pencarian</Text>
            <Text style={styles.closeButton} onPress={this.props.onClose}>
              Tutup
            </Text>
          </View>

          <View style={styles.form}>
            <ScrollView keyboardShouldPersistTaps="always">
              <FormSection subTitle="Range Tanggal">
                <View style={styles.dateRange}>
                  <TextInputWithLabel
                    label="Tanggal Mulai"
                    style={[formStyles.inputWrapper,styles.date, { marginRight: 8 }]}
                    inputStyle={formStyles.input}
                    labelStyle={formStyles.inputLabel}
                    value={
                      firstDate
                        ? formatDate(firstDate, "DD MMMM YYYY")
                        : null
                    }
                    editable={false}
                    onTouchStart={this.props.showDatePickerFirst}
                  />
                  <TextInputWithLabel
                    label="Tanggal Selesai"
                    style={[formStyles.inputWrapper,styles.date, { marginLeft: 8 }]}
                    inputStyle={formStyles.input}
                    labelStyle={formStyles.inputLabel}
                    value={
                      endDate
                        ? formatDate(endDate, "DD MMMM YYYY")
                        : null
                    }
                    editable={false}
                    onTouchStart={this.props.showDatePickerEnd}
                  />
                </View>
              </FormSection>
              <FormSection subTitle="Provinsi dan Marketing">
                <TextInputWithLabel
                  style={[formStyles.inputWrapper,{ flex: 1 }]}
                  inputStyle={formStyles.input}
                  labelStyle={formStyles.inputLabel}
                  placeholder="Cari Provinsi"
                  onTouchStart={() => this.props.onProvince()}
                  editable={false}
                  value={
                    provinceName.toString() ? provinceName.toString() : null
                  }
                />
                {this.props.roleUser == 1 &&
                <TextInputWithLabel
                  style={[formStyles.inputWrapper,{ flex: 1 }]}
                  inputStyle={formStyles.input}
                  labelStyle={formStyles.inputLabel}
                  placeholder="Pilih Marketing"
                  onTouchStart={() => this.props.onMarketing()}
                  editable={false}
                  value={
                    marketing ? marketing.name : null
                  }
                />}
              </FormSection>
            </ScrollView>
          </View>

          <View style={styles.footer}>
            <GradientBorderButton
              label="Reset"
              borderWidth={1}
              containerStyle={[styles.button, { marginRight: 8 }]}
              borderGradient={BUTTON_GRADIENT}
              textColor={COLOR_PRIMARY}
              backgroundColor="white"
              style={{ height: 36 }}
              onPress={() => this.props.onFilterReset()}
            />
            <Button
              gradient={BUTTON_GRADIENT}
              gradientDirection="horizontal"
              label="Terapkan"
              style={styles.button}
              containerStyle={[styles.button, { marginLeft: 8 }]}
              onPress={() => this.props.onFilter()}
            />
          </View>
        </View>
      </Modal>
    );
  }
}

export default FilterForm;
