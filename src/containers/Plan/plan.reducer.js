import * as actionTypes from "./plan.actionTypes";
import { RESET_STATE } from "../../config/reducer.action";
import { isArray, mapKeys, orderBy } from "lodash";

const iniialState = {
  isFetching: false,
  isError: false,
  isUpdating: false,
  errors: null,
  list: [],
  lastPage:1
};

const updateList = (list, data) => {
  var newList = list;
  newList[data.id] = data;

  return newList;
};

const updateFetchedList = (list,data) => {
  var newList = [...list,...data];

  return newList;
};

const reducer = (state = iniialState, action) => {
  const type = action.type;

  switch (type) {
    case actionTypes.FETCH_PLANS:
      return {
        ...state,
        isFetching: true,
        isError: false
      };
    case actionTypes.PLANS_FETCHED:
      return {
        ...state,
        isFetching: false,
        isError: false,
        isUpdating: false,
        list: updateFetchedList(state.list,action.data.data),
        errors: null,
        lastPage: action.data.last_page
      };
    case actionTypes.DELETE_PLAN_FAIL:
    case actionTypes.DELETE_PLAN:
    case actionTypes.UPDATE_PLAN:
    case actionTypes.ADD_PLAN:
      return {
        ...state,
        isUpdating: true,
        isError: false
      };
    case actionTypes.PLAN_UPDATED:
    case actionTypes.PLAN_ADDED:
      return {
        ...state,
        isUpdating: false,
        isError: false,
        list: {
          ...state.list,
          ...updateList(state.list, action.data)
        }
      };
    case actionTypes.PLAN_DELETED:
      return {
        ...state,
        isUpdating: false,
        isError: false,
        list: deleteFamily(state.list, action.id)
      };
    case actionTypes.DELETE_PLAN_FAIL:
    case actionTypes.UPDATE_PLAN_FAIL:
    case actionTypes.FETCH_FAMILIES_FAIL:
    case actionTypes.ADD_PLAN_FAIL:
      return {
        ...state,
        isFetching: false,
        isUpdating: false,
        isError: true,
        errors: action.errors
      };
    case actionTypes.PLAN_CLEAR_ERROR:
      return {
        ...state,
        isError: false,
        errors: null
      };
    case actionTypes.RESET_PLAN:
    case RESET_STATE:
      return iniialState;
    default:
      return state;
  }
};

export default reducer;
