import React, { Component } from "react";
import { Image, View, Text } from "react-native";
import LinearGradient from "react-native-linear-gradient";
import { connect } from "react-redux";
import { NavigationActions } from "react-navigation";
import Container from "../../../components/Container";
import containerStyles from "../../../styles/container";
import Item from "../../../components/Item";
import styles from "./styles";
import Separator from "../../../components/Separator";
import {
  HEADER_GRADIENT,
  COLOR_PRIMARY,
  COLOR_GREY
} from "../../../config/common";
import ActionButton from "../../../components/ActionButton";
import { formatDate } from "../../../utils/date";
import ProfilePicture from "../../../components/ProfilePicture";

class PlanDetail extends Component {
  static navigationOptions = ({ navigation }) => {
    var action = () => {};
    if (navigation.state.params.action) {
      action = navigation.state.params.action;
    }

    return {
      headerStyle: {
        elevation: 0,
        backgroundColor: COLOR_PRIMARY
      },
      headerRight: (
        <View style={{ paddingHorizontal: 16 }}>
          <ActionButton icon="create" onPress={action} />
        </View>
      )
    };
  };

  componentDidMount() {
    this.props.navigation.setParams({
      action: this.navigateToEdit
    });
  }

  navigateToEdit = () => {
    this.props.dispatch(
      NavigationActions.navigate({
        routeName: "PlanInput",
        params: {
          title: "Edit Plan",
          id: this.props.plan.id,
          data: this.props.plan,
        }
      })
    );
  };

  render() {
    const plan = this.props.plan || {};
    var customerName = [];
    if (plan.plan_customer) {
      plan.plan_customer.forEach(function(item) {
        customerName.push(item.name);
      });
    } 
    

    var regencyName = [];
    if (plan.plan_regency) {
      plan.plan_regency.forEach(function(item) {
        regencyName.push(item.name);
      });
    }
    
    return (
      <Container style={[containerStyles.container, styles.container]}>
        <View style={styles.content}>
          <View style={styles.row}>
            <Item
              label="Judul"
              value={plan.title}
              style={{ flex: 1 }}
            />
            <Item
              label="Keterangan"
              value={plan.description || "-"}
              style={{ flex: 1 }}
            />
          </View>
          <View style={styles.row}>
            <Item
              label="Tanggal"
              value={`${formatDate(plan.first_date, "DD MMMM YYYY")} - ${formatDate(plan.second_date, "DD MMMM YYYY")}`}
              style={{ flex: 1 }}
            />
            <Item
              label="Marketing"
              value={plan.marketing.name || "-"}
              style={{ flex: 1 }}
            />
          </View>
          <Separator />
          <Item
            label="Customer"
            value={customerName.toString() ? customerName.join('\n') : "-"}
          />
          <Item
            label="Kota/Kabupaten"
            value={regencyName.toString() ? regencyName.join('\n') : "-"}
          />
        </View>
      </Container>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  const planId = ownProps.navigation.state.params.data.id;
  const plan = state.plan.list.filter(function(data) {
    return data.id == planId;
  });
  return {
    plan: ownProps.navigation.state.params.data
  };
};

export default connect(mapStateToProps)(PlanDetail);
