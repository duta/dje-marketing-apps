import EStyleSheet from "react-native-extended-stylesheet";
import { COLOR_PRIMARY_GREY } from "../../../config/common";

const styles = EStyleSheet.create({
  container: {
    alignItems: "flex-start",
    justifyContent: "flex-start",
    flexGrow: 1,
    backgroundColor: "white"
  },
  content: {
    padding: 16,
    flex: 1,
    alignSelf: "stretch",
    backgroundColor: "white"
  },
  row: {
    flexDirection: "row",
    alignSelf: "stretch",
    alignItems: "center"
  },
  header: {
    alignSelf: "stretch",
    alignItems: "center",
    justifyContent: "center",
    paddingVertical: 16,
    borderTopColor: COLOR_PRIMARY_GREY,
    borderTopWidth: 1
  },
  profilePict: {
    height: 72,
    width: 72,
    borderRadius: 36,
    marginBottom: 8,
    resizeMode: "cover"
  },
  name: {
    color: "white",
    fontSize: 16,
    fontFamily: "Arial",
    fontWeight: "bold",
    textAlign: "center",
    alignSelf: "stretch"
  }
});

export default styles;
