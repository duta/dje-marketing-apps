import React, { Component } from "react";
import { Image, View, Text, StyleSheet } from "react-native";
import { connect } from "react-redux";
import Modal from "react-native-modal";
import SnackBar from "react-native-snackbar";
import { NavigationActions, StackActions } from "react-navigation";
import Container from "../../../components/Container";
import PlanForm from "./PlanForm";
import { addPlan, updatePlan, clearError } from "../plan.action";
import { Loading, FullScreenLoading } from "../../../components/Loading";
import { COLOR_SOFTEN_RED, COLOR_VIOLET_GREY, BUTTON_GRADIENT } from "../../../config/common";

class PlanInput extends Component {
  timeout = null;
  state = {
    success: false
  };

  static navigationOptions = ({ navigation }) => {
    return {
      headerRight: <View />
    };
  };

  onFormSubmit = data => {
    if (this.props.plan === null) {
      this.props.dispatch(addPlan(data));
    } else {
      this.props.dispatch(updatePlan(this.props.plan.id, data));
    }
  };

  componentWillMount() {
    this.props.dispatch(clearError());
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.isUpdating && !nextProps.isUpdating) {
      if (!nextProps.isError) {
        this.setState({ success: true });
      } else {
        setTimeout(() => {
          SnackBar.show({
            title: "Tidak dapat menyimpan data.",
            duration: SnackBar.LENGTH_LONG,
            backgroundColor: COLOR_SOFTEN_RED
          });
        }, 500);
      }
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (!prevState.success && this.state.success) {
      this.timeout = setTimeout(this.dismissSuccessMessage, 1000);
    }
  }

  dismissSuccessMessage = () => {
    //this.setState({ success: false });
    this.redirectToList();

    if (this.timeout) {
      clearTimeout(this.timeout);
    }
  };

  renderSuccessMessage = () => {
    var text = "Penambahan plan telah berhasil";
    if (this.props.navigation.state.params.id) {
      text = "Data berhasil diperbarui";
    }

    return (
      <Modal
        isVisible={this.state.success}
        onBackButtonPress={this.dismissSuccessMessage}
        onBackdropPress={this.dismissSuccessMessage}
      >
        <View style={styles.messageWrapper}>
          <Image source={require("../../../assets/images/updated.png")} />
          <Text style={styles.message}>{text}</Text>
        </View>
      </Modal>
    );
  };

  redirectToList = () => {
    const { dispatch, routes } = this.props;
    const routeQty = routes.length;
    dispatch(NavigationActions.back());
    if (routeQty == 4) {
      dispatch(NavigationActions.back());
    }
  };

  render() {
    return (
      <Container style={{ padding: 0 }}>
        <PlanForm
          errors={this.props.errors}
          onSubmit={this.onFormSubmit}
          plan={this.props.plan}
          isLoading={this.props.isUpdating}
        />
        
        <FullScreenLoading 
          isVisible={this.props.isUpdating}
          text="Memproses data ..."
        />
        {this.renderSuccessMessage()}
      </Container>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  const planId = ownProps.navigation.state.params.id;
  const plan = ownProps.navigation.state.params.data;
  return {
    plan: planId ? plan : null,
    isUpdating: state.plan.isUpdating,
    isError: state.plan.isError,
    errors: state.plan.errors || {},
    routes: state.nav.routes
  };
};

const styles = StyleSheet.create({
  messageWrapper: {
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center",
    padding: 16,
    borderRadius: 3
  },
  message: {
    fontFamily: "Quicksand",
    color: COLOR_VIOLET_GREY,
    textAlign: "center",
    marginVertical: 16
  }
});

export default connect(mapStateToProps)(PlanInput);
