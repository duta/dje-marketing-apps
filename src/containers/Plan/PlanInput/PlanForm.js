import React, { Component } from "react";
import {
  Keyboard,
  KeyboardAvoidingView,
  ScrollView,
  Text,
  View,
} from "react-native";
import { TextInputWithLabel } from "../../../components/TextInput";
import formStyles from "../../../styles/form";
import styles from "./styles";
import { BUTTON_GRADIENT, COLOR_GREY,COLOR_PRIMARY } from "../../../config/common";
import _ from "lodash";
import DatePicker from "react-native-modal-datetime-picker";
import { formatDate, getToday } from "../../../utils/date";
import { clean } from "../../../utils/array";
import {
  pad,
} from "../../../utils/string";
import { NavigationActions } from "react-navigation";
import { connect } from "react-redux";
import { Button } from "../../../components/Button";

let initialState = {
  fields: {
    title: null,
    first_date: getToday(),
    second_date: getToday(),
    description:null,
  }
};

class PlanForm extends Component {
  state = {
    fields: initialState.fields,
    showDatePickerFirst: false,
    showDatePickerLast: false,
    selectedRegency: [],
    selectedCustomer:[],
    selectedProject:[],
  };

  componentDidMount() {
    const { plan } = this.props;
    let customers = [];
    let projects = [];
    let regencies = [];
    if(plan){
      customers = plan.plan_customer.map(function(item1){
        return item1.id
      });
      projects = plan.plan_project.map(function(item2){
        return item2.id
      });
      regencies = plan.plan_regency.map(function(item3){
        return item3.id
      }); 
    }

    var fields = {
      ... this.state.fields,
      ... plan,
      plan_cus: customers,
      plan_proj:projects,
      plan_regen:regencies,
    };

    this.setState({
      ...this.state,
      fields,
      selectedRegency: plan ? plan.plan_regency : [],
      selectedCustomer: plan ? plan.plan_customer : [],
      selectedProject: plan ? plan.plan_project : [],
    });
  }

  resetFields = () => {
    const fields = {
      title: null,
      first_date: getToday(),
      second_date: getToday(),
      description:null,
    };

    initialState.fields = fields;
    this.setState({ fields });
  };

  componentWillUnmount() {
    this.resetFields();
  }

  onSubmit() {
    const customers = this.state.selectedCustomer.map(function(item1){
      return item1.id
    });
    const projects = this.state.selectedProject.map(function(item2){
      return item2.id
    });
    const regencies = this.state.selectedRegency.map(function(item3){
      return item3.id
    });

    if (typeof this.props.onSubmit === "function") {
      const data = {
        ...this.state.fields,
        plan_cus: customers,
        plan_proj: projects,
        plan_regen: regencies,
      };
      this.props.onSubmit(clean(data));
    }
  }

  updateTextField = field => value => {
    let newFields = { ...this.state.fields };
    newFields[field] = value;
    this.setState({
      ...this.state,
      fields: { ...newFields }
    });
  };

  renderErrorMessage = field => {
    const { errors } = this.props;

    // No error
    if (typeof errors === "undefined" || typeof errors[field] === "undefined") {
      return null;
    } else {
      // Error found
      return <Text style={styles.errorMessage}> {errors[field][0]} </Text>;
    }
  };

  showDatePickerFirst = () => {
    Keyboard.dismiss();
    this.setState({
      showDatePickerFirst: true
    });
  };

  hideDatePickerFirst = () => {
    this.setState({
      showDatePickerFirst: false
    });
  };

  showDatePickerLast = () => {
    Keyboard.dismiss();
    this.setState({
      showDatePickerLast: true
    });
  };

  hideDatePickerLast = () => {
    this.setState({
      showDatePickerLast: false
    });
  };

  onDatePickerConfirmedFirst = date => {
    const d = pad(date.getMonth(), 2);
    const selectedDate = `${date.getFullYear()}-${pad(
      date.getMonth() + 1,
      2
    )}-${pad(date.getDate(), 2)}`;

    let newFields = { ...this.state.fields };
    newFields["first_date"] = selectedDate;
    newFields["second_date"] = selectedDate;

    this.setState({
      ...this.state,
      fields: { ...newFields }
    });
  };

  onDatePickerConfirmedEnd = date => {
    const d = pad(date.getMonth(), 2);
    const selectedDate = `${date.getFullYear()}-${pad(
      date.getMonth() + 1,
      2
    )}-${pad(date.getDate(), 2)}`;

    let newFields = { ...this.state.fields };
    newFields["second_date"] = selectedDate;

    this.setState({
      ...this.state,
      fields: { ...newFields }
    });
  };

  getSubmitButtonLabel = () => {
    if (this.props.isLoading) {
      return "Menyimpan data ...";
    } else {
      return "Simpan";
    }
  };

  selectCustomer = () => {
    const { dispatch } = this.props;

    dispatch(
      NavigationActions.navigate({
        routeName: "CustomerSelect",
        params: {
          type:"multy",
          callback: this.onCustomerSelected
        }
      })
    );
  };
  
  onCustomerSelected = customer => {
    this.setState({
      selectedCustomer: customer
    });
  };

  selectRegency = () => {
    const { dispatch } = this.props;

    dispatch(
      NavigationActions.navigate({
        routeName: "SelectRegency",
        params: {
          type:"multy",
          callback: this.onRegencySelected
        }
      })
    );
  };

  onRegencySelected = regency => {
    this.setState({
      selectedRegency: regency
    });
  };

  selectProject = () => {
    const { dispatch } = this.props;

    dispatch(
      NavigationActions.navigate({
        routeName: "ProjectSelect",
        params: {
          type:"multy",
          callback: this.onProjectSelected
        }
      })
    );
  };

  onProjectSelected = project => {
    this.setState({
      selectedProject: project
    });
  };

  render() {
    const { fields } = this.state;
    const props = this.props;
    var projectTitle = [];
    this.state.selectedProject.forEach(function(item) {
        projectTitle.push(item.name);
    });

    var customerName = [];
    this.state.selectedCustomer.forEach(function(item) {
      customerName.push(item.name);
    });

    var regencyName = [];
    this.state.selectedRegency.forEach(function(item) {
      regencyName.push(item.name);
    });
    
    return (
      <KeyboardAvoidingView behavior="padding" style={{flex: 1, alignSelf: "stretch"}}>
        <ScrollView style={styles.form} keyboardShouldPersistTaps="always">
          <View style={styles.section}>
            <TextInputWithLabel
              label="Judul"
              style={formStyles.inputWrapper}
              inputStyle={formStyles.input}
              labelStyle={formStyles.inputLabel}
              onChangeText={this.updateTextField("title")}
              value={fields.title}
              autoCapitalize="words"
            />
            {this.renderErrorMessage("title")}
          </View>
          <View style={styles.section}>
            <TextInputWithLabel
              label="Tanggal Mulai"
              style={formStyles.inputWrapper}
              inputStyle={formStyles.input}
              labelStyle={formStyles.inputLabel}
              value={
                fields.first_date
                  ? formatDate(fields.first_date, "DD MMMM YYYY")
                  : null
              }
              editable={false}
              onTouchStart={this.showDatePickerFirst}
            />
            {this.renderErrorMessage("first_date")}
          </View>
          <View style={styles.section}>
            <TextInputWithLabel
              label="Tanggal Selesai"
              style={formStyles.inputWrapper}
              inputStyle={formStyles.input}
              labelStyle={formStyles.inputLabel}
              value={
                fields.second_date
                  ? formatDate(fields.second_date, "DD MMMM YYYY")
                  : null
              }
              editable={false}
              onTouchStart={this.showDatePickerLast}
            />
            {this.renderErrorMessage("second_date")}
          </View>
          <View style={styles.section}>
            <TextInputWithLabel
              label="Kabupaten/Kota"
              placeholder={"Pilih kabupaten atau kota"}
              style={formStyles.inputWrapper}
              inputStyle={[formStyles.input, { textAlignVertical: "center" }]}
              labelStyle={formStyles.inputLabel}
              editable={false}
              multiline={true}
              value={
                regencyName.toString() ? regencyName.toString() : null
              }
              onTouchStart={this.selectRegency}
            />
            {this.renderErrorMessage("regency_id")}
          </View>
          <View style={styles.section}>
            <TextInputWithLabel
              label="Customer"
              placeholder={"Pilih customer"}
              style={formStyles.inputWrapper}
              inputStyle={[formStyles.input, { textAlignVertical: "center" }]}
              labelStyle={formStyles.inputLabel}
              editable={false}
              multiline={true}
              value={
                customerName.toString() ? customerName.toString() : null
              }
              onTouchStart={this.selectCustomer}
            />
            {this.renderErrorMessage("customer_id")}
          </View>
          <View style={styles.section}>
            <TextInputWithLabel
              label="Project"
              placeholder={"Pilih project"}
              style={formStyles.inputWrapper}
              inputStyle={[formStyles.input, { textAlignVertical: "center" }]}
              labelStyle={formStyles.inputLabel}
              editable={false}
              multiline={true}
              value={
                projectTitle.toString() ? projectTitle.toString() : null
              }
              onTouchStart={this.selectProject}
            />
            {this.renderErrorMessage("project_id")}
          </View>
          <View style={styles.section}>
            <TextInputWithLabel
              label="Keterangan"
              style={formStyles.inputWrapper}
              inputStyle={[formStyles.input, { textAlignVertical: "center" }]}
              labelStyle={formStyles.inputLabel}
              multiline={true}
              onChangeText={this.updateTextField("description")}
              value={fields.description}
              autoCapitalize="words"
            />
            {this.renderErrorMessage("description")}
          </View>
          <Button
            label={this.getSubmitButtonLabel()}
            style={formStyles.button}
            gradient={BUTTON_GRADIENT}
            gradientDirection="horizontal"
            disabled={this.props.isDisabled || this.props.isLoading}
            onPress={() => this.onSubmit()}
            containerStyle={{ elevation: 3 }}
          />
          <DatePicker
            isVisible={this.state.showDatePickerFirst}
            onConfirm={this.onDatePickerConfirmedFirst}
            onCancel={this.hideDatePickerFirst}
            onHideAfterConfirm={this.hideDatePickerFirst}
            minimumDate={new Date()}
            datePickerModeAndroid="spinner"
            date={new Date(fields.first_date)}
          />
          <DatePicker
            isVisible={this.state.showDatePickerLast}
            onConfirm={this.onDatePickerConfirmedEnd}
            onCancel={this.hideDatePickerLast}
            onHideAfterConfirm={this.hideDatePickerLast}
            minimumDate={new Date()}
            datePickerModeAndroid="spinner"
            date={new Date(fields.second_date)}
          />
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}

export default connect()(PlanForm);
