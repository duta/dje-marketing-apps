import { all, call, put, takeLatest } from "redux-saga/effects";
import {
  FETCH_PLANS,
  FETCH_PLANS_FAIL,
  PLANS_FETCHED,
  ADD_PLAN,
  ADD_PLAN_FAIL,
  PLAN_ADDED,
  PLAN_UPDATED,
  UPDATE_PLAN_FAIL,
  UPDATE_PLAN,
  PLAN_DELETED,
  DELETE_PLAN_FAIL,
  DELETE_PLAN,
  RESET_PLAN
} from "./plan.actionTypes";
import Api from "../../apis/api";
import ApiUtils from "../../apis/ApiUtils";

function fetchPlans(params) {
  return Api.get(`plan`, params);
  // return require('../../data/plan.json');
}

function* handleFetchingPlans(action) {
  try {
    const response = yield call(fetchPlans, action.params);
    const data = response.data;

    yield put({
      type: PLANS_FETCHED,
      data
    });
  } catch (error) {
    yield put({
      type: FETCH_PLANS_FAIL,
      errors: ApiUtils.handleErrors(error)
    });
  }
}

function addPlan(data) {
  return Api.post("plan", data);
}

function* handleAddingPlan(action) {
  try {
    const response = yield call(addPlan, action.data);
    const data = response.data.data;

    yield put({
      type: RESET_PLAN,
    });

    yield put({
      type: FETCH_PLANS,
      // type: FAMILY_ADDED,
      // data
    });
  } catch (error) {
    yield put({
      type: ADD_PLAN_FAIL,
      errors: ApiUtils.handleErrors(error)
    });
  }
}

function updatePlan(id, data) {
  return Api.patch(`plan/${id}`, data);
}

function* handleUpdatingPlan(action) {
  try {
    const response = yield call(updatePlan, action.id, action.data);
    const data = response.data.data;

    yield put({
      type: RESET_PLAN,
    });

    yield put({
      type: FETCH_PLANS,
      // type: FAMILY_UPDATED,
      // data
    });
  } catch (error) {
    yield put({
      type: UPDATE_PLAN_FAIL,
      errors: ApiUtils.handleErrors(error)
    });
  }
}

function deletePlan(id) {
  return Api.delete(`plan/${id}`);
}

function* handleDeletingPlan(action) {
  try {
    const response = yield call(deletePlan, action.id);

    yield put({
      type: RESET_PLAN,
    });
    
    yield put({
      type: FETCH_PLANS
      // type: PLAN_DELETED,
      // id: action.id
    });
  } catch (error) {
    yield put({
      type: DELETE_PLAN_FAIL,
      errors: ApiUtils.handleErrors(error)
    });
  }
}

export default function* watchAll() {
  yield all([
    takeLatest(FETCH_PLANS, handleFetchingPlans),
    takeLatest(ADD_PLAN, handleAddingPlan),
    takeLatest(UPDATE_PLAN, handleUpdatingPlan),
    takeLatest(DELETE_PLAN, handleDeletingPlan),
  ]);
}
