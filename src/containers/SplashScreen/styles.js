import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    container: {
        alignItems: "center",
        justifyContent: "center",
        paddingHorizontal: 16,
        flex: 1
    },
    logo: {
        marginBottom: 32,
    },
    loadingImage: {
        width: 32,
        resizeMode: "center"
    }
});

export default styles;
