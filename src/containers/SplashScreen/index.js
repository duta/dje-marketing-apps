import React from 'react'
import { View, Text, Image } from 'react-native'
import Background from "../../components/Background";
import styles from "./styles";
import Logo from '../../components/Logo';
import { COLOR_PRIMARY } from "../../config/common";
import { Spinner } from "native-base";

const SplashScreen = props => (
    <Background containerStyle={styles.container}>
        <Logo style={styles.logo} />
        {/* <Image 
            source={require("../../assets/images/loading.gif")}
            style={styles.loadingImage}
        /> */}
        <Spinner color={COLOR_PRIMARY} />
    </Background>
)

export default SplashScreen;
