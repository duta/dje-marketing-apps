import {
  PROVINCES_FETCH,
  PROVINCES_FETCHED,
  PROVINCES_FETCH_FAILED
} from "./select_province.action";
import { persistReducer } from "redux-persist";
import FilesystemStorage from "redux-persist-filesystem-storage";
import { RESET_STATE } from "../../config/reducer.action";

const initialState = {
  list: [],
  isFetching: false
};

const reducer = (state = initialState, action) => {
  const actionType = action.type;
  switch (actionType) {
    case PROVINCES_FETCH:
      return {
        ...state,
        isFetching: true
      };
    case PROVINCES_FETCHED:
      return {
        ...state,
        list: action.data,
        isFetching: false
      };
    case PROVINCES_FETCH_FAILED:
      return {
        ...state,
        isFetching: false
      };
    case RESET_STATE:
      return initialState;
    default:
      return state;
  }
};

const provinceReducerConfig = {
  key: "provinces",
  storage: FilesystemStorage,
  blacklist: ["isFetching"]
};

export default persistReducer(provinceReducerConfig, reducer);
