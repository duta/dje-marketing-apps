import SelectProvince from "./SelectProvince";
import reducer from "./select_province.reducer";
import saga from "./select_province.saga";

export {reducer, saga};

export default SelectProvince;
