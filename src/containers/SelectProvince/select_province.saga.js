import { all, takeLatest, call, put } from "redux-saga/effects";
import * as action from "./select_province.action";

function* fetchProvinces() {
  try {
    const response = yield call(action.getProvinces);
    const data = yield response.data.data;
    yield put({
      type: action.PROVINCES_FETCHED,
      data: data
    });
  } catch (err) {
    yield put({
      type: action.PROVINCES_FETCH_FAILED
    });
  }
}

export default all([takeLatest(action.PROVINCES_FETCH, fetchProvinces)]);
