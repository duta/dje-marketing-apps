import React, { Component } from "react";
import { connect } from "react-redux";
import _ from "lodash";
import { FlatList, View, Text, TouchableOpacity } from "react-native";
import { NavigationActions } from "react-navigation";
import Icon from "react-native-vector-icons/MaterialIcons";
import CustomComponent from "../../components/CustomComponent";
import Container from "../../components/Container";
import containerStyle from "../../styles/container";
import styles from "./styles";
import { fetchProvinces } from "./select_province.action";
import { Loading, FullPageLoading } from "../../components/Loading";

class SelectProvince extends CustomComponent {
  
  constructor(props) {
    super(props);

    this.requestData = this.requestData.bind(this);

    this.state = {
      selected: null,
      loading: false,
      type:null
    }
  }

  static navigationOptions = ({ navigation }) => {
    const { action } = navigation.state.params;
    return {
      title: "Pilih Provinsi",
      headerRight: (
        <TouchableOpacity
          activeOpacity={0.7}
          style={{ paddingHorizontal: 16 }}
          onPress={action}
        >
          <Icon size={24} name="check" color="white" />
        </TouchableOpacity>
      )
    };
  };

  onItemSelected = province => {
    let selected;
    if (this.state.type == "single") {
      selected = province;
    }else if(this.state.type == "multy"){
      selected = [...this.state.selected];
      const itemIndex = this.state.selected.indexOf(province);
      if (itemIndex === -1) {
        selected.push(province);
      } else {
        selected.splice(itemIndex, 1);
      }
    }

    this.setState({
      selected: selected
    });
    
  };

  handleButtonCheck = () => {
    if (this.state.selected === null) return;

    const { dispatch } = this.props;
    dispatch(NavigationActions.back());

    const { callback } = this.props.navigation.state.params;

    if (callback !== undefined && _.isFunction(callback)) {
      callback(this.state.selected);
    }
  };

  componentWillFocus() {
    const { type } = this.props.navigation.state.params;
    let selected;
    if (type == "single") {
      selected = null;
    }else if(type == "multy"){
      selected = [];
    }
    this.setState({
      loading: true,
      type: type,
      selected : selected
    });
  }

  requestData() {
    this.props.dispatch(fetchProvinces());
  }

  componentDidFocus() {
    
    this.setState({loading: false});

    this.props.navigation.setParams({
      action: this.handleButtonCheck
    });
    this.requestData();
  }

  renderList = () => {
    const provinces = Object.values(this.props.provinces) || [];
    const listItem = ({item}) => {
      var selected = this.state.selected;
      var check = styles.uncheck;

      if (this.state.type == "single") {
        if (_.isEqual(item, this.state.selected)) check = styles.check;
      }else if(this.state.type == "multy"){
        if (_.includes(selected, item)) {
          check = styles.check;
        }
      }

      return (
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => {
            this.onItemSelected(item);
          }}
        >
          <View style={styles.listItem}>
            <Text style={styles.nama}>{item.name}</Text>
            <View style={check}>
              <Icon name="check" color="white" size={16} />
            </View>
          </View>
        </TouchableOpacity>
      );
    };

    return (
      <FlatList
        data={provinces}
        renderItem={listItem}
        style={styles.list}
        removeClippedSubviews={false}
        extraData={this.state.selected}
        keyExtractor={(item, index) => `key-${index}`}
      />
    );
  };

  render() {
    return (
      <Container
        style={containerStyle.container}
        refreshing={this.props.fetching || this.state.loading}
        onRefresh={this.requestData}
      >
        {(!this.props.fetching && !this.state.loading) && this.renderList()}

        {/* <FullPageLoading 
          text="Mengambil data provinsi ..."
          isVisible={this.props.fetching || this.state.loading}
        /> */}
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    provinces: state.provinces.list,
    fetching: state.provinces.isFetching
  };
};

export default connect(mapStateToProps)(SelectProvince);
