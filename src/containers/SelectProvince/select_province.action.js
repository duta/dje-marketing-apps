import Api from "../../apis/api";
import { API_V3_PREFIX } from "../../config/api";

export const PROVINCES_FETCH = "PROVINCES_FETCH";
export const PROVINCES_FETCHED = "PROVINCES_FETCHED";
export const PROVINCES_FETCH_FAILED = "PROVINCES_FETCH_FAILED";

/**
 * Send request to api
 */
function getProvinces() {
  return Api.get("province");
  // return require('../../data/province.json');
}

/**
 * Redux Action
 */
function fetchProvinces() {
  return {
    type: PROVINCES_FETCH
  };
}

export { getProvinces, fetchProvinces };
