import React, { Component } from 'react';
import { View, Text } from "react-native";

class CalloutView extends Component {

    render() {
        var marketing = this.props.marketing;
        return (

            <View>
                <View>
                    <Text style={{ fontWeight: "bold" }}>
                        {marketing.name}
                    </Text>
                </View>
                <View>
                    <Text>
                        {marketing.last_activity ? marketing.last_activity.title : "-"}
                    </Text>
                </View>
            </View>

        )
    }
}

export default CalloutView;