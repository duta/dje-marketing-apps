import React, { Component } from 'react';
import MapView, { PROVIDER_GOOGLE, Marker, Callout } from 'react-native-maps'; // remove PROVIDER_GOOGLE import if not using Google Maps
import { View } from "react-native";
import containerStyles from "../../styles/container";
import styles from "./styles";
import { connect } from "react-redux";
import { fetchMarketings } from "../Marketing/marketing.action";
import { getRandomColor } from "../../utils/string";
import { COLOR_PRIMARY } from "../../config/common";
import  CalloutView  from "./CalloutView";
import { NavigationActions } from "react-navigation";

class Maps extends Component {

  state = {
      region: {
          latitude: -7.3270001,
          longitude: 112.6950613,
          latitudeDelta: 20,
          longitudeDelta: 20,
      }
  }

  static navigationOptions = ({ navigation }) => {    
      return {
        headerStyle: {
          elevation: 0,
          backgroundColor: COLOR_PRIMARY
        },
        headerRight: (
          <View style={{ paddingHorizontal: 16 }} />
        )
      };
    };

  componentDidMount() {
      const params = {
          limit : 1,
      }
      this.props.dispatch(fetchMarketings(params));

      if (this.props.coordinate) {
          var coordinate = this.props.coordinate;
          this.setState({
              ...this.state,
              region:{
                  latitude: parseFloat(coordinate.latitude),
                  longitude: parseFloat(coordinate.longitude),
                  latitudeDelta: 0.0922,
                  longitudeDelta: 0.0421,
              }
          });
      }

      console.log("state",this.state)
  }

  onItemPress(marketing){
    console.log("marketing",marketing)
    if(marketing.last_activity){
      this.showActivityDetail(marketing.last_activity);
    } 
  };

  showActivityDetail = activity => {
    const { id } = activity;
    this.props.dispatch(
      NavigationActions.navigate({ 
        routeName: "ActivityDetail", 
        params: { 
          id,
          data: activity
        } 
      })
    );
  };

  render() {
      var coordinate = this.props.coordinate;
      console.log("state2",this.state);
      return (
          <View style={containerStyles.container}>
          <MapView
            style={styles.map}
            region={this.state.region}
          >
              {!coordinate &&
              Object.values(this.props.marketings).map(item => (
                  <Marker
                  coordinate={{
                      latitude: parseFloat(item.latitude),
                      longitude: parseFloat(item.longitude),
                  }}
                  // title={item.name}
                  // description={item.last_activity.length ? item.last_activity[0].title : "-"}
                  pinColor={getRandomColor()}
                  key={`maps-${item.id}`}
                  >
                    <Callout

                      onPress={() => this.onItemPress(item)}>
                      <CalloutView marketing={item} />
                    </Callout>
                  </Marker>
              ))}

              {coordinate && 
              <Marker
              coordinate={{
                  latitude: parseFloat(coordinate.latitude),
                  longitude: parseFloat(coordinate.longitude),
              }}
              title={coordinate.marketing.name}
              description={coordinate.title}
              pinColor={getRandomColor()}
              key={`maps-${coordinate.id}`}

              />}
          </MapView>
        </View>
      );
  }
}

const mapStateToProps = (state, ownProps) => {
    const activity = ownProps.navigation.state.params.data;
    return {
      marketings: state.marketing.list || {},
      coordinate: activity ? activity : null
    };
  };
  
export default connect(mapStateToProps)(Maps);