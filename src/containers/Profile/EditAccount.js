import React, { Component } from "react";
import { connect } from "react-redux";
import SnackBar, { LENGTH_SHORT, LENGTH_LONG } from "react-native-snackbar";
import { View } from "react-native";

import Container from "../../components/Container";
import UserForm from "../../components/UserForm";
import containerStyles from "../../styles/container";
import Section from "./Settings/Section";
import styles from "./Settings/styles";
import { COLOR_SOFTEN_RED } from "../../config/common";
import Api from "../../apis/api";

class EditAccount extends Component {
  constructor(props) {
    super(props);

    this.updateAccount = this.updateAccount.bind(this);
  }

  static navigationOptions = ({ navigation }) => ({
    headerRight: <View />
  });

  handleFormSubmit = dataakun => {
    Api.post('marketing-updateaccount', dataakun)
    .catch(function(error) {
      console.log("error",error)
    })
    .then(function(response) {
      console.log("response",response)
      if (response == undefined) {
        setTimeout(() => {
          SnackBar.show(
            {
              title: "Gagal, cek email dan password.",
              duration: LENGTH_LONG,
              backgroundColor: COLOR_SOFTEN_RED
            },
            100
          );
        });
      }else{
        console.log("berhasil",response.data)
        setTimeout(() => {
          SnackBar.show({
            title: "Akun berhasil diupdate.",
            duration: LENGTH_LONG
          });
        }, 100);
        this.props.dispatch(NavigationActions.back());
      }
      
    });

  };

  render() {
    return (
      <Container style={[containerStyles.container, styles.container]}>
        <Section
          title="Akun"
          icon="account-circle"
          style={[styles.section, { marginVertical: 16 }]}
        >
          <UserForm
            isEditing={true}
            onSubmit={this.handleFormSubmit}
            errors={this.props.errors}
            email={this.props.marketing.user.email}
          />
        </Section>

      </Container>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    marketing: ownProps.navigation.state.params.data,
    errors: state.marketing.errors || {}
  };
};

export default connect(mapStateToProps)(EditAccount);
