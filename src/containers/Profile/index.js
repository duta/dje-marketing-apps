import Profile from "./Profile";
import reducer from "./profile.reducer";
import saga from "./profile.saga";

export { reducer, saga };
export default Profile;
