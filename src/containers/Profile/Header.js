import React from "react";
import { Image, Text, View } from "react-native";
import LinearGradient from "react-native-linear-gradient";
import AppBar from "../../components/AppBar";
import { HEADER_GRADIENT, COLOR_PRIMARY } from "../../config/common";
import styles from "./styles";
import BackButton from "../../components/BackButton";
import {UpdateButton} from "../../components/Button";
import ProfilePicture from "../../components/ProfilePicture";
import SettingButton from "./SettingButton";

const Header = ({
  name,
  address,
  gender,
  onBackPressed,
  photo,
  phone,
  onUpdatePressed,
  onSettingButtonPress,
  onLogoutButtonPress
}) => {
  return (
    
    <View>
      <AppBar
        title="Profile"
        backgroundColor={COLOR_PRIMARY}
        titleColor="white"
        headerLeft={<BackButton onPress={onBackPressed} />}
        headerRight={<UpdateButton onPress={onUpdatePressed}/>}
      />
      <LinearGradient
        colors={HEADER_GRADIENT}
        style={styles.header}
        locations={[0.5, 1]}
      >
        <ProfilePicture
          gender={gender}
          style={styles.imageThumbnail}
          source={photo}
          borderRadius={55}
        />

        <View style={styles.headerDetail}>
          <Text style={[styles.headerText, styles.headerDoctorName]}>
            {name}
          </Text>
          <View style={styles.row}>
            <Text style={styles.distance}>{phone}</Text>
          </View>
          <View style={styles.headerAddrWrap}>
            <Text style={[styles.headerText, styles.headerDoctorDesc]}>
              {address}
            </Text>
          </View>

          <View style={[styles.row,{marginTop:20}]}>
            <View style={{ flex: 1 }} />
            <SettingButton
              onPress={onSettingButtonPress}
              isActive={true}
              icon="settings"
              title="Pengaturan"
              width={88}
            />
            <View style={{ marginLeft:10 }} />
            <SettingButton
              onPress={onLogoutButtonPress}
              isActive={true}
              icon="exit-to-app"
              title="Logout"
              width={68}
            />
          </View>

        </View>
      </LinearGradient>
    </View>
  );
};

export default Header;
