import EStyleSheet from "react-native-extended-stylesheet";
import { Dimensions } from "react-native";
import { COLOR_INPUT_BORDER_FOCUS, COLOR_PRIMARY,COLOR_ORANGE } from "../../config/common";

const imageSize = 64;
const profilePictPosition = (Dimensions.get("window").width - imageSize) / 2;

const styles = EStyleSheet.create({
  button: {
    height: 32,
    marginHorizontal: 32,
    marginTop: 8,
    borderRadius: 16
  },
  container: {
    alignContent: "flex-start",
    justifyContent: "flex-start"
  },
  header: {
    width: Dimensions.get("window").width,
    padding: 16,
    flexDirection: "row",
    borderTopWidth: 0.3,
    borderTopColor: COLOR_ORANGE
  },
  gradientHeader: {
    height: 56,
    alignSelf: "stretch",
    alignItems: "center",
    justifyContent: "center"
  },
  profilePictWrapper: {
    position: "absolute",
    top: 16,
    left: profilePictPosition,
    height: 64,
    width: 64,
    borderRadius: 32,
    borderColor: "#F2F2F2",
    backgroundColor: "white"
  },
  profilePict: {
    height: 64,
    width: 64,
    borderRadius: 32,
    resizeMode: "cover",
  },
  userName: {
    color: COLOR_PRIMARY,
    fontWeight: "bold"
  },

  menuWrapper: {
    backgroundColor: "white",
    paddingHorizontal: 16,
    paddingVertical: 16,
    marginVertical: 8,
    marginHorizontal: 16,
    alignSelf: "stretch",
    borderColor: "#e6eef7",
    borderWidth: 2,
    borderRadius: 3
  },
  menuItem: {
    flexDirection: "row",
    justifyContent: "center",
    alignSelf: "stretch"
  },
  menuItemTextWrapper: {
    marginLeft: 24,
    borderBottomColor: "#e0e0e0",
    borderBottomWidth: 1,
    flex: 1,
    justifyContent: "center",
    paddingBottom: 10,
    marginBottom: 16,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  menuDesc: {
    color: COLOR_PRIMARY,
    fontFamily: "Arial",
    textDecorationLine: "underline"
  },
  headerDetail: {
    paddingTop: 4,
    flex: 1
  },
  headerText: {
    color: "white"
  },
  headerDoctorName: {
    fontSize: 16,
    fontWeight: "600",
    fontFamily: "Arial"
  },
  headerAddrWrap: {
    flexDirection: "row"
  },
  headerDoctorDesc: {
    fontSize: 12,
    flex: 1,
    flexWrap: "wrap",
    fontFamily: "Quicksand"
  },
  headerSpWrap: {
    marginVertical: 12,
    flex: 1
  },
  headerDoctorSp: {
    fontFamily: "Quicksand"
  },
  tabBar: {
    flexDirection: "row"
  },
  tabItem: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "white",
    borderColor: "#CCC",
    paddingVertical: 8
  },
  tabItemLabel: {
    textAlign: "center",
    fontSize: 12,
    marginHorizontal: 0,
    marginVertical: 4,
    fontFamily: "Arial",
    fontWeight: "500"
  },
  row: {
    flexDirection: "row",
    alignItems: "center"
  },
  distance: {
    fontSize: 12,
    color: "white",
    fontFamily: "Quicksand"
  },
  imageThumbnail: {
    height: 110,
    width: 110,
    resizeMode: "cover",
    marginRight: 24,
    borderRadius: 55,
  },
  deleteModalWrapper: {
    flexDirection: "row",
    alignSelf: "stretch",
    paddingVertical: 16
  },
  deleteModalButton: {
    height: 38,
    flex: 1
  },
});

export default styles;
