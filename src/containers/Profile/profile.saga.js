import { all, call, put, takeLatest } from "redux-saga/effects";
import * as actions from "./profile.action";
import Api from "../../apis/api";
import ApiUtils from "../../apis/ApiUtils";
import { API_V3_PREFIX } from "../../config/api";

function* handleFetchProfile(action) {
  try {
    const response = yield call(actions.getProfile);
    const data = yield response.data;
    yield put({
      type: actions.PROFILE_FETCHED,
      data
    });
  } catch (err) {
    console.log("error",err)
    yield put({
      type: actions.PROFILE_FAILED_FETCHED
    });
  }
}

function getCredit() {
  return Api.get('profile/get-credit', {}, {}, true, API_V3_PREFIX);
}

function* handleGetCredit(action) {
  try {
    const response = yield call(getCredit);
    const data = response.data.data;

    yield put({
      type: actions.PROFILE_CREDIT_FETCHED,
      credit: data.credit
    });

  } catch (error) {}
}


export default all([
  takeLatest(actions.FETCH_PROFILE, handleFetchProfile),
  takeLatest(actions.PROFILE_FETCH_CREDIT, handleGetCredit)
]);
