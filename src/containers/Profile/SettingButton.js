import PropTypes from "prop-types";
import React, { Component } from "react";
import { StyleSheet, Text, TouchableOpacity } from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";
import { COLOR_PRIMARY } from "../../config/common";

class SettingButton extends Component {
  static propTypes = {
    onPress: PropTypes.func,
    activeColor: PropTypes.string,
    color: PropTypes.string,
    isActive: PropTypes.bool,
    icon:PropTypes.string,
    title:PropTypes.string,
  };

  static defaultProps = {
    isActive: false,
    color: "white",
    activeColor: COLOR_PRIMARY
  };

  getColor() {
    return this.props.isActive ? this.props.activeColor : this.props.color;
  }

  getBackgroundColor() {
    return !this.props.isActive ? this.props.activeColor : this.props.color;
  }

  render() {
    return (
      <TouchableOpacity
        activeOpacity={0.8}
        style={[
          styles.wrapper,
          {
            borderColor: this.getColor(),
            backgroundColor: this.getBackgroundColor(),
            width:this.props.width
          },
          this.props.style
        ]}
        onPress={this.props.onPress}
      >
        <Icon name={this.props.icon} size={14} color={this.getColor()} />
        <Text
          style={[
            styles.text,
            {
              color: this.getColor()
            }
          ]}
        >
          {this.props.title}
        </Text>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
    flexDirection: "row",
    padding: 3,
    borderWidth: 1,
    alignContent: "center",
    // width: 88,
    borderRadius: 5
  },
  text: {
    fontSize: 11,
    fontFamily: "Arial",
    marginLeft: 4
  }
});

export default SettingButton;
