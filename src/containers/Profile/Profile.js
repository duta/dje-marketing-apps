import React, { Component } from "react";
import { Image, View, Text, Animated, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import { NavigationActions, HeaderProps, StackActions } from "react-navigation";
import styles from "./styles";
import {
  HEADER_GRADIENT,
  COLOR_PRIMARY,
  COLOR_GREY,
  BUTTON_GRADIENT
} from "../../config/common";
import { TabViewAnimated, TabView } from "react-native-tab-view";
import CustomComponent from "../../components/CustomComponent";
import Header from "./Header";
import ProjectHistory from "../Marketing/ProjectHistory";
import PlanHistory from "../Marketing/PlanHistory";
import ActivityHistory from "../Marketing/ActivityHistory";
import { doLogout as logout } from "../Login/login.actions";
import ModalDialog from "../../components/Modal";
import { GradientBorderButton, Button} from "../../components/Button";

class Profile extends CustomComponent {
  
  static navigationOptions = ({ navigation }) => {
    const header = navigation.state.params.header || HeaderProps;

    return {header};
  };

  constructor(props) {
    super(props);
    this.state = {
      tabs: {
        index: 1,
        routes: [
          { key: "project", title: "Project" },
          { key: "plan", title: "Rencana" },
          { key: "activity", title: "Aktivitas" }
        ]
      },
      keyboardShow: false,
      loading: false,
      showDeleteDialog:false
    };
  }

  componentDidMount() {
    this.setHeader();
  }

  navigateToEdit = () => {
    this.props.dispatch(
      NavigationActions.navigate({
        routeName: "MarketingInput",
        params: {
          title: "Edit Profile",
          id: this.props.marketing.id,
          data: this.props.marketing
        }
      })
    );
  };

  navigateToEditAccount = () => {
    this.props.dispatch(
      NavigationActions.navigate({
        routeName: "EditAccount",
        params: {
          title: "Edit Akun",
          id: this.props.marketing.id,
          data: this.props.marketing
        }
      })
    );
  };

  setHeader(){
    const { navigation } = this.props;
    const marketing = this.props.marketing || {};
    let imageSource = null;
    if (null !== marketing.photo){
      imageSource = { uri: marketing.url_photo };
    }

    navigation.setParams({header: (
      <Header
        name={marketing.name}
        address={marketing.address}
        gender={marketing.gender}
        photo={imageSource}
        phone={marketing.phone}
        onBackPressed={() => navigation.dispatch(NavigationActions.back())}
        onUpdatePressed={() => this.navigateToEdit()}
        onLogoutButtonPress={this.confirmLogout}
        onSettingButtonPress={() => this.navigateToEditAccount()}
      />
    )
    });
  }

  //fungsi logout
  doLogout = () => {
    const { dispatch } = this.props;
    dispatch(logout());
  };

  componentWillReceiveProps(nextProps) {
    if (this.props.isFetching && !nextProps.isFetching) {
      setTimeout(() => {
        this.setHeader();
      }, 500);
    }

    if (!nextProps.isLoggedIn) {
      this.props.dispatch(
        StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: "Login" })]
        })
      );
    }
    
  }

  renderTab(){
    return (
      <TabViewAnimated
        renderHeader={this.renderTabHeader}
        navigationState={this.state.tabs}
        renderScene={this.renderScene}
        onIndexChange={this.handleTabIndexChange}
      />
    );
  };

  renderTabHeader = props => {
    const inputRange = props.navigationState.routes.map((x, i) => i);
    return (
      <View style={styles.tabBar}>
        {props.navigationState.routes.map((route, i) => {
          const color = props.position.interpolate({
            inputRange,
            outputRange: inputRange.map(
              inputIndex => (props.navigationState.index === i ? COLOR_PRIMARY : "#999")
            )
          });
          
          const borderBottomColor =
            props.navigationState.index === i ? COLOR_PRIMARY : "#CCC";
          const borderBottomWidth = props.navigationState.index === i ? 3 : 1;
          
          return (
            <TouchableOpacity
              activeOpacity={0.8}
              style={[
                styles.tabItem,
                { borderBottomColor, borderBottomWidth: borderBottomWidth }
              ]}
              onPress={() => this.handleTabIndexChange(i)}
              key={i}
            >
              <Animated.Text style={[styles.tabItemLabel, { color }]}>
                {route.title}
              </Animated.Text>
            </TouchableOpacity>
          );
        })}
        {this.renderDeleteDialog()}
      </View>
    );
  };

  renderScene = ({ route }) => {
    var id = this.props.marketing ? this.props.marketing.id : 0;
    switch (route.key | this.state.tabs.index) {
      case "project" | 0:
        return (<ProjectHistory marketingId={id} />);
      case "plan" | 1:
        return (<PlanHistory marketingId={id}/>);
      case "activity" | 2:
        return (<ActivityHistory marketingId={id}/>);
    }
  };

  handleTabIndexChange = index =>
    this.setState({
      tabs: {
        ...this.state.tabs,
        index
      }
    });
  
  
    renderDeleteDialog = () => {
      return (
        <ModalDialog
          isVisible={this.state.showDeleteDialog}
          title="Apakah Anda yakin logout ?"
          onClose={this.hideDeleteDialog}
        >
          <View style={styles.deleteModalWrapper}>
            <GradientBorderButton
              borderGradient={BUTTON_GRADIENT}
              label="Batal"
              backgroundColor="white"
              textColor={COLOR_PRIMARY}
              style={{ alignSelf: "stretch", height: 34 }}
              containerStyle={[
                styles.deleteModalButton,
                { borderRadius: 3, marginRight: 8 }
              ]}
              borderWidth={2}
              onPress={this.hideDeleteDialog}
            />
  
            <Button
              gradient={BUTTON_GRADIENT}
              label="OK"
              style={styles.deleteModalButton}
              containerStyle={[styles.deleteModalButton, { marginLeft: 8 }]}
              onPress={() => this.doLogout()}
              gradientDirection="horizontal"
            />
          </View>
        </ModalDialog>
      );
    };
  
    hideDeleteDialog = () => {
      this.setState({
        showDeleteDialog: false,
      });
    };
  
    confirmLogout = () => {
      this.setState({
        showDeleteDialog: true
      });
    };

  render() {
    return (
      this.renderTab()
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    marketing: state.user.profile.data ? state.user.profile.data[0] : null,
    isLoggedIn: state.auth.isLoggedIn,
    isFetching: state.user.isFetching
  };
};

export default connect(mapStateToProps)(Profile);
