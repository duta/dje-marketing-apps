import * as actions from "./profile.action.js";
import { persistReducer } from "redux-persist";
import FilesystemStorage from "redux-persist-filesystem-storage";
import { RESET_STATE } from "../../config/reducer.action";

const initialState = {
  profile: {},
  isFetching: false,
  isUpdating: false,
  isFailed: false,
  errors: []
};

const reducer = (state = initialState, action) => {
  const actionType = action.type;
  switch (actionType) {
    case actions.FETCH_PROFILE:
      return {
        ...state,
        isFetching: true
      };
    case actions.PROFILE_FETCHED:
      return {
        ...state,
        isFetching: false,
        isFailed: false,
        profile: action.data
      };
    case actions.PROFILE_FAILED_FETCHED:
      return {
        ...state,
        isFailed: true
      };
    case actions.UPDATE_PROFILE:
      return {
        ...state,
        isUpdating: true,
        isFailed: false
      };
    case actions.PROFILE_CREDIT_FETCHED:
      return {
        ...state,
        profile: {
          ... state.profile,
          credit: action.credit
        }
      };
    case actions.PROFILE_UPDATE_FAILED:
      return {
        ...state,
        isUpdating: false,
        isFailed: true,
        errors: action.errors
      };
    case RESET_STATE:
      return initialState;
    default:
      return state;
  }
};

export default persistReducer(
  {
    key: "user",
    storage: FilesystemStorage,
    whitelist: ["profile"]
  },
  reducer
);
