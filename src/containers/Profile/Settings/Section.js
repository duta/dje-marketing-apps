import React from "react";
import { View, Text } from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";
import EStyleSheet from "react-native-extended-stylesheet";

import styles from "./styles";
import { COLOR_VIOLET_GREY } from "../../../config/common";

const Section = props => (
  <View style={props.style}>
    <View style={styles.sectionHeader}>
      <Icon name={props.icon} size={24} color={COLOR_VIOLET_GREY} />
      <Text style={styles.sectionTitle}>{props.title}</Text>

      {props.rightButton}
    </View>
    <View>{props.children}</View>
  </View>
);

export default Section;
