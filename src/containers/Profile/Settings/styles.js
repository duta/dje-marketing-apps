import EStyleSheet from "react-native-extended-stylesheet";
import { COLOR_VIOLET_GREY } from "../../../config/common";
import { Dimensions } from "react-native";

const styles = EStyleSheet.create({
  container: {
    alignItems: "flex-start",
    justifyContent: "flex-start"
  },
  section: {
    backgroundColor: "white",
    marginHorizontal: 16,
    marginVertical: 8,
    padding: 16,
    alignSelf: "stretch",
    borderColor: "#e6eef7",
    borderWidth: 2
  },
  "section:first-child": {
    marginTop: 16
  },
  "section:last-child": {
    marginBottom: 16
  },
  sectionHeader: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 16,
    alignSelf: "stretch"
  },
  sectionTitle: {
    fontWeight: "bold",
    marginLeft: 8,
    color: COLOR_VIOLET_GREY
  },
  accountRightButton: {
    position: "absolute",
    right: 0
  },
  header: {
    borderLeftWidth: 6,
    borderLeftColor: "rgba(6, 190, 182, 0.5)",
    backgroundColor: "rgba(6, 190, 182, 0.2)",
    alignSelf: "stretch",
    height: 43
  },
  wrapper: {
    alignSelf: "stretch",
    marginBottom: 8,
    width: "100%"
  },
  itemHeader: {
    alignSelf: "stretch",
    flexDirection: "row",
    alignItems: "center"
  },
  itemTitle: {
    fontFamily: "Arial",
    marginRight: 12
  },
  content: {
    padding: 8,
    backgroundColor: "white",
    borderColor: "#DDD",
    borderWidth: 1
  },
  item: {
    backgroundColor: "white",
    alignSelf: "stretch",
    padding: 4,
    flexDirection: "row",
    alignSelf: "stretch",
    alignItems: "flex-start"
  }
});

export default styles;
