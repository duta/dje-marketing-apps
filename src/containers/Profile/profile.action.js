import Api from "../../apis/api";

export const FETCH_PROFILE = "FETCH_PROFILE";
export const PROFILE_FETCHED = "PROFILE_FETCHED";
export const PROFILE_FAILED_FETCHED = "PROFILED_FAILED_FETCHED";

export const UPDATE_PROFILE = "UPDATE_PROFILE";
export const PROFILE_UPDATED = "PROFILE_UPDATED";
export const PROFILE_UPDATE_FAILED = "PROFILE_UPDATE_FAILED";

export const PROFILE_FETCH_CREDIT = "PROFILE_FETCH_CREDIT";
export const PROFILE_CREDIT_FETCHED = "PROFILE_CREDIT_FETCHED";

export function getCredit() {
  return {
    type: PROFILE_FETCH_CREDIT
  };
}

export function fetchProfile() {
  return {
    type: FETCH_PROFILE
  };
}

export function getProfile() {
  return Api.get("marketing", { type: "profile" });
}

export function updateProfile(id, data) {
  return {
    type: UPDATE_PROFILE,
    id,
    data
  };
}
