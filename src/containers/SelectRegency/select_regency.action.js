import Api from "../../apis/api";

export const REGENCIES_FETCH = "REGENCIES_FETCH";
export const REGENCIES_FETCHED = "REGENCIES_FETCHED";
export const REGENCIES_FETCH_FAILED = "REGENCIES_FETCH_FAILED";

export const REGENCIES_SEARCH = "REGENCIES_SEARCH";
export const REGENCIES_RESET_SEARCH = "REGENCIES_RESET_SEARCH";

/**
 * Send request to api
 */
function getRegencies() {
  return Api.get("regency");
  // return require('../../data/city.json');
}

/**
 * Redux Action
 */
function fetchRegencies() {
  return {
    type: REGENCIES_FETCH
  };
}

/**
 * Redux Action
 */
function searchRegencies(params) {
  return {
    type: REGENCIES_SEARCH,
    params
  };
}

function resetSearch(){
  return {
    type: REGENCIES_RESET_SEARCH
  }
};

export { getRegencies, fetchRegencies, searchRegencies, resetSearch };
