import SelectRegency from "./SelectRegency";
import reducer from "./select_regency.reducer";
import saga from "./select_regency.saga";

export {reducer, saga};

export default SelectRegency;
