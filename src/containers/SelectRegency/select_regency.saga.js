import { all, takeLatest, call, put } from "redux-saga/effects";
import * as action from "./select_regency.action";

function* fetchRegencies() {
  try {
    const response = yield call(action.getRegencies);
    const data = yield response.data.data;
    yield put({
      type: action.REGENCIES_FETCHED,
      data: data
    });
  } catch (err) {
    yield put({
      type: action.REGENCIES_FETCH_FAILED
    });
  }
}

export default all([takeLatest(action.REGENCIES_FETCH, fetchRegencies)]);
