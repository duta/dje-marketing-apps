import {
  REGENCIES_FETCH,
  REGENCIES_FETCHED,
  REGENCIES_FETCH_FAILED,
  REGENCIES_SEARCH,
  REGENCIES_RESET_SEARCH
} from "./select_regency.action";
import { persistReducer } from "redux-persist";
import FilesystemStorage from "redux-persist-filesystem-storage";
import { RESET_STATE } from "../../config/reducer.action";
import _ from "lodash";

const initialState = {
  list: [],
  isFetching: false,
  isSearching: false,
  search: {},
};

const searchRegencies = (regencies, params) => {
  const searchName = _.lowerCase(params.name);
  const filteredRegency = _.filter(regencies, function(regency) {
    const regencyName = _.lowerCase(regency.name);

    return regencyName.includes(searchName);
  });

  return filteredRegency;
};

const reducer = (state = initialState, action) => {
  const actionType = action.type;
  switch (actionType) {
    case REGENCIES_FETCH:
      return {
        ...state,
        isFetching: true
      };
    case REGENCIES_FETCHED:
      return {
        ...state,
        list: _.mapKeys(action.data, "id"),
        isFetching: false
      };
    case REGENCIES_FETCH_FAILED:
      return {
        ...state,
        isFetching: false
      };
    case REGENCIES_SEARCH: {
      return {
        ...state,
        isSearching: true,
        search: searchRegencies(state.list, action.params)
      };
    }
    case REGENCIES_RESET_SEARCH:
      return {
        ...state,
        isSearching: false,
        search: {}
      };
    case RESET_STATE:
      return initialState;
    default:
      return state;
  }
};

const locationReducerConfig = {
  key: "cities",
  storage: FilesystemStorage,
  blacklist: ["isFetching"]
};

export default persistReducer(locationReducerConfig, reducer);
