import React, { Component } from "react";
import { Text, View } from "react-native";
import {
  NavigationActions,
  TabNavigator,
  TabBarBottom,
  StackActions
} from "react-navigation";
import { connect } from "react-redux";
import EStyleSheet from "react-native-extended-stylesheet";

// import TabBar from "../../components/TabBar";

import Dashboard from "./Dashboard";
// import Notification from "../Notification";
// import Profile from "../Profile";
import Help from "../Help";

// const TabMenus = TabNavigator(
//   {
//     Home: {
//       screen: Dashboard
//     },
//     Notifications: {
//       screen: Notification
//     },
//     Profile: {
//       screen: Profile
//     },
//     Help: {
//       screen: Help
//     }
//   },
//   {
//     tabBarPosition: "bottom",
//     tabBarComponent: TabBarBottom,
//     tabBarOptions: {
//       activeTintColor: "#3FD08C",
//       inactiveTintColor: "#757B95",
//       labelStyle: {
//         fontSize: 10
//       },
//       style: {
//         height: 49,
//         backgroundColor: "#FFF",
//         paddingBottom: 3
//       }
//     }
//   }
// );

class Home extends Component {
  // componentWillMount() {
  //   const { dispatch, navigation, isLoggedIn } = this.props;

  //   if (!isLoggedIn) {
  //     const resetToLogin = StackActions.reset({
  //       index: 0,
  //       actions: [NavigationActions.navigate({ routeName: "Login" })]
  //     });

  //     dispatch(navigation.dispatch(resetToLogin));
  //   }
  // }

  render() {
    return <TabMenus />;
  }
}

const mapStateToProps = state => ({
  isLoggedIn: state.login.isLoggedIn
});

export default connect(mapStateToProps)(Home);
// export default Home;
