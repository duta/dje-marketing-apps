import React, { Component } from "react";
import { BackHandler, Dimensions, Image, View } from "react-native";
import EStyleSheet from "react-native-extended-stylesheet";
import { NavigationActions } from "react-navigation";
import { connect } from "react-redux";
import Container from "../../components/Container";
import { Line } from "../../components/Chart"
import containerStyle from "../../styles/container";
import NotificationRecived from "../Notification/NotificationRecived";
import MenuItem from "./components/MenuItem";
import styles from "./styles";
import { fetchChart } from "../Marketing/marketing.action";
import { fetchNotifications } from "../Notification/notification.action";

class Dashboard extends Component {

  constructor(props) {
    super(props);
    this.state = {
      menuDisabled: false
    };

    this.requestData = this.requestData.bind(this);
  }

  static navigationOptions = {
    tabBarLabel: "Home",
    tabBarIcon: ({ tintColor }) => (
      <Image
        source={require("../../assets/icon/tab-bottom/home-new.png")} 
        style={[{ tintColor: tintColor, height: 24, width: 24 }]}
      />
    )
  };
  
  componentDidMount() {
    this.requestData();
  }

  requestData() {
    this.props.dispatch(fetchChart());
    this.props.dispatch(fetchNotifications());
  }

  onMenuPress = (route, params = {}) => () => {
    this.setState({menuDisabled: true});

    const { dispatch } = this.props;
    dispatch(
      NavigationActions.navigate({
        routeName: route,
        params
      })
    );

    setTimeout(() => {
      this.setState({menuDisabled: false});
    }, 5000);
  };

  render() {
    return (
      <Container 
        style={[containerStyle.container, styles.container]}
        refreshing={this.props.isFetching}
        onRefresh={this.requestData}
      >
        <NotificationRecived />
        <Line chart={this.props.chart} />
        <View style={styles.menuContainer}>
          <MenuItem
            onPress={this.onMenuPress("Customer", { title: "Customer" })}
            style={EStyleSheet.child(styles, "menuItem", 0, 2)} 
            source={require("../../assets/icon/main-menu/customer.png")}
            label="Customer"
            disabled={this.state.menuDisabled}
          />
          <MenuItem
            style={EStyleSheet.child(styles, "menuItem", 1, 2)}
            source={require("../../assets/icon/main-menu/project.png")}
            label="Project"
            onPress={this.onMenuPress("Project", { title: "Project" })}
            disabled={this.state.menuDisabled}
          />          
        </View>
        <View style={styles.menuContainer}>
          <MenuItem
            style={EStyleSheet.child(styles, "menuItem", 0, 2)}
            source={require("../../assets/icon/main-menu/perencanaan.png")}
            label="Plan"
            onPress={this.onMenuPress("Plan")}
            disabled={this.state.menuDisabled}
          />
          <MenuItem
            style={EStyleSheet.child(styles, "menuItem", 1, 2)}
            source={require("../../assets/icon/main-menu/aktifitas.png")}
            label="Aktifitas"
            onPress={this.onMenuPress("Activity", { title: "Aktifitas" })}
            disabled={this.state.menuDisabled}
          />
        </View>
        {this.props.roleUser == 1 &&
        <View style={styles.menuContainer}>
          <MenuItem
            onPress={this.onMenuPress("Marketing", { title: "Marketing" })}
            style={EStyleSheet.child(styles, "menuItem", 0, 2)}
            source={require("../../assets/icon/main-menu/marketing.png")}
            label="Marketing"
            disabled={this.state.menuDisabled}
          />
          <MenuItem
            style={EStyleSheet.child(styles, "menuItem", 1, 2)}
            source={require("../../assets/icon/main-menu/peta.png")}
            label="Maps"
            onPress={this.onMenuPress("Maps", { title: "Maps" })}
            disabled={this.state.menuDisabled}
          />
        </View>}
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  banner: state.banner,
  chart: state.marketing.chart,
  isFetching: state.marketing.isFetching,
  navIndex: state.nav.index,
  roleUser: state.user.profile.data ? state.user.profile.data[0].user.user_role : 2
});

export default connect(mapStateToProps)(Dashboard);
// export default Dashboard;
