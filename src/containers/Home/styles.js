import EStyleSheet from "react-native-extended-stylesheet";

const styles = EStyleSheet.create({
  container: {
    alignItems: "flex-start",
    justifyContent: "flex-start",
    padding: 8,
    elevation: 4
  },
  buttonContainer: {
    alignSelf: "stretch",
    marginTop: 8
  },
  bookButton: {
    height: 50,
    backgroundColor: "$primaryColor"
  },
  sliderContainer: {
    height: 132,
    alignSelf: "stretch",
    alignItems: "center",
    backgroundColor: "white",
    marginBottom: 8
  },
  menuContainer: {
    alignSelf: "stretch", 
    flexDirection: "row",
    paddingVertical: 4
  },
  menuItem: {
    alignSelf: "stretch",
    flex: 1,
    backgroundColor: "white",
    borderRadius: 1,
    borderColor: "#FFF",
  },
  "menuItem:first-child": {
    marginRight: 8
  }
});

export default styles;
