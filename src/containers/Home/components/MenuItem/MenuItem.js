import React, { Component } from "react";
import { TouchableOpacity } from "react-native";

import { ImageText } from "../../../../components/ImageText";

import styles from "./styles";

class MenuItem extends Component {
  render() {
    const { disabled, style, onPress, icon, label, source } = this.props;

    return (
      <TouchableOpacity
        onPress={onPress}
        style={[styles.container, style]}
        activeOpacity={0.6}
        disabled={disabled}
      >
        <ImageText
          icon={icon}
          label={label}
          style={style}
          source={source}
          iconStyle={styles.iconStyle}
          labelStyle={styles.text}
        />
      </TouchableOpacity>
    );
  }
}

export default MenuItem;
