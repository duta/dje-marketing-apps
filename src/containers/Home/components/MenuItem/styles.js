import EStyleSheet from "react-native-extended-stylesheet";

const styles = EStyleSheet.create({
  container: {
    alignSelf: "stretch",
    height: 88
  },
  iconStyle: {
    height: 42,
    resizeMode: "contain",
    marginBottom: 6
  },
  text: {
    fontFamily: "Arial",
    fontWeight: "500"
  }
});

export default styles;
