import React from "react";
import { Text } from "react-native";
import PropTypes from "prop-types";

import styles from "./styles";
import {
  COLOR_PRIMARY,
  COLOR_SOFTEN_RED,
  COLOR_ORANGE
} from "../../../config/common";

const QueueStatus = props => {
  const { status } = props;
  let statusStyles = [styles.status];
  let text = status;
  
  switch (status) {
    case 0:
      statusStyles.push({ color: "blue",height:23 });
      text = "Meet";
      break;
    case 1:
      statusStyles.push({ color: COLOR_ORANGE,height:23 });
      text = "Call";
      break;
    case 2:
      statusStyles.push({ color: COLOR_PRIMARY,height:23 });
      text = "Stay";
      break;
  }

  return <Text style={statusStyles}>{text}</Text>;
};

QueueStatus.propTypes = {
  status: PropTypes.oneOf([0, 1, 2]).isRequired
};

export default QueueStatus;
