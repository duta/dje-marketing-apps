import React, { Component } from "react";
import { BackHandler, FlatList, View, Text, HeaderProps, SafeAreaView, KeyboardAvoidingView, TouchableOpacity,RefreshControl } from "react-native";
import { NavigationActions } from "react-navigation";
import { connect } from "react-redux";
import containerStyles from "../../../styles/container";
import ActionButton from "../../../components/ActionButton";
import ListItem from "./ListItem";
import styles from "./styles";
import {
  BUTTON_GRADIENT,
  COLOR_PRIMARY,
  COLOR_SOFTEN_RED
} from "../../../config/common";
import {
  fetchActivities,
  resetActivity,
  clearError,
} from "../activity.action";
import { FullPageLoading } from "../../../components/Loading";
import CustomComponent from "../../../components/CustomComponent";
import SearchBar from "../../../components/SearchBar";
import FilterForm from "./FilterForm";
import DatePicker from "react-native-modal-datetime-picker";
import { pad } from "../../../utils/string";
import Icon from "react-native-vector-icons/MaterialIcons";
import SnackBar from "react-native-snackbar";
import inflateError from "../../../utils/inflateError";

class ActivityList extends CustomComponent {
  state = {
    searching: false,
    searchText: null,
    showFilterForm: false,
    selectedProvince: [],
    showDatePickerFirst:false,
    showDatePickerEnd:false,
    firstDate:null,
    endDate:null,
    selectedMarketing:null,
    selectedTypes:[],
    currentPage:1
  };

  constructor(props) {
    super(props);

    this.requestData = this.requestData.bind(this);
    this.handleLoadMore = this.handleLoadMore.bind(this);
    this.refreshData = this.refreshData.bind(this);
  }

  static navigationOptions = ({ navigation }) => ({
    header: navigation.state.params.searchBar || HeaderProps,
    headerRight: (
      <View style={{ paddingHorizontal: 16 }}>
        {navigation.state.params.right !== undefined
          ? navigation.state.params.right
          : null}
      </View>
    )
  });

  componentWillUnmount() {
    super.componentWillUnmount();
    this.cancelSearch();
  }

  componentWillMount() {
    this.setState({loading: true});
  }

  componentDidFocus() {
    
    this.setState({loading: false});
    this._showSearchButton();

    if (!this.state.fetched) {
      this.refreshData();
      this.setState({fetched: true});
    }
  }

  /**
   * Show search button
   *
   */
  _showSearchButton = () => {
    const { searching } = this.state;
    if (!searching) {
      let add_button = <ActionButton onPress={this.onAddButtonPress} icon="add" />;
      let margin = 16;
      this.props.navigation.setParams({
        right: (
          <View style={{ flexDirection: "row" }}>
            <ActionButton
              onPress={this._showSearchForm}
              icon="search"
              style={{ marginRight: margin }}
            />
            {add_button}
          </View>
        )
      });
    }
  };

  /**
   * Do search
   *
   */
  doSearch = text => {
    this.setState({ searchText: text });
    this.refreshData();
  };

  /**
   * Cancel search
   *
   */
  cancelSearch = () => {
    this.props.navigation.setParams({ searchBar: null });
    this.setState({ searching: false, searchText: null });
    this.refreshData();
  };

  /**
   * Show search form on header
   *
   */
  _showSearchForm = () => {
    this.setState({ searching: true });
    this.props.navigation.setParams({
      searchBar: (
        <SafeAreaView style={{backgroundColor: COLOR_PRIMARY}}>
          <SearchBar
            backgroundColor={COLOR_PRIMARY}
            placeholder="Ketikan judul aktivitas"
            onCancel={this.cancelSearch}
            onChangeText={this.doSearch}
            text={this.state.searchText}
          />
        </SafeAreaView>
      )
    });
  };

  requestData() {
    const provinces = this.state.selectedProvince.map(function(item){
      return item.id;
    });

    const params = {
      page : this.state.currentPage,
      search : this.state.searchText,
      province : provinces,
      marketing : this.state.selectedMarketing ? this.state.selectedMarketing.id : null,
      first_date : this.state.firstDate,
      second_date : this.state.endDate,
    }
    this.props.dispatch(fetchActivities(params));
  }

  refreshData(){
    this.props.dispatch(resetActivity());
    this.setState({
      currentPage: 1,
    },() => {this.requestData()});
  }

  onEditButtonPress = activity => {
    this.props.dispatch(
      NavigationActions.navigate({
        routeName: "ActivityInput",
        params: {
          title: "Edit Activity",
          id: activity.id,
          data: activity
        }
      })
    );
  };

  onItemPress = activity => {
     this.showActivityDetail(activity);
  };

  showActivityDetail = activity => {
    const { id } = activity;
    this.props.dispatch(
      NavigationActions.navigate({ 
        routeName: "ActivityDetail", 
        params: { 
          id,
          data: activity
        } 
      })
    );
  };

  renderList() {
    const activities = Object.values(this.props.activities) || [];
    const listItem = listItem => {
      const activity = listItem.item;

      return (
        <ListItem
          title={activity.title}
          description={activity.description}
          date={activity.first_date}
          status={activity.type}
          marketing={activity.marketing.name}
          onPress={() => this.onItemPress(activity)}
          // editAction={() => this.onEditButtonPress(activity)}
          // deleteAction={() => this.onDeleteButtonPress(activity)}
          key={`activity-list-${activity.id}`}
          swipeDisable={true}
        />
      );
    };

    return (
      <FlatList
        data={activities}
        renderItem={listItem}
        style={styles.list}
        extraData={this.state.selectedMarketing}
        keyExtractor={item => `key-${item.id}`}
        removeClippedSubviews={false}
        onEndReached={this.handleLoadMore}
        onEndReachedThreshold={0.3}
        refreshControl={
          <RefreshControl
            refreshing={this.props.isFetching}
            onRefresh={this.refreshData}
          />
        }
      />
    );
  }

  onAddButtonPress = () => {
    this.props.dispatch(
      NavigationActions.navigate({
        routeName: "ActivityInput",
        params: {
          title: "Tambah Activity"
        }
      })
    );
  };

  componentWillReceiveProps(nextProps) {
    if (!this.props.isError && nextProps.isError) {
      setTimeout(() => {
        SnackBar.show({
          title: inflateError(nextProps.errors),
          duration: SnackBar.LENGTH_LONG,
          backgroundColor: COLOR_SOFTEN_RED
        });
      }, 500);
    }
  }

  handleLoadMore() {
    if (this.props.lastPage !== this.state.currentPage) {
      this.setState({
        currentPage: parseInt(this.state.currentPage) + 1,
      },() => {this.requestData()});
    }
  };

  renderFilterButton = () => {
    return (
      <View style={styles.filterButtonWrapper}>
        <TouchableOpacity
          style={[styles.filterButton, styles.filterButtonBorder]}
          onPress={this.showFilterForm}
        >
          <Icon name="filter-list" size={24} color={COLOR_PRIMARY} />
          <Text style={styles.filterButtonText}>Filter</Text>
        </TouchableOpacity>
        {/* <TouchableOpacity style={styles.filterButton} onPress={this.showMap}>
          <Icon name="map" size={24} color={COLOR_PRIMARY} />
          <Text style={styles.filterButtonText}>Sekitar Saya</Text>
        </TouchableOpacity> */}
      </View>
    );
  };

  showFilterForm = () => {
    this.setState({ showFilterForm: true });
  };

  hideFilterForm = () => {
    this.setState({ showFilterForm: false });
  };
  
  onFilterTypeItemPress = type => {
    var selectedTypes = this.state.selectedTypes;
    const itemIndex = this.state.selectedTypes.indexOf(type.id);
    if (itemIndex === -1) {
      selectedTypes.push(type.id);
    } else {
      selectedTypes.splice(itemIndex, 1);
    }

    this.setState({
      selectedTypes
    });
  };

  selectProvince = () => {
    this.props.dispatch(
      NavigationActions.navigate({
        routeName: "SelectProvince",
        params: {
          callback: this.onProvinceSelected,
          type: "multy"
        }
      })
    ); 
    this.hideFilterForm();
  };

  onProvinceSelected = province => {
    this.setState({ selectedProvince: province });
    this.showFilterForm();
  };

  selectMarketing = () => {
    this.props.dispatch(
      NavigationActions.navigate({
        routeName: "MarketingSelect",
        params: {
          callback: this.onMarketingSelected,
          type: "single"
        }
      })
    ); 
    this.hideFilterForm();
  };

  onMarketingSelected = marketing => {
    this.setState({ 
      selectedMarketing:marketing
    });
    this.showFilterForm();
  };

  onFilterReset = () => {
    this.setState({
      selectedProvince: [],
      firstDate:null,
      endDate:null,
      selectedMarketing: null,
      selectedTypes:[],
    });
  };

  onFilter = () => {
    this.refreshData();
    this.hideFilterForm();
  };

  showDatePickerFirst = () => {
    this.setState({
      showDatePickerFirst: true
    });
  };

  hideDatePickerFirst = () => {
    this.setState({
      showDatePickerFirst: false
    });
  };

  onDatePickerConfirmedFirst = date => {
    const selectedDate = `${date.getFullYear()}-${pad(
      date.getMonth() + 1,
      2
    )}-${pad(date.getDate(), 2)}`;

    this.setState({
      firstDate : selectedDate,
      endDate : selectedDate
    });
  };

  showDatePickerEnd = () => {
    this.setState({
      showDatePickerEnd: true
    });
  };

  hideDatePickerEnd = () => {
    this.setState({
      showDatePickerEnd: false
    });
  };

  onDatePickerConfirmedEnd = date => {
    const selectedDate = `${date.getFullYear()}-${pad(
      date.getMonth() + 1,
      2
    )}-${pad(date.getDate(), 2)}`;

    this.setState({
      endDate : selectedDate
    });
  };

  render() {
    const typeList = this.props.types;
    return (
      <KeyboardAvoidingView style={containerStyles.container}>
        {/* <FullPageLoading
          isVisible={this.props.isFetching}
          text="Mengambil data ..."
        /> */}

        <View style={styles.listWrapper}>
          {this.renderList()}
        </View>
        {this.renderFilterButton()}
        <FilterForm
          types={typeList}
          isVisible={this.state.showFilterForm}
          onClose={this.hideFilterForm}
          onTypePress={this.onFilterTypeItemPress}
          selectedTypes={this.state.selectedTypes}
          onProvince={this.selectProvince}
          selectedProvince={this.state.selectedProvince}
          onMarketing={this.selectMarketing}
          selectedMarketing={this.state.selectedMarketing}
          showDatePickerFirst= {this.showDatePickerFirst}
          valFirstDate={this.state.firstDate}
          showDatePickerEnd={this.showDatePickerEnd}
          valEndDate={this.state.endDate}
          onFilter={this.onFilter}
          onFilterReset={this.onFilterReset}
          roleUser={this.props.roleUser}
        />
        <DatePicker
          isVisible={this.state.showDatePickerFirst}
          onConfirm={this.onDatePickerConfirmedFirst}
          onCancel={this.hideDatePickerFirst}
          onHideAfterConfirm={this.hideDatePickerFirst}
          // minimumDate={new Date()}
          datePickerModeAndroid="spinner"
          date={this.state.firstDate ? new Date(this.state.firstDate) : new Date()}
        />
        <DatePicker
          isVisible={this.state.showDatePickerEnd}
          onConfirm={this.onDatePickerConfirmedEnd}
          onCancel={this.hideDatePickerEnd}
          onHideAfterConfirm={this.hideDatePickerEnd}
          // minimumDate={new Date()}
          datePickerModeAndroid="spinner"
          date={this.state.endDate ? new Date(this.state.endDate) : new Date()}
        />
      </KeyboardAvoidingView>
    );
  }
}

const mapStateToProps = state => {
  const data_type = [
    {
      id: 0,
      name: "Meet",
    },
    {
      id: 1,
      name: "Call",
    },
    {
      id: 2,
      name: "Stay",
    }
  ];
  return {
    activities: state.activity.list,
    isFetching: state.activity.isFetching,
    isError: state.activity.isError,
    errors: state.activity.errors,
    types: data_type,
    lastPage:state.plan.lastPage,
    roleUser:state.user.profile.data[0].user.user_role
  };
};

export default connect(mapStateToProps)(ActivityList);
