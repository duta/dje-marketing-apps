import EStyleSheet from "react-native-extended-stylesheet";
import { COLOR_PRIMARY_GREY, COLOR_PRIMARY } from "../../../config/common";
import { Dimensions } from "react-native";

const styles = EStyleSheet.create({
  container: {
    alignItems: "flex-start",
    justifyContent: "flex-start",
    flexGrow: 1,
    backgroundColor: "white"
  },
  content: {
    padding: 16,
    flex: 1,
    alignSelf: "stretch",
    backgroundColor: "white"
  },
  row: {
    flexDirection: "row",
    alignSelf: "stretch",
    alignItems: "center"
  },
  header: {
    alignSelf: "stretch",
    alignItems: "center",
    justifyContent: "center",
    paddingVertical: 16,
    borderTopColor: COLOR_PRIMARY_GREY,
    borderTopWidth: 1
  },
  profilePict: {
    height: 72,
    width: 72,
    borderRadius: 36,
    marginBottom: 8,
    resizeMode: "cover"
  },
  name: {
    color: "white",
    fontSize: 16,
    fontFamily: "Arial",
    fontWeight: "bold",
    textAlign: "center",
    alignSelf: "stretch"
  },
  headerImage: {
    height: 200,
    alignSelf: "stretch",
    borderBottomWidth: 3,
    borderBottomColor: "#CCC"
  },
  attachment: {
    width: Dimensions.get("window").width / 1.1,
    height: 200,
    resizeMode: "cover"
  },
  floatButton: {
    position: "absolute",
    bottom: 16,
    right: 16,
    backgroundColor: COLOR_PRIMARY,
    height: 40,
    width: 40,
    borderRadius: 20,
    alignItems: "center",
    justifyContent: "center",
    elevation: 4
  },
  locationButton: {
    position: "relative",
    // bottom: 16,
    // right: 16,
    backgroundColor: COLOR_PRIMARY,
    height: 40,
    width: 40,
    borderRadius: 20,
    alignItems: "center",
    justifyContent: "center",
    // elevation: 4
  },
});

export default styles;
