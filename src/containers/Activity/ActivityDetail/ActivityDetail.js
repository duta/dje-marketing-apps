import React, { Component } from "react";
import { Image, View, Text,TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import { NavigationActions } from "react-navigation";
import Container from "../../../components/Container";
import containerStyles from "../../../styles/container";
import Item from "../../../components/Item";
import styles from "./styles";
import Separator from "../../../components/Separator";
import {
  COLOR_PRIMARY,
} from "../../../config/common";
import ActionButton from "../../../components/ActionButton";
import ImagePreview from "../../../components/ImagePreview";
import Icon from "react-native-vector-icons/MaterialIcons";

class ActivityDetail extends Component {
  static navigationOptions = ({ navigation }) => {
    var action = () => {};
    if (navigation.state.params.action) {
      action = navigation.state.params.action;
    }

    return {
      headerStyle: {
        elevation: 0,
        backgroundColor: COLOR_PRIMARY
      },
      headerRight: (
        <View style={{ paddingHorizontal: 16 }}>
          {/* <ActionButton icon="create" onPress={action} /> */}
        </View>
      )
    };
  };

  state = {
    previewAttachment: false
  };

  showAttachment = () => {
    this.setState({ previewAttachment: true });
  };

  hideAttachment = () => {
    this.setState({ previewAttachment: false });
  };

  componentDidMount() {
    this.props.navigation.setParams({
      action: this.navigateToEdit
    });
  }

  navigateToEdit = () => {
    this.props.dispatch(
      NavigationActions.navigate({
        routeName: "ActivityInput",
        params: {
          title: "Edit Customer",
          id: this.props.activity.id
        }
      })
    );
  };

  navigateToMaps = () => {
    this.props.dispatch(
      NavigationActions.navigate({
        routeName: "Maps",
        params: {
          title: "Maps",
          data: this.props.activity
        }
      })
    );
  };

  render() {
    const activity = this.props.activity || {};

    if (activity.images.length > 0){
      imageSource = { uri: activity.images[0].url_image };
    }else{
      imageSource = '';
    }

    var images = this.props.activity.images || [];
    if (images.length > 0) {
      images = images.map(image => image.url_image);
    }

    return (
      <Container style={[containerStyles.container, styles.container]}>
        <View style={styles.content}>
          <View style={styles.row}>
            <Item
              label="Judul"
              value={activity.title}
              style={{ flex: 1 }}
            />
            <Item
              label="Kota"
              value={activity.regency ? activity.regency.name : null}
              style={{ flex: 1 }}
            />
          </View>
          <Item
              label="Keterangan"
              value={activity.description || "-"}
            />
          {this.props.roleUser == 1 && activity.marketing &&
          <View style={{alignSelf:"flex-end"}}>
            <TouchableOpacity
              activeOpacity={0.8}
              style={styles.locationButton}
              onPress={this.navigateToMaps}
            >
              <Icon name="location-on" color="white" size={24} />
            </TouchableOpacity>
          </View>}
          
          <Separator />

          <View style={styles.row}>
          {images.length > 0 && (
            <View style={styles.headerImage}>
              <Image
                source={imageSource}
                style={styles.attachment}
              />
              <TouchableOpacity
                activeOpacity={0.8}
                style={styles.floatButton}
                onPress={this.showAttachment}
              >
                <Icon name="zoom-in" color="white" size={24} />
              </TouchableOpacity>
            </View>
          )}
          {images.length > 0 && (
            <ImagePreview
              imageSources={images}
              isVisible={this.state.previewAttachment}
              onClose={this.hideAttachment}
            />
          )}
          </View>
        </View>
      </Container>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  const activity = ownProps.navigation.state.params.data;
  return {
    activity: activity,
    roleUser:state.user.profile.data[0].user.user_role
  };
};

export default connect(mapStateToProps)(ActivityDetail);
