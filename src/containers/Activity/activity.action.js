import * as actionTypes from "./activity.actionTypes";

export function resetActivity(){
  return {
    type:actionTypes.RESET_ACTIVITY
  }
}

export function fetchActivities(params) {
  return {
    type: actionTypes.FETCH_ACTIVITIES,
    params
  };
}

export function addActivity(data) {
  return {
    type: actionTypes.ADD_ACTIVITY,
    data
  };
}

export function updateActivity(id, data) {
  return {
    type: actionTypes.UPDATE_ACTIVITY,
    id,
    data
  };
}

export function deleteActivity(id) {
  return {
    type: actionTypes.DELETE_ACTIVITY,
    id
  };
}

export function clearError() {
  return {
    type: actionTypes.ACTIVITY_CLEAR_ERROR
  };
}
