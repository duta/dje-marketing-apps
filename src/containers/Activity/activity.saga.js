import { all, call, put, takeLatest } from "redux-saga/effects";
import {
  ACTIVITIES_FETCHED,
  FETCH_ACTIVITIES_FAIL,
  FETCH_ACTIVITIES,
  ADD_ACTIVITY,
  ADD_ACTIVITY_FAIL,
  ACTIVITY_ADDED,
  ACTIVITY_UPDATED,
  UPDATE_ACTIVITY_FAIL,
  UPDATE_ACTIVITY,
  ACTIVITY_DELETED,
  DELETE_ACTIVITY_FAIL,
  DELETE_ACTIVITY,
  DELETE_MULTIPLE_ACTIVITY
} from "./activity.actionTypes";
import Api from "../../apis/api";
import { API_V3_PREFIX, API_VERSION } from "../../config/api";
import ApiUtils from "../../apis/ApiUtils";

function fetchActivities(params) {
  console.log("params",params);
  if (params.customerId) {
    return Api.get(`customer/${params.customerId}/activity`, params);
  }
  return Api.get(`activity`, params);
  // return require('../../data/activity.json');
}

function* handleFetchingActivities(action) {
  try {
    const response = yield call(fetchActivities, action.params);
    const data = response.data;

    yield put({
      type: ACTIVITIES_FETCHED,
      data
    });
  } catch (error) {
    yield put({
      type: FETCH_ACTIVITIES_FAIL,
      errors: ApiUtils.handleErrors(error)
    });
  }
}

function addActivity(data) {
  let header = {
    headers: {
      'Content-Type': 'multipart/form-data; charset=utf-8; boundary="another cool boundary";'
    }
  };

  let formData = new FormData();
  
  Object.keys(data).forEach(key => {
    if (key == "images") {
      var images = data.images;
      for (var i=0;i<images.length;i++) {
        formData.append('images[' + i + ']', images[i]);
      }
    } else if(key == "projects"){
      var projects = data.projects;
      projects.forEach(val => {
        formData.append(`${key}[]`, JSON.stringify(val));
      });
    }else{
      formData.append(key, data[key]);
    }
  });

  return Api.post("activity", formData, {}, header);
}

function* handleAddingActivity(action) {
  try {
    const response = yield call(addActivity, action.data);
    const data = response.data.data;

    yield put({
      type: FETCH_ACTIVITIES,
      params: {
        customerId:null
      }
      // type: ACTIVITY_ADDED,
      // data
    });
  } catch (error) {
    yield put({
      type: ADD_ACTIVITY_FAIL,
      errors: ApiUtils.handleErrors(error)
    });
  }
}

export default function* watchAll() {
  yield all([
    takeLatest(FETCH_ACTIVITIES, handleFetchingActivities),
    takeLatest(ADD_ACTIVITY, handleAddingActivity),
  ]);
}
