import { StyleSheet,Dimensions } from "react-native";
import { COLOR_SOFTEN_RED, COLOR_VIOLET_GREY } from "../../../config/common";

const styles = StyleSheet.create({
  form: {
    marginVertical: 16,
    alignSelf: "stretch",
    flexGrow: 1,
    padding: 16
  },
  section: {
    marginBottom: 6,
    alignSelf: "stretch"
  },
  profilePictWrapper: {
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#F2F2F2",
    borderColor: "#CCC",
    borderWidth: 1,
    height: 64,
    width: 64,
    borderRadius: 32
  },
  profilePict: {
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "rgba(0, 0, 0, 0.0)",
    height: 64,
    width: 64,
    borderRadius: 32
  },
  errorMessage: {
    color: COLOR_SOFTEN_RED,
    fontSize: 12
  },
  row: {
    flexDirection: "row",
    alignSelf: "stretch",
    alignItems: "center"
  },
  buttonContainer: {
    flex: 1,
    width: Dimensions.get("window").width / 2.5
  }
});

export default styles;
