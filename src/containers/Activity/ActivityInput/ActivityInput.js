import React, { Component } from "react";
import { Image, View, Text, StyleSheet } from "react-native";
import { connect } from "react-redux";
import Modal from "react-native-modal";
import SnackBar from "react-native-snackbar";
import { NavigationActions, StackActions } from "react-navigation";
import Container from "../../../components/Container";
import ActivityForm from "./ActivityForm";
import { addActivity, updateActivity, clearError } from "../activity.action";
import { Loading, FullScreenLoading } from "../../../components/Loading";
import { COLOR_SOFTEN_RED, COLOR_VIOLET_GREY, BUTTON_GRADIENT } from "../../../config/common";


class ActivityInput extends Component {
  timeout = null;
  state = {
    success: false
  };

  static navigationOptions = ({ navigation }) => {
    return {
      headerRight: <View />
    };
  };

  onFormSubmit = data => {
    if (this.props.activity === null) {
      this.props.dispatch(addActivity(data));
    } else {
      this.props.dispatch(updateActivity(this.props.customer.id, data));
    }
  };

  componentWillMount() {
    this.props.dispatch(clearError());
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.isUpdating && !nextProps.isUpdating) {
      if (!nextProps.isError) {
        this.setState({ success: true });
      } else {
        setTimeout(() => {
          SnackBar.show({
            title: "Tidak dapat menyimpan data.",
            duration: SnackBar.LENGTH_LONG,
            backgroundColor: COLOR_SOFTEN_RED
          });
        }, 500);
      }
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (!prevState.success && this.state.success) {
      this.timeout = setTimeout(this.dismissSuccessMessage, 1000);
    }
  }

  dismissSuccessMessage = () => {
    this.setState({ success: false });
    this.redirectToList();

    if (this.timeout) {
      clearTimeout(this.timeout);
    }
  };

  renderSuccessMessage = () => {
    var text = "Penambahan aktivitas telah berhasil";
    if (this.props.navigation.state.params.id) {
      text = "Data berhasil diperbarui";
    }

    return (
      <Modal
        isVisible={this.state.success}
        onBackButtonPress={this.dismissSuccessMessage}
        onBackdropPress={this.dismissSuccessMessage}
      >
        <View style={styles.messageWrapper}>
          <Image source={require("../../../assets/images/updated.png")} />
          <Text style={styles.message}>{text}</Text>
        </View>
      </Modal>
    );
  };

  redirectToList = () => {
    const { dispatch } = this.props;
    dispatch(NavigationActions.back());
  };

  render() {
    return (
      <Container style={{ padding: 0 }}>
        <ActivityForm
          errors={this.props.errors}
          onSubmit={this.onFormSubmit}
          activity={this.props.activity}
          isLoading={this.props.isUpdating}
          plan={this.props.plan}
        />
        
        <FullScreenLoading 
          isVisible={this.props.isUpdating}
          text="Memproses data ..."
        />
        {this.renderSuccessMessage()}
      </Container>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  const activityId = ownProps.navigation.state.params.id;
  const activity = ownProps.navigation.state.params.data;
  const plan =ownProps.navigation.state.params.plan;
  return {
    activity: activityId ? activity : null,
    isUpdating: state.activity.isUpdating,
    isError: state.activity.isError,
    errors: state.activity.errors || {},
    plan:plan
  };
};

const styles = StyleSheet.create({
  messageWrapper: {
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center",
    padding: 16,
    borderRadius: 3
  },
  message: {
    fontFamily: "Quicksand",
    color: COLOR_VIOLET_GREY,
    textAlign: "center",
    marginVertical: 16
  }
});

export default connect(mapStateToProps)(ActivityInput);
