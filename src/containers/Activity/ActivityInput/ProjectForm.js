import React, { Component } from "react";
import { StyleSheet, Text, View } from "react-native";
import PropTypes from "prop-types";
import { TextInputWithLabel } from "../../../components/TextInput";
import { COLOR_PRIMARY } from "../../../config/common";
import formStyles from "../../../styles/form";
import { connect } from "react-redux";
import { formatMoney } from "../../../utils/number";
import { formatDate } from "../../../utils/date";
import { pad } from "../../../utils/string";
import DatePicker from "react-native-modal-datetime-picker";
import { NavigationActions } from "react-navigation";

var today = new Date();
var dd = today.getDate();
var mm = today.getMonth() + 1; //January is 0!

var yyyy = today.getFullYear();
if (dd < 10) {
  dd = '0' + dd;
} 
if (mm < 10) {
  mm = '0' + mm;
} 
var today = yyyy + '-' + mm + '-' + dd  ;

class ProjectForm extends Component {
  state = {
    fields: {
      name: null,
      date: today,
      regency_id: null,
      price: 0,
    },
    showDatePicker: false,
    selectedRegency:null
  };

  static propTypes = {
    name: PropTypes.string,
    date: PropTypes.string,
    regency_id: PropTypes.number,
    price: PropTypes.number,
    onFieldsUpdated: PropTypes.func
  };

  componentDidMount() {
    this.mergePropsWithState();
    var fields = {
      ... this.state.fields,
    };

    this.setState({
      ...this.state,
      fields
    });
  }

  /**
   * Menggabungkan props dengan state
   * Jika data sebelumnya telah ada
   * atau akan melakukan perubahan data
   */
  mergePropsWithState = () => {
    const { fields } = this.state;
    var newFields = fields;

    Object.keys(fields).forEach((key, index) => {
      if (this.props[key]) {
        newFields[key] = this.props[key];
      }
    });

    this.setState({ fields: newFields });
  };

  updateField = field => value => {
    var newFields = this.state.fields;
    newFields[field] = value;
    if (field == 'price') {
      newFields[field] = value.replace(/\./g,"");
      if (!value || value == "NaN") {
        newFields[field] = parseInt(0);
      }
    }

    this.setState({ fields: newFields });
    this.onFieldsUpdated();
  };

  /**
   * Melempar data ke parent ketika terjadi perubahan
   */
  onFieldsUpdated = () => {
    if (this.props.onFieldsUpdated) {
      this.props.onFieldsUpdated(this.state.fields);
    }
  };

  showDatePicker = () => {
    this.setState({
      showDatePicker: true
    });
  };

  hideDatePicker = () => {
    this.setState({
      showDatePicker: false
    });
  };

  onDatePickerConfirmed = date => {
    const d = pad(date.getMonth(), 2);
    const selectedDate = `${date.getFullYear()}-${pad(
      date.getMonth() + 1,
      2
    )}-${pad(date.getDate(), 2)}`;

    let newFields = { ...this.state.fields };
    newFields["date"] = selectedDate;

    this.setState({
      ...this.state,
      fields: { ...newFields }
    });
  };

  selectRegeny = () => {
    const { dispatch } = this.props;

    dispatch(
      NavigationActions.navigate({
        routeName: "SelectRegency",
        params: {
          type:"single",
          callback: this.onRegencySelected
        }
      })
    );
  };

  onRegencySelected = regency => {
    this.setState({
      selectedRegency: regency,
      fields: {
        ...this.state.fields,
        regency_id: regency.id
      }
    });
  };

  render() {
    const {fields} = this.state;
    return (
      <View style={styles.form}>
        
        <View style={styles.row}>
          <TextInputWithLabel
            style={[formStyles.inputWrapper, { flex: 1, marginRight: 4 }]}
            inputStyle={formStyles.input}
            labelStyle={formStyles.inputLabel}
            placeholder="Nama project"
            label="Nama project"
            onChangeText={this.updateField("name")}
            value={fields.name}
          />
          <TextInputWithLabel
            style={[formStyles.inputWrapper, { flex: 1, marginLeft: 4 }]}
            inputStyle={formStyles.input}
            labelStyle={formStyles.inputLabel}
            label="Tanggal"
            value={
              fields.date
                ? formatDate(fields.date, "DD MMMM YYYY")
                : null
            }
            editable={false}
            onTouchStart={this.showDatePicker}
          />
        </View>
        <View style={styles.row}>
          <TextInputWithLabel
            style={[formStyles.inputWrapper, { flex: 1, marginRight: 4 }]}
            inputStyle={formStyles.input}
            labelStyle={formStyles.inputLabel}
            label="Kota/Kabupaten"
            placeholder={"Pilih kota/kabupaten"}
            editable={false}
            value={
              this.state.selectedRegency !== null
              ? this.state.selectedRegency.name
              : null}
            onTouchStart={this.selectRegeny}
          />
          <TextInputWithLabel
            style={[formStyles.inputWrapper, { flex: 1, marginLeft: 4 }]}
            inputStyle={formStyles.input}
            labelStyle={formStyles.inputLabel}
            placeholder="Harga"
            label="Harga"
            onChangeText={this.updateField("price")}
            keyboardType="numeric"
            value={formatMoney(fields.price)}
          />
        </View>
        <DatePicker
          isVisible={this.state.showDatePicker}
          onConfirm={this.onDatePickerConfirmed}
          onCancel={this.hideDatePicker}
          onHideAfterConfirm={this.hideDatePicker}
          minimumDate={new Date()}
          datePickerModeAndroid="spinner"
          date={new Date(fields.date)}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  form: {
    padding: 16,
    backgroundColor: `${COLOR_PRIMARY}33`,
    alignSelf: "stretch",
    borderRadius: 3,
    zIndex: 1,
    overflow: "visible",
    marginBottom: 8
  },
  row: {
    flexDirection: "row",
    alignSelf: "stretch",
    alignItems: "center"
  }
});

export default connect()(ProjectForm);
