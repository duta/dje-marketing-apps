import React, { Component } from "react";
import {
  Image,
  Keyboard,
  KeyboardAvoidingView,
  ScrollView,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
  Platform,
  Picker,
  Dimensions
} from "react-native";
import Icon  from "react-native-vector-icons/MaterialIcons";
import { TextInputWithLabel } from "../../../components/TextInput";
import { Select } from "../../../components/Select";
import formStyles from "../../../styles/form";
import styles from "./styles";
import { BUTTON_GRADIENT,BUTTON_GRADIENT_INVERSE, COLOR_GREY,COLOR_PRIMARY, COLOR_DARKEN_RED, COLOR_SOFTEN_RED } from "../../../config/common";
import _ from "lodash";
import DatePicker from "react-native-modal-datetime-picker";
import UploadImage from "../../../components/UploadImage";
import PropTypes from "prop-types";
import { formatDate,getToday } from "../../../utils/date";
import { clean } from "../../../utils/array";
import {
  pad,
  formatPhone,
  clearPhoneFormat,
  toTitleCase,
  filterPhoneNumber,
  getFileNameFromPath
} from "../../../utils/string";
import { Button } from "../../../components/Button";
import { NavigationActions } from "react-navigation";
import { connect } from "react-redux";
import ProjectForm from "./ProjectForm";
import SnackBar from "react-native-snackbar";

let initialState = {
  fields: {
    title: null,
    type: null,
    customer_id: null,
    description:null,
    project_id:null,
    regency_id:null,
    first_date: getToday(),
    images:[],
    latitude:null,
    longitude:null
  }
  
};

const ImageItem = props => (
  <View
    style={{marginRight: 4}}
  >
    <Image
      source={props.source}
      style={{width: 100, height: 100}}
    />
    <TouchableOpacity 
      onPress={props.onDelete}
      style={{
        position: "absolute",
        right: 2,
        top: 2,
        backgroundColor: COLOR_DARKEN_RED,
        borderRadius: 2,
        padding: 2
      }}
    >
      <Icon name="delete" size={16} color="white" />
    </TouchableOpacity>
  </View>
)

class ActivityForm extends Component {
  state = {
    fields: initialState.fields,
    showDatePicker: false,
    showImagePicker: false,
    pickAttachment: false,
    selectedRegency: null,
    selectedCustomer:null,
    selectedProject:null,
    projects:[]
  };

  static propTypes = {
    onImagePickerSelected: PropTypes.func,
    onImagePickerError: PropTypes.func
  };

  static defaultProps = {
    errors: []
  };

  initializeData = (fields) => {

    switch (fields.status) {
      case 0:
        fields["type_index"] = 0;
        break;
      case 1:
        fields["type_index"] = 1;
        break;
      case 2:
        fields["type_index"] = 2;
        break;
      case 3:
        fields["type_index"] = 3;
        break;  
      default:
        fields["type_index"] = null;
        break;
    }
    
    return fields;
  };

  componentDidMount() {
    const { activity,plan } = this.props;
    navigator.geolocation.getCurrentPosition(
      position => {
        this.setState({
          ...this.state,
          fields:{
            ...this.state.fields,
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
          }
          
        });
      },
      error => {
        console.log("error",error)
      },
    );

    var fields = {
      ... this.state.fields,
      title: plan ? plan.title : null
    };

    fields = this.initializeData(fields);

    this.setState({
      ...this.state,
      fields
    });
  }

  resetFields = () => {
    const fields = {
      title: null,
      type: null,
      customer_id: null,
      description:null,
      project_id:null,
      regency_id:null,
      first_date: getToday(),
      images:[]
    };

    initialState.fields = fields;
    this.setState({ fields });
  };

  componentWillUnmount() {
    this.resetFields();
  }

  onSubmit() {
    if (typeof this.props.onSubmit === "function") {
      const data = {
        ...this.state.fields,
        projects: this.state.projects
      };
      this.props.onSubmit(clean(data));
    }
  }

  updateTextField = field => value => {
    let newFields = { ...this.state.fields };
    newFields[field] = value;
    this.setState({
      ...this.state,
      fields: { ...newFields }
    });
  };

  updatePickerField = field => (value, index) => {
    let newFields = { ...this.state.fields };
    newFields[field] = value.value;
    newFields[field + "_index"] = index;

    this.setState({
      ...this.state,
      fields: { ...newFields }
    });
    
  };

  renderErrorMessage = field => {
    const { errors } = this.props;

    // No error
    if (typeof errors === "undefined" || typeof errors[field] === "undefined") {
      return null;
    } else {
      // Error found
      return <Text style={styles.errorMessage}> {errors[field][0]} </Text>;
    }
  };

  showDatePicker = () => {
    Keyboard.dismiss();
    this.setState({
      showDatePicker: true
    });
  };

  hideDatePicker = () => {
    this.setState({
      showDatePicker: false
    });
  };

  showImagePicker = () => {
    this.setState({
      showImagePicker: true
    });
  };

  hideImagePicker = () => {
    this.setState({
      showImagePicker: false
    });
  };

  onDatePickerConfirmed = date => {
    const d = pad(date.getMonth(), 2);
    const selectedDate = `${date.getFullYear()}-${pad(
      date.getMonth() + 1,
      2
    )}-${pad(date.getDate(), 2)}`;

    let newFields = { ...this.state.fields };
    newFields["first_date"] = selectedDate;

    this.setState({
      ...this.state,
      fields: { ...newFields }
    });
  };

  onImagePickerSelected = response => {
    // Update state
    this.setState({
      showImagePicker: false,
      fields: {
        ...this.state.fields,
        photo: {
          uri: response.path,
          name: "profile-pict.jpeg",
          type: response.mime
        }
      }
    });

    // Jalankan callback dari parent
    if (typeof this.props.onImagePickerSelected === "function") {
      this.props.onImagePickerSelected(response);
    }
  };

  onImagePickerError = response => {
    // Tutup image picker
    this.setState({ showImagePicker: false });

    // Tampilkan error
    Snackbar.show({
      title: "ImagePicker error: " + response.error,
      duration: SnackBar.LENGTH_SHORT
    });

    // Jalankan callback tambahan dari parent
    if (typeof this.props.onImagePickerError === "function") {
      this.props.onImagePickerError(response);
    }
  };

  getSubmitButtonLabel = () => {
    if (this.props.isLoading) {
      return "Menyimpan data ...";
    } else {
      return "Simpan";
    }
  };

  showAttachmentPicker = () => {
    this.setState({ pickAttachment: true });
  };

  hideAttachmentPicker = () => {
    this.setState({ pickAttachment: false });
  };

  deleteLocalImage = index => {
    var images = this.state.fields.images;
    images = images.filter((photo, i) => index !== i);

    this.updateTextField("images")(images);
  }

  renderImageList = () => {
    var images = [...this.state.fields.images];

    if (this.props.activity && this.props.activity.images.length > 0) {
      var propPhotos = this.props.activity.images;
      for(var i=0;i<propPhotos.length;i++) {
        if (propPhotos[i]) {
          images.push({
            uri: propPhotos[i].photo,
            id: propPhotos[i].id
          });
        }
      }
    }

    return (
      <ScrollView 
        style={{
          flexDirection: "row",
          alignSelf: "stretch",
          marginTop: 16
        }}
        horizontal={true}>
        {images.length < 10 && (
          <TouchableOpacity
            onPress={this.showAttachmentPicker}
            activeOpacity={0.8}
            style={{
              width: 100,
              height: 100,
              borderColor: COLOR_PRIMARY,
              borderWidth: 2,
              alignContent: "center",
              alignItems: "center",
              justifyContent: "center",
              borderRadius: 3,
              marginRight: 8
            }}
          >
            <Icon name="add-a-photo" color={COLOR_PRIMARY} size={32} />
          </TouchableOpacity>
        )}
        {images.map((photo, index) => (
          <ImageItem
            key={Math.random()}
            source={photo}
            onDelete={() => {
              if (photo.id) {
                this.deleteRemoteImage(photo.id);
              } else {
                this.deleteLocalImage(index);
              }
            }}
          />
        ))}
      </ScrollView>
    );
  }

  onImageSelected = images => {
    var selectedPhotos = images.map((image) => {
      return {
        uri: image.path,
        name: getFileNameFromPath(image.path),
        type: image.mime
      };
    });

    this.addImages(selectedPhotos);
  }

  addImages(images) {
    var photos = [
      ... this.state.fields.images,
      ... images
    ];

    if (images.length > 10) {
      SnackBar.show({
        title: "Maksimal 10 foto yang dapat disimpan.",
        duration: SnackBar.LENGTH_LONG,
        backgroundColor: COLOR_SOFTEN_RED
      });
    }

    photos  = photos.filter((photo, index) => {
      return index < 10;
    });

    this.updateTextField("images")(photos);
    this.hideAttachmentPicker();
    
  }

  selectRegency = () => {
    const { dispatch } = this.props;

    dispatch(
      NavigationActions.navigate({
        routeName: "SelectRegency",
        params: {
          type:"single",
          callback: this.onRegencySelected
        }
      })
    );
  };

  onRegencySelected = regency => {
    const fields = {
      ...this.state.fields,
      regency_id:regency.id
    }
    this.setState({
      selectedRegency: regency,
      fields:fields
    });
  };

  selectCustomer = () => {
    const { dispatch } = this.props;

    dispatch(
      NavigationActions.navigate({
        routeName: "CustomerSelect",
        params: {
          type:"single",
          callback: this.onCustomerSelected
        }
      })
    );
  };

  onCustomerSelected = customer => {
    const fields = {
      ...this.state.fields,
      customer_id:customer.id
    }
    this.setState({
      selectedCustomer: customer,
      fields:fields
    });
  };

  selectProject = () => {
    const { dispatch } = this.props;

    dispatch(
      NavigationActions.navigate({
        routeName: "ProjectSelect",
        params: {
          type:"single",
          callback: this.onProjectSelected
        }
      })
    );
  };

  onProjectSelected = project => {
    const fields = {
      ...this.state.fields,
      project_id:project.id
    }
    this.setState({
      selectedProject: project,
      fields:fields
    });
  };

  updateProject = (project, index) => {
    var newProjects = this.state.projects;
    newProjects[index] = project;

    this.setState({ projects: newProjects });
  };

  renderProjectList = () => {
    if (this.state.projects.length === 0) return null;

    const list = this.state.projects.map((project, index) => {
      return (
        <ProjectForm
          key={`project-${index}`}
          {...project}
          onFieldsUpdated={data => this.updateProject(data, index)}
        />
      );
    });

    return list;
  };

  addProject = () => {
    var projects = this.state.projects;
    projects.push({
      name: null,
      date: null,
      regency_id: null,
      marketing_id: null,
      price: null
    });

    this.setState({ projects });
  };

  render() {
    const { fields } = this.state;
    const props = this.props;
    
    return (
      <KeyboardAvoidingView behavior="padding" style={{flex: 1, alignSelf: "stretch"}}>
        <ScrollView style={styles.form} keyboardShouldPersistTaps="always">
          <View style={styles.section}>
            <TextInputWithLabel
              label="Judul"
              style={formStyles.inputWrapper}
              inputStyle={formStyles.input}
              labelStyle={formStyles.inputLabel}
              onChangeText={this.updateTextField("title")}
              value={fields.title}
              autoCapitalize="words"
            />
            {this.renderErrorMessage("title")}
          </View>
          <View style={styles.section}>
            <Select
              label="Tipe Aktivitas"
              title="Pilih Tipe Aktivitas"
              style={[formStyles.selectWrapper]}
              inputStyle={formStyles.select}
              labelStyle={formStyles.inputLabel}
              selectedIndex={fields.type_index}
              onItemSelected={this.updatePickerField("type")}
              items={[
                { label: "Meet", value: 0 },
                { label: "Call", value: 1 },
                { label: "Stay", value: 2 },
              ]}
            />
            {this.renderErrorMessage("type")}
          </View>
          <View style={styles.section}>
            <TextInputWithLabel
              label="Tanggal"
              style={formStyles.inputWrapper}
              inputStyle={formStyles.input}
              labelStyle={formStyles.inputLabel}
              value={
                fields.first_date
                  ? formatDate(fields.first_date, "DD MMMM YYYY")
                  : null
              }
              editable={false}
              onTouchStart={this.showDatePicker}
            />
            {this.renderErrorMessage("first_date")}
          </View>
          <View style={styles.section}>
            <TextInputWithLabel
              label="Kota"
              placeholder={"Pilih kota"}
              style={formStyles.inputWrapper}
              inputStyle={[formStyles.input, { textAlignVertical: "center" }]}
              labelStyle={formStyles.inputLabel}
              editable={false}
              value={
                this.state.selectedRegency !== null
                ? this.state.selectedRegency.name
                : null}
              onTouchStart={this.selectRegency}
            />
            {this.renderErrorMessage("regency_id")}
          </View>
          <View style={styles.section}>
            <TextInputWithLabel
              label="Customer"
              placeholder={"Pilih customer"}
              style={formStyles.inputWrapper}
              inputStyle={[formStyles.input, { textAlignVertical: "center" }]}
              labelStyle={formStyles.inputLabel}
              editable={false}
              value={
                this.state.selectedCustomer !== null
                ? this.state.selectedCustomer.name
                : null}
              onTouchStart={this.selectCustomer}
            />
            {this.renderErrorMessage("customer_id")}
          </View>
          <View style={styles.section}>
            <TextInputWithLabel
              label="Project"
              placeholder={"Pilih project"}
              style={formStyles.inputWrapper}
              inputStyle={[formStyles.input, { textAlignVertical: "center" }]}
              labelStyle={formStyles.inputLabel}
              editable={false}
              value={
                this.state.selectedProject !== null
                ? this.state.selectedProject.name
                : null}
              onTouchStart={this.selectProject}
            />
            {this.renderErrorMessage("customer_id")}
          </View>
          <View style={styles.section}>
            <TextInputWithLabel
              label="Keterangan"
              style={formStyles.inputWrapper}
              inputStyle={[formStyles.input, { textAlignVertical: "top" }]}
              labelStyle={formStyles.inputLabel}
              multiline={true}
              onChangeText={this.updateTextField("description")}
              value={fields.description}
              autoCapitalize="words"
            />
            {this.renderErrorMessage("description")}
          </View>

          <DatePicker
            isVisible={this.state.showDatePicker}
            onConfirm={this.onDatePickerConfirmed}
            onCancel={this.hideDatePicker}
            onHideAfterConfirm={this.hideDatePicker}
            minimumDate={new Date()}
            datePickerModeAndroid="spinner"
            date={new Date(fields.first_date)}
          />
          <View style={styles.section}>
            <UploadImage
              isVisible={this.state.pickAttachment}
              onImageSelected={this.onImageSelected}
              onClose={this.hideAttachmentPicker}
              onError={error => {
                console.log('Error', error);
              }}
              enable={false}
              cropping={false}
              multiple={true}
              compressImageQuality={0.4}
            />
            {this.renderImageList()}
          </View>
          {this.renderProjectList()}
          <View style={styles.row}>
          <Button
            gradient={BUTTON_GRADIENT_INVERSE}
            gradientDirection="horizontal"
            label="Tambah Project"
            style={formStyles.button}
            onPress={this.addProject}
            containerStyle={[styles.buttonContainer,{marginRight:5}]}
          />
          <Button
            label={this.getSubmitButtonLabel()}
            style={formStyles.button}
            gradient={BUTTON_GRADIENT}
            gradientDirection="horizontal"
            disabled={this.props.isDisabled || this.props.isLoading}
            onPress={() => this.onSubmit()}
            containerStyle={[styles.buttonContainer,{marginLeft:5}]}
          />
          </View>
          
          
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}

export default connect()(ActivityForm);
