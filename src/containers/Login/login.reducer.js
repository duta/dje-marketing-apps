/**
 * ------------------------------------------------------------------------------------
 * Login Reducers
 * ------------------------------------------------------------------------------------
 *
 */

import * as actions from "./login.actions";
import { persistReducer } from "redux-persist";
import FilesystemStorage from "redux-persist-filesystem-storage";
import { RESET_STATE } from "../../config/reducer.action";

const initialState = {
  email: null,
  isLoggingIn: false,
  isLoggedIn: false,
  errors: [],
  token: {},
  fcmToken: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.LOGIN_FORM_SUBMITTED:
      return {
        ...state,
        isLoggingIn: true
      };
    case actions.LOGIN_IS_FAILED:
      return {
        ...state,
        isLoggingIn: false,
        errors: action.error
      };
    case actions.LOGIN_IS_SUCCESS:
      return {
        ...state,
        isLoggingIn: false,
        isLoggedIn: true,
        errors: [],
        token: action.data
      };
    case actions.RESTART_STATE:
      return {
        ...initialState
      };
    case actions.STORE_FCM_TOKEN:
      return {
        ...state,
        fcmToken: action.token
      };
    case RESET_STATE:
      return initialState;
    default:
      return state;
  }
};

const authPersistConfig = {
  key: "auth",
  storage: FilesystemStorage,
  blacklist: ["isLoggingIn", "errors", "email"]
};

export default persistReducer(authPersistConfig, reducer);
