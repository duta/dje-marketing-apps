import {StyleSheet} from 'react-native';
import { COLOR_PRIMARY, COLOR_VIOLET_GREY } from "../../config/common";
import EStyleSheet from "react-native-extended-stylesheet";

const styles = EStyleSheet.create({
    form: {
        alignSelf: "stretch",
        marginTop: 48
    },
    button: {
        borderRadius: 20,
        flex: 1
    },
    buttonContainer: {
        height: 38,
        elevation: 4,
        borderRadius: 20
    },
    container : {
        alignItems: "center",
        justifyContent: "center",
        paddingHorizontal: 30,
        flex: 1
    },
    signupTextCont : {
        flexGrow: 1,
        alignItems:'flex-end',
        justifyContent :'center',
        paddingVertical:10,
        flexDirection:'row'
    },
    signupText: {
        color:'rgba(255,255,255,0.6)',
        fontSize:16
    },
    signupButton: {
        color:'#ffffff',
        fontSize:16,
        fontWeight:'500'
    },
    inputBox: {
        width:300,
        backgroundColor:'rgba(255, 255,255,0.2)',
        borderRadius: 25,
        paddingHorizontal:16,
        fontSize:16,
        color:'#ffffff',
        marginVertical: 10
    },
    
    input: {
        backgroundColor: "white",
        alignSelf: "stretch",
        lineHeight: 16,
        borderRadius: 20,
    },
    inputWrapper: {
        paddingHorizontal: 8,
        borderColor: COLOR_PRIMARY,
        borderBottomColor: COLOR_PRIMARY,
        borderWidth: 1,
        borderRadius: 20,
        marginBottom: 12,
        backgroundColor: "white"
    },
});

export default styles;