/**
 * ------------------------------------------------------------------------------------
 * Login Action Creator
 * ------------------------------------------------------------------------------------
 *
 */

import Api from "../../apis/api";
import Config from "react-native-config";
import querystring from "query-string";

export const LOGIN_FORM_SUBMITTED = "LOGIN_FORM_SUBMITTED";
export const LOGIN_IS_SUCCESS = "LOGIN_IS_SUCCESS";
export const LOGIN_IS_FAILED = "LOGIN_IS_FAILED";
export const RESTART_STATE = "RESTART_STATE";

export const DO_LOGOUT = "DO_LOGOUT";

export const STORE_FCM_TOKEN = "STORE_FCM_TOKEN";
export const FCM_TOKEN_STORED = "FCM_TOKEN_STORED";
export const STORE_FCM_ERROR = "STORE_FCM_ERROR";

/**
 * On login form submitted
 * @param {*} data
 */
export function loginFormSubmitted(data) {
  return {
    type: LOGIN_FORM_SUBMITTED,
    data
  };
}
/**
 * On login success
 * @param {*} userData
 */
export function loginSuccess(userData) {
  return {
    type: LOGIN_IS_SUCCESS,
    userData
  };
}
/**
 * On login failed
 * @param {*} errors
 */
export function loginFailed(errors) {
  return {
    type: LOGIN_IS_FAILED,
    errors
  };
}
/**
 * Restart state
 * @param {*} errors
 */
export function restartState() {
  return {
    type: RESTART_STATE
  };
}

/**
 * Do Logout
 *
 * @export
 * @returns
 */
export function doLogout() {
  return {
    type: DO_LOGOUT
  };
}

/**
 * Requesting login to web server
 * @param {*} data
 */
export function submitForm(data) {
  data.grant_type = "password";
  data.client_id = Config.DJE_CLIENT_ID;
  data.client_secret = Config.DJE_CLIENT_SECRET;

  return Api.post(
    "oauth/token",
    JSON.stringify(data),
    {},
    {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    false,
    ""
  );
}

/**
 * Store FCM Token
 * @param {*} token
 */
export function storeFcmToken(token) {
  return {
    type: STORE_FCM_TOKEN,
    token
  };
}
