import React, { Component } from 'react';
import {StyleSheet,Text,View,StatusBar,TouchableOpacity} from 'react-native';
import Logo from '../../components/Logo';
import {Actions} from 'react-native-router-flux';
import styles from './style';
import { TextInput } from "../../components/TextInput";
import { BUTTON_GRADIENT, COLOR_PRIMARY } from "../../config/common";
import containerStyle from "../../styles/container";
import Container from "../../components/Container";
import Background from "../../components/Background";
import { Button } from "../../components/Button";
import { NavigationActions, StackActions } from "react-navigation";
import { connect } from "react-redux";
import * as actions from "./login.actions";
import { Alert } from "../../components/Alert";
import Api from "../../apis/api";
import firebase from 'react-native-firebase';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: null,
            password: null,
            categories:[]
        };
    }

    /**
     * Rendering error message
     *
     */
    _renderErrorMessages() {
        // Error yg berupa string berarti network error, tampilkan dengan snackbar
        if (typeof this.props.errors === "string") {
        return (
            <Alert type="error" text={"Username atau password tidak sesuai"} style={styles.alert} />
        );
        }

        let errors = this.props.errors || [];
        if (errors.length === 0) return null;

        return <Alert type="error" text={errors[0].message} style={styles.alert} />;
    }

    /**
     * Handling login button pressed
     *
     */
    _onLoginButtonPressed = () => {
        let userData = {
        username: this.state.email,
        password: this.state.password
        };

        this.props.dispatch(actions.loginFormSubmitted(userData));
    };

    /**
     * Component wiil receive props
     * For checking login status
     *
     */
    componentDidUpdate(prevProps) {
        const { dispatch, navigation, isLoggedIn, user } = this.props;
        if (isLoggedIn && prevProps.isLoggingIn) {
            const resetToHome = StackActions.reset({
                index: 0,
                actions: [
                NavigationActions.navigate({
                    routeName: "Home"
                })
                ]
            });

            firebase.messaging().hasPermission()
            .then(enabled => {
                if (enabled) {
                    firebase.messaging().getToken()
                    .then(fcmToken => {
                        if (fcmToken) {
                            this.props.dispatch(actions.storeFcmToken(fcmToken));
                            console.log("fcmToken",fcmToken)
                        } else {
                            console.log("fcmToken")
                        } 
                    });
                } else {
                    try {
                        firebase.messaging().requestPermission();
                    } catch (error) {
                        console.log("error")
                    }
                }
            });
            dispatch(navigation.dispatch(resetToHome));
        }
    }

    /**
     * Component Will Mount
     * Checking login status
     *
     */
    componentWillMount() {
        const { dispatch, navigation, isLoggedIn, token, user } = this.props;

        // Redirect setelah login
        if (isLoggedIn && token !== null) {
            const resetToHome = StackActions.reset({
                index: 0,
                actions: [
                NavigationActions.navigate({
                    routeName: "Home"
                })
                ]
            });

            // Set API token
            Api.setAuthToken(token);

            // Api.get("users/check-token", {}, {}, true, API_V3_PREFIX)
            //     .catch(error => {
            //     const errorResponse = error.response;
            //     if (errorResponse && errorResponse.status === 401) {
            //         alert("Sesi Anda telah habis, silahkan login kembali.");
            //         dispatch(actions.restartState());
            //     }
            //     })
            //     .then(() => {});

            dispatch(navigation.dispatch(resetToHome));
        }
        // Restart login state
        else {
            dispatch(actions.restartState());
        }
    }
    
    getLoginButtonLabel = () => {
        if (this.props.isLoggingIn) {
          return "Proses login ...";
        } else {
          return "Login";
        }
    };

    render() {
        return(
            <Container style={[containerStyle.container]}>
                <Background containerStyle={styles.container}>
                <Logo/>
                <View style={styles.form}>
                    {/** Rendering error messages */}
                    {this._renderErrorMessages()}
                    <TextInput
                        style={styles.input}
                        wrapperStyle={styles.inputWrapper}
                        icon="mail"
                        placeholder="Email"
                        keyboardType="email-address"
                        onChangeText={email =>
                            this.setState({
                            email: email
                            })
                        }
                        autoCapitalize="none"
                    />

                    <TextInput
                        style={styles.input}
                        wrapperStyle={styles.inputWrapper}
                        icon="lock"
                        placeholder="Password"
                        onChangeText={password =>
                            this.setState({
                            password: password
                            })
                        }
                        type="password"
                        iconColor={COLOR_PRIMARY}
                        autoCapitalize="none"
                    />
                    <Button
                        label={this.getLoginButtonLabel()}
                        style={styles.button}
                        containerStyle={styles.buttonContainer}
                        gradient={BUTTON_GRADIENT}
                        gradientDirection="horizontal"
                        onPress={this._onLoginButtonPressed}
                        disabled={this.props.isLoggingIn}
                    />
                </View>
                <View style={styles.signupTextCont} />
                </Background>
            </Container>
        )
    }
}

const mapStateToProps = state => {
    return {
      errors: state.auth.errors,
      isLoggingIn: state.auth.isLoggingIn,
      isLoggedIn: state.auth.isLoggedIn,
      token: state.auth.token,
      user: state.user
    };
  };
  
  export default connect(mapStateToProps)(Login);

