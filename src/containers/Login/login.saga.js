import { all, call, put, takeLatest } from "redux-saga/effects";
import ApiUtils from "../../apis/ApiUtils";
import Api from "../../apis/api";
import { logout as deviceLogout } from "../../config/reducer.action";
import * as actions from "./login.actions";
import { PROFILE_FETCHED } from "../Profile/profile.action";
import * as action_provincies from "../SelectProvince/select_province.action";
import * as action_regencies from "../SelectRegency/select_regency.action";

function getUserInfo() {
  return Api.get("marketing", { type: "profile" });
  // return "data"
}

/**
 * submitLoginForm
 * Handle every LOGIN_FORM_SUBMITTED is fired
 * @param {*} action
 */
function* submitLoginForm(action) {
  try {
    // send requet to api
    const response = yield call(actions.submitForm, action.data);
    // On login success
    const data = response.data;
    // Update api auth token
    Api.setAuthToken(data);

    const getUserRequest = yield call(getUserInfo);
    const userData = getUserRequest.data;
    
    // Menyimpan data profile
    yield put({
      type: PROFILE_FETCHED,
      data: userData
    });

    // Mengirim action login berhasil
    yield put({
      type: actions.LOGIN_IS_SUCCESS,
      data: data
    });

    // fetch data provinsi
    yield put({
      type: action_provincies.PROVINCES_FETCH
    });

    // fetch data kabupaten/kota
    yield put({
      type: action_regencies.REGENCIES_FETCH
    });
    
  } catch (err) {

    const error =
      err.response === undefined
        ? "Terjadi error yang tidak diketahui!"
        : err.response.data.message;
    yield put({
      type: actions.LOGIN_IS_FAILED,
      error
    });
  }
}

function storeFcmToken(token) {
  return Api.post("fcm-token", { fcm_token: token });
}

function* handleStoreFcmToken(action) {
  try {
    const response = yield call(storeFcmToken, action.token);
    yield put({
      type: actions.FCM_TOKEN_STORED
    });
  } catch (error) {
    yield put({
      type: actions.STORE_FCM_ERROR,
      errors: ApiUtils.handleErrors(error)
    });
  }
}

function logout() {
  return Api.get("logout");
}

function* handleLogout(action) {
  try {
    const reponse = yield call(logout);
    yield put(deviceLogout());
  } catch (error) {
    yield put(deviceLogout());  
  }
}

export default function* watchAll() {
  yield all([
    takeLatest(actions.LOGIN_FORM_SUBMITTED, submitLoginForm),
    takeLatest(actions.STORE_FCM_TOKEN, handleStoreFcmToken),
    takeLatest(actions.DO_LOGOUT, handleLogout)
  ]);
}
