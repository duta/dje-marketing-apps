import EStyleSheet from "react-native-extended-stylesheet";

const containerStyle = EStyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "$bgColor",
    padding: 0
  },
  row: {
    flexDirection: "row",
    alignSelf: "stretch"
  }
});

export default containerStyle;
