import EStyleSheet from "react-native-extended-stylesheet";
import { Dimensions } from "react-native";
import { COLOR_PRIMARY } from "../config/common";

const styles = EStyleSheet.create({
  modal: {
    padding: 0,
    margin: 0
  },
  wrapper: {
    backgroundColor: "white",
    position: "absolute",
    bottom: 0,
    left: 0,
    width: Dimensions.get("window").width
  },
  header: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    alignSelf: "stretch",
    paddingHorizontal: 16,
    paddingVertical: 8,
    borderBottomColor: "#CCC",
    borderBottomWidth: 1,
    marginBottom: 8
  },
  title: {
    fontWeight: "bold",
    fontSize: 15,
    fontFamily: "Arial",
    color: "rgba(117, 123, 149, 0.8)"
  },
  closeButton: {
    fontWeight: "bold",
    fontSize: 15,
    fontFamily: "Arial",
    color: COLOR_PRIMARY
  },
  form: {
    padding: 16
  },
  formSection: {
    marginBottom: 16
  },
  formContent: {
    paddingTop: 8
  },
  subTitle: {
    fontWeight: "bold",
    color: "rgba(117, 123, 149, 0.7)"
  },
  doctorCategories: {
    flexWrap: "wrap",
    alignItems: "flex-start",
    flexDirection: "row",
    alignSelf: "stretch"
  },
  customerType: {
    backgroundColor: "#CCC",
    color: "#666",
    paddingVertical: 6,
    paddingHorizontal: 12,
    borderRadius: 6,
    fontFamily: "Arial",
    fontSize: 12,
    marginRight: 6,
    marginBottom: 6
  },
  cutomerTypeSelected: {
    backgroundColor: "#d4edda",
    color: "#155724"
  },
  scheduleHour: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  time: {
    fontFamily: "Quicksand",
    fontSize: 13
  },
  textInput: {
    backgroundColor: "#F2F2F2",
    borderColor: "#CCC",
    borderWidth: 1,
    borderBottomWidth: 1,
    borderBottomColor: "#CCC",
    borderRadius: 3
  },
  footer: {
    flexDirection: "row",
    alignSelf: "stretch",
    backgroundColor: "#FAFAFA",
    flex: 1,
    paddingHorizontal: 16,
    paddingVertical: 8
  },
  button: {
    flex: 1,
    height: 38,
    alignSelf: "stretch"
  },
  dateRange: {
    flex:1,
    alignSelf: "stretch",
    flexDirection: "row",
  },
  date:{
    flex:1,
    alignSelf: "stretch"
  }
});

export default styles;
