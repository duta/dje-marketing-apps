import EStyleSheet from "react-native-extended-stylesheet";

const formStyles = EStyleSheet.create({
  button: {
    height: 36,
    alignSelf: "stretch"
  },
  inputWrapper: {
    padding: 0,
    marginBottom: 16,
    alignSelf: "stretch"
  },
  input: {
    padding: 0,
    fontSize: 14,
    paddingBottom: 0,
    lineHeight: 15,
    fontFamily: "Arial"
  },
  inputLabel: {
    fontSize: 12,
    marginBottom: 4,
    fontFamily: "Quicksand"
  },
  selectWrapper: {
    marginBottom: 16,
    alignSelf: "stretch"
  },
  select: {
    padding: 0,
    height: 28,
    borderBottomWidth: 1,
    borderBottomColor: "#CCCCCC"
  },
  selectText: {
    fontSize: 12
  }
});

export default formStyles;
