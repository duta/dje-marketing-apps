import { all } from "redux-saga/effects";
import { saga as loginSaga } from "../containers/Login";
import { saga as customerSaga } from "../containers/Customer";
import { saga as marketingSaga } from "../containers/Marketing";
import { saga as activitySaga } from "../containers/Activity";
import { saga as planSaga } from "../containers/Plan";
import { saga as projectSaga } from "../containers/Project";
import { saga as regencySaga } from "../containers/SelectRegency";
import { saga as provincesSaga } from "../containers/SelectProvince";
import { saga as userSaga } from "../containers/Profile";
import { saga as notificationSaga } from "../containers/Notification";

export default function* rootSaga() {
  yield all([
    loginSaga(),
    customerSaga(),
    marketingSaga(),
    activitySaga(),
    planSaga(),
    projectSaga(),
    regencySaga,
    provincesSaga,
    notificationSaga(),
    userSaga
  ]);
}
