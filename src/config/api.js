import Config from "react-native-config";

export const API_ROOT = `${Config.API_ENDPOINT}`;
export const API_PREFIX = `/api`;
