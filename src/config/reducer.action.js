export const RESET_STATE = "RESET_STATE";

export const logout = () => ({ type: RESET_STATE });
