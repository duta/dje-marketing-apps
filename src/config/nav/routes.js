import React from "react";
import { createStackNavigator, createTabNavigator } from "react-navigation";
import { Image, View } from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";

import Login from "../../containers/Login/Login";
import Dashboard from "../../containers/Home/Dashboard";
import Help from "../../containers/Help";
import Profile from "../../containers/Profile";
import { COLOR_PRIMARY } from "../../config/common";
import TabBar from "../../components/TabBar/TabBar";
import {
  CustomerList,
  CustomerDetail,
  CustomerInput
} from "../../containers/Customer";
import {
  MarketingList,
  MarketingDetail,
  MarketingInput
} from "../../containers/Marketing";
import {
  ActivityList,
  ActivityDetail,
  ActivityInput
} from "../../containers/Activity";
import {
  PlanList,
  PlanDetail,
  PlanInput
} from "../../containers/Plan";
import {
  ProjectList,
  ProjectDetail,
  ProjectInput
} from "../../containers/Project";
import ProjectSelect from "../../containers/Project/ProjectSelect";
import SelectRegency from "../../containers/SelectRegency";
import SelectProvince from "../../containers/SelectProvince";
import CustomerSelect from "../../containers/Customer/CustomerSelect";
import MarketingSelect from "../../containers/Marketing/MarketingSelect";
import Maps from "../../containers/Maps";
import EditAccount from "../../containers/Profile/EditAccount";
import Notification from "../../containers/Notification";

const tabRoutes = {
  Notification: {
    target: "Notification"
  },
  Order: {
    target: "Category"
  },
  Profile: {
    target: "Profile"
  },
  Help: {
    target: "Help"
  }
};

const HomeNavigator = createTabNavigator(
  {
    Home: {
      screen: Dashboard,
      navigationOptions: {
        tabBarLabel: "Beranda"
      }
    },
    Notification: {
      screen: View,
      navigationOptions: {
        tabBarLabel: "Notifikasi",
        // tabBarIcon: ({ tintColor }) => (
        //   <Image
        //     source={require("../../assets/icon/tab-bottom/notif1.png")}
        //     style={[{ tintColor: tintColor, height: 24, width: 24 }]}
        //   />
        // ) 
      }
    },
    Profile: {
      screen: View,
      navigationOptions: {
        tabBarLabel: "Profil",
        tabBarIcon: ({ tintColor }) => (
          <Image
            source={require("../../assets/icon/tab-bottom/profile.png")}
            style={[{ tintColor: tintColor, height: 24, width: 24 }]}
          />
        )
      }
    },
    Help: {
      screen: View,
      navigationOptions: {
        tabBarLabel: "Bantuan",
        tabBarIcon: ({ tintColor }) => (
          <Image
            source={require("../../assets/icon/tab-bottom/help.png")}
            style={[{ tintColor: tintColor, height: 24, width: 24 }]}
          />
        )
      }
    }
  },
  {
    tabBarPosition: "bottom",
    tabBarComponent: TabBar,
    swipeEnabled: false, 
    tabBarOptions: {
      showIcon: true, 
      activeTintColor: COLOR_PRIMARY,
      inactiveTintColor: "#757B95",
      labelStyle: {
        fontSize: 12,
        margin: 0,
        padding: 0,
        fontFamily: "Arial",
        textAlign: "center"
      },
      style: {
        height: 55,
        backgroundColor: "#FFF",
        paddingBottom: 3
      }
    }
  }
);

const configureRootNavigator = () => {
  const routes = {
    Login: {
      screen: Login,
      navigationOptions: {
        header: null
      }
    },
    Home: {
      screen: ({ navigation }) => (
        <HomeNavigator
          screenProps={{ rootNavigation: navigation, routes: tabRoutes }}
        />
      ),
      navigationOptions: ({ navigation }) => {
        return {
          title: "Beranda"
        };
      }
    },
    Customer: {
      screen: CustomerList,
      navigationOptions: {
        title: "Customer"
      }
    },
    CustomerDetail: {
      screen: CustomerDetail,
      navigationOptions: {
        title: "Detail Customer"
      }
    },
    CustomerInput: {
      screen: CustomerInput,
      navigationOptions: ({ navigation }) => ({
        title: navigation.state.params
          ? navigation.state.params.title
          : "Input Customer"
      })
    },
    Marketing: {
      screen: MarketingList,
      navigationOptions: {
        title: "Marketing"
      }
    },
    MarketingDetail: {
      screen: MarketingDetail,
      navigationOptions: {
        title: "Detail Marketing"
      }
    },
    MarketingInput: {
      screen: MarketingInput,
      navigationOptions: ({ navigation }) => ({
        title: navigation.state.params
          ? navigation.state.params.title
          : "Input Marketing"
      })
    },
    Activity: {
      screen: ActivityList,
      navigationOptions: {
        title: "Aktivitas"
      }
    },
    ActivityDetail: {
      screen: ActivityDetail,
      navigationOptions: {
        title: "Detail Aktivitas"
      }
    },
    ActivityInput: {
      screen: ActivityInput,
      navigationOptions: ({ navigation }) => ({
        title: navigation.state.params
          ? navigation.state.params.title
          : "Input Aktivitas"
      })
    },
    Plan: {
      screen: PlanList,
      navigationOptions: {
        title: "Rencana"
      }
    },
    PlanDetail: {
      screen: PlanDetail,
      navigationOptions: {
        title: "Detail Rencana"
      }
    },
    PlanInput: {
      screen: PlanInput,
      navigationOptions: ({ navigation }) => ({
        title: navigation.state.params
          ? navigation.state.params.title
          : "Input Rencane"
      })
    },
    CustomerSelect: {
      screen: CustomerSelect,
      navigationOptions: {
        title: "Pilih Customer"
      }
    },
    MarketingSelect: {
      screen: MarketingSelect,
      navigationOptions: {
        title: "Pilih Marketing"
      }
    },
    Project: {
      screen: ProjectList,
      navigationOptions: {
        title: "Project"
      }
    },
    ProjectDetail: {
      screen: ProjectDetail,
      navigationOptions: {
        title: "Detail Project"
      }
    },
    ProjectInput: {
      screen: ProjectInput,
      navigationOptions: ({ navigation }) => ({
        title: navigation.state.params
          ? navigation.state.params.title
          : "Input Project"
      })
    },
    ProjectSelect: {
      screen: ProjectSelect,
      navigationOptions: {
        title: "Pilih Project"
      }
    },
    SelectRegency: {
      screen: SelectRegency,
      navigationOptions: {
        title: "Pilih Kota/Kabupaten"
      }
    },
    SelectProvince: {
      screen: SelectProvince
    },
    Profile: {
      screen: Profile,
      navigationOptions: {
        title: "Profil"
      }
    },
    Maps: {
      screen: Maps,
      navigationOptions: {
        title: "Maps"
      }
    },
    EditAccount: {
      screen: EditAccount,
      navigationOptions: {
        title: "Edit Akun"
      },
    },
    Notification: {
      screen: Notification,
      navigationOptions: {
        title: "Notifikasi"
      }
    },
    Help: {
      screen: Help,
      navigationOptions: {
        title: "Bantuan"
      }
    },
  };

  return createStackNavigator(routes, {
    mode: "modal",
    navigationOptions: ({ navigation }) => ({
      headerStyle: {
        backgroundColor: COLOR_PRIMARY
      },
      headerTitleStyle: {
        fontWeight: "500",
        fontSize: 17,
        color: "white",
        alignSelf: "center",
        fontFamily: "Arial",
        textAlign: "center",
        flex: 1
      },
      headerTintColor: "white"
    })
  });
};

export default configureRootNavigator;
