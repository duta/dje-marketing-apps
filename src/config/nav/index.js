import React, { Component } from "react";
import { BackHandler } from "react-native";
import { StackNavigator, NavigationActions } from "react-navigation";
import {
  createNavigationReducer,
  reduxifyNavigator,
  createReactNavigationReduxMiddleware
} from "react-navigation-redux-helpers";
import { connect } from "react-redux";
import configureRootNavigator from "./routes";
import SnackBar from "react-native-snackbar";

const AppNavigator = configureRootNavigator();
const reducer = createNavigationReducer(AppNavigator);
const navMiddleware = createReactNavigationReduxMiddleware(
  "root",
  state => state.nav
);

const Navigation = reduxifyNavigator(AppNavigator, "root");

class AppNavigation extends Navigation {
  timer = 0;
  interval = null;
  counter = 0;

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
  }

  componentWillUpdate() {
    BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
  }

  componentWillUnmount() {
    this.resetBackButtonTimer();
  }

  onBackPress = () => {
    console.log("CallingOnBackPress");
    const { dispatch, state } = this.props;
    if (state.index === 0) {
      this.counter++;

      if (this.counter >= 2 && this.timer <= 5000) {
        return false;
      } else {
        this.countBackButtonTimer();
        SnackBar.show({
          title: "Tekan tombol back 2 kali untuk keluar dari aplikasi.",
          duration: SnackBar.LENGTH_SHORT
        });
        return true;
      }
    }

    dispatch(NavigationActions.back());
    return true;
  };

  countBackButtonTimer = () => {
    this.interval = setInterval(() => {
      this.timer++;

      if (this.timer > 5000) {
        this.resetBackButtonTimer();
      }
    }, 1);
  };

  resetBackButtonTimer = () => {
    clearInterval(this.interval);
    this.interval = null;
    this.timer = 0;
    this.counter = 0;
  };
}

const mapStateToProps = state => {
  return {
    state: state.nav
  };
};

export { reducer, navMiddleware };

export default connect(mapStateToProps)(AppNavigation);
// export default AppNavigation;
