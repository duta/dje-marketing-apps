// Color
export const COLOR_PRIMARY = "#EA9266";
export const COLOR_SECONDARY = "#ED4F00";
export const COLOR_INPUT_BORDER_NORMAL = "#828388";
export const COLOR_INPUT_BORDER_FOCUS = COLOR_PRIMARY;
export const BG_COLOR = "#EEF5FC";
export const COLOR_GREY = "#828388";
export const COLOR_VIOLET_GREY = "#757B95";
export const COLOR_SOFTEN_RED = "#ED6E6E";
export const COLOR_DARKEN_RED = "#C03647";
export const COLOR_ORANGE = "#F5A623";

export const COLOR_PRIMARY_GREY = "rgba(126, 126, 126, 0.7)";

// Font Size
export const FONT_SIZE_LARGE = 32;
export const FONT_SIZE_MEDIUM = 24;
export const FONT_SIZE_SMALL = 12;

export const BUTTON_GRADIENT = [COLOR_PRIMARY, COLOR_SECONDARY];
export const BUTTON_GRADIENT_INVERSE = [COLOR_SECONDARY,COLOR_PRIMARY];
export const HEADER_GRADIENT = [COLOR_PRIMARY, "#FF8F71"];
export const BUTTON_GRADIENT_GREY = [
  COLOR_PRIMARY_GREY,
  "rgba(146, 146, 146, 0.7)"
];

export const CATEGORIES_ROW_COUNT = 3;
