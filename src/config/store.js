import { createStore, applyMiddleware } from "redux";
import logger from "redux-logger";
import createSagaMiddleware from "redux-saga";
import { createTransform, persistStore, persistReducer } from "redux-persist";
import FilesystemStorage from "redux-persist-filesystem-storage";
import createEncryptor from "redux-persist-transform-encrypt";
import Config from "react-native-config";

import reducers from "./reducers";
import rootSaga from "./sagas";
import { navMiddleware } from "./nav";
import createReduxPersistTransform from "./createReduxPersistTransform";

// middleware list
const middleware = [];

// create saga middleware
const saga = createSagaMiddleware();

// push middleware
middleware.push(saga);
middleware.push(logger);
middleware.push(navMiddleware);

const reduxPersistEncryptor = createEncryptor({
  secretKey: Config.APP_KEY
});

const config = {
  key: "dje-store",
  storage: FilesystemStorage,
  transforms: [createReduxPersistTransform, reduxPersistEncryptor],
  blacklist: ["nav", "register", "forgot_password"],
};

const persReducer = persistReducer(config, reducers);

// create redux store
const configureStore = () => {
  const store = createStore(persReducer, applyMiddleware(...middleware));
  const persistor = persistStore(store);

  // Run redux-saga
  saga.run(rootSaga);

  return { persistor, store };
};

export default configureStore;
