import { combineReducers } from "redux";
import { reducer as loginReducer } from "../containers/Login";
import { reducer as notificationReducer } from "../containers/Notification";
import { reducer as navReducer } from "./nav";
import { reducer as customerReducer } from "../containers/Customer";
import { reducer as marketingReducer } from "../containers/Marketing";
import { reducer as activityReducer } from "../containers/Activity";
import { reducer as planReducer } from "../containers/Plan";
import { reducer as projectReducer } from "../containers/Project";
import { reducer as regencyReducer } from "../containers/SelectRegency";
import { reducer as provinceReducer } from "../containers/SelectProvince";
import { reducer as userReducer } from "../containers/Profile";

export default combineReducers({
  auth: loginReducer,
  nav: navReducer,
  notification: notificationReducer,
  customer: customerReducer,
  marketing: marketingReducer,
  activity: activityReducer,
  plan: planReducer,
  project: projectReducer,
  regencies: regencyReducer,
  provinces: provinceReducer,
  user: userReducer
});
