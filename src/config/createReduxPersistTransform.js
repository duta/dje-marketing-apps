import { createTransform } from "redux-persist";

const createReduxPersistTransform = createTransform(
  (inboundState, key) => inboundState,
  (outboundState, key) => outboundState
);

export default createReduxPersistTransform;
