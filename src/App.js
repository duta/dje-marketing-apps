/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, StatusBar} from 'react-native';
import EStyleSheet from "react-native-extended-stylesheet";
import Navigation from "./config/nav";
import configureStore from "./config/store";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/es/integration/react";
import SplashScreen from "./containers/SplashScreen";

import {
  BG_COLOR,
  COLOR_INPUT_BORDER_FOCUS,
  COLOR_INPUT_BORDER_NORMAL,
  COLOR_PRIMARY,
  COLOR_SECONDARY
} from "./config/common";

// const instructions = Platform.select({
//   ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
//   android:
//     'Double tap R on your keyboard to reload,\n' +
//     'Shake or press menu button for dev menu',
// });

const { persistor, store } = configureStore();

EStyleSheet.build({
  $primaryColor: COLOR_PRIMARY,
  $borderInputNormal: COLOR_INPUT_BORDER_NORMAL,
  $borderInputFocus: COLOR_INPUT_BORDER_FOCUS,
  $bgColor: BG_COLOR,
  $link: {
    fontSize: 14,
    color: "blue",
    marginBottom: 4
  }
});

// type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      <Provider store={store}>
        <PersistGate
          loading={<SplashScreen />}
          persistor={persistor}
        >
          <StatusBar
            backgroundColor={COLOR_SECONDARY}
            barStyle="light-content"
          />
          <Navigation />
        </PersistGate>
      </Provider>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  container2 : {
    flex: 1,
  }
});
