import { AsyncStorage } from "react-native";

const storageKey = "@DJE:state";

/**
 * Store state to localStorage
 *
 * @param {*} state
 */
export async function saveState(state) {
  try {
    await AsyncStorage.setItem(storageKey, JSON.stringify(state));
  } catch (error) {}
}

/**
 * Get state from store
 *
 * @param {*} state
 */
export async function getState(state) {
  try {
    let state = await AsyncStorage.getItem(storageKey);
    if (state === null) return undefined;

    state = JSON.parse(state);
    return state;
  } catch (error) {
    return undefined;
  }
}
