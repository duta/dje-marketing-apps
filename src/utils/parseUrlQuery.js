export default (parseUrlQuery = (queries = {}) => {
  const esc = encodeURIComponent;
  let parsedQueries = Object.keys(queries)
    .map(key => `${esc(key)}=${esc(queries[key])}`)
    .join("&");

  return parsedQueries;
});
