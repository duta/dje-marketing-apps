import React from "react";
import { Circle, Defs, G, Path, Rect } from "react-native-svg";

const icons = {
  home: {
    svg: (
      <Circle
        cx="0"
        cy="0"
        r="45"
        stroke="blue"
        strokeWidth="2.5"
        fill="green"
      />
    ),
    viewBox: "0 0 32 32"
  }
};

export default icons;
