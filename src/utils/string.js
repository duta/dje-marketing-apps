export function pad(number, width, char = "0") {
  number = number + "";
  return number.length >= width
    ? number
    : new Array(width - number.length + 1).join(char) + number;
}

export function formatPhone(phone) {
  if (null === phone || undefined === phone) return null;

  if (phone.charAt(0) === "+") {
    return phone.replace(/(\+)(\d{2})(\d{4})(\d{4})(\d)/g, "$1$2 $3-$4-$5");
  }

  return phone.replace(/(\d{4})(\d{4})(\d)/g, "$1-$2-$3");
}

export function clearPhoneFormat(phone) {
  if (null === phone || undefined === phone) return null;
  phone = phone.replace(/\s/g, "");
  phone = phone.replace(/-/g, "");

  return phone;
}

export function formatCreditCard(credit_card) {
  if (null === credit_card || undefined === credit_card) return null;

  var v = credit_card.replace(/\s+/g, "").replace(/[^0-9]/gi, "");
  var matches = v.match(/\d{4,16}/g);
  var match = (matches && matches[0]) || "";
  var parts = [];
  for (i = 0, len = match.length; i < len; i += 4) {
    parts.push(match.substring(i, i + 4));
  }
  if (parts.length) {
    return parts.join(" ");
  } else {
    return credit_card;
  }
}

export function toTitleCase(words) {
  if (undefined === words || null === words || words === "") return words;

  words = words.split(" ");

  for (var i = 0; i < words.length; i++) {
    if (undefined !== words[i][0]) {
      if (words[i].length === 1) words[i] = words[i].toUpperCase();
      else words[i] = words[i][0].toUpperCase() + words[i].substr(1);
    }
  }

  return words.join(" ");
}

export function toLowerCase(str) {
  if (null === str || str === undefined) return null;

  return str.toLowerCase();
}

export function truncate(text, maxLength = 100) {
  if (null === text || undefined === text) return null;
  if (text.length > maxLength) {
    return text.substr(0, maxLength) + "...";
  }

  return text;
}

export function getFileNameFromPath(path) {
  if (path === null) return null;
  var pathArray = path.split('/');

  return pathArray[pathArray.length - 1];
}

export function filterPhoneNumber(val) {
  if (!val || val === null) return null;
  return val.replace(/[^0-9]/g, '');
}

export function projecStatus(val){
  switch (val) {
    case 0:
      return "Perencanaan";
    case 1:
      return "Tender";
    case 2:
      return "PO";
    case 3:
      return "Selesai";
    case 4:
      return "Batal";
  }
}

export function getRandomColor() {
  var letters = '0123456789ABCDEF';
  var color = '#';
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}
