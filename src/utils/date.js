import Config from "react-native-config";
import moment from "moment";

export const months = [
  "Januari",
  "Februari",
  "Maret",
  "April",
  "Mei",
  "Juni",
  "Juli",
  "Agustus",
  "September",
  "Oktober",
  "November",
  "Desember"
];

/**
 * Formatting Date
 * Pastikan LOCALE pada .env diset id-ID
 *
 * @param {*} date
 * @param {*} format
 * @param {*} dateFormat
 */
export function formatDate(date, format = "d-m-Y", dateFormat) {
  if (null === date || undefined === date) return null;

  var idLocale = require("moment/locale/id");
  moment.updateLocale("id", idLocale);
  
  if (dateFormat) {
    var formattedDate = moment(date, dateFormat).format(format);
  } else {
    var formattedDate = moment(date).format(format);
  }

  if (formattedDate === "Invalid date") return null;

  return formattedDate;
}

export function getAge(birthDate) {
  if (birthDate === null || undefined === birthDate) return null;

  var ageInYears = moment().diff(birthDate, "years");
  if (ageInYears > 0) return `${ageInYears} tahun`;

  var ageInMonths = moment().diff(birthDate, "months");
  return `${ageInMonths} bulan`;
}

export function getToday(){
  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth() + 1; //January is 0!

  var yyyy = today.getFullYear();
  if (dd < 10) {
    dd = '0' + dd;
  } 
  if (mm < 10) {
    mm = '0' + mm;
  } 
  var today = yyyy + '-' + mm + '-' + dd;
  
  return today;
}
