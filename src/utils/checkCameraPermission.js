import { PermissionsAndroid } from "react-native";

async function checkCameraPermission() {
  try {
    const status = await PermissionsAndroid.check(
      PermissionsAndroid.PERMISSIONS.CAMERA
    );
    return status;
  } catch (error) {
    return Promise.reject(error);
  }
}

export default checkCameraPermission;
