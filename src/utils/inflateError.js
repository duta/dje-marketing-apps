import _ from "lodash";

export default function inflateError(errors) {
  if (null === errors || undefined === errors) return errors;

  let output = errors;
  if (typeof errors === "object") {
    output = "";
    _.forIn(errors, (val, key) => {
      if (!_.isNaN(key)) output += `${val}`;
      else output += `${val[0]}, `;
    });
  }

  return output;
}
