import _ from "lodash";

export default function convertToFormData(object) {
  let formData = new FormData();
  _.forEach(object, (value, key) => {
    formData.append(key, value);
  });

  return formData;
}
