export function isEq(arr1 = [], arr2 = []) {
  if (arr1.length !== arr2.length) return false;

  var isEqual = true;
  for (var i = 0; i < arr1.length; i++) {
    if (arr2.indexOf(arr1[i]) === -1) {
      isEqual = false;
      break;
    }
  }

  return isEqual;
}

export function clean(obj) {
  if (obj == null) return null;

  Object.keys(obj).forEach(key => {
    if (obj[key] === null) delete obj[key];
  });

  return obj;
}
